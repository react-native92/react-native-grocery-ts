import React, {FC, useEffect} from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {
  DarkTheme,
  DefaultTheme,
  NavigationContainer,
  useTheme,
} from '@react-navigation/native';
import {enableLatestRenderer} from 'react-native-maps';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {Provider} from 'react-redux';
import {Security} from './src/components/otherComponents/Security';
import {storeFCMToken} from './src/redux/Root.Actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Colors from './src/utils/Colors';
import FlashMessage from 'react-native-flash-message';
import inAppMessaging from '@react-native-firebase/in-app-messaging';
import Indicator from './src/components/appComponents/Indicator';
import messaging, {
  FirebaseMessagingTypes,
} from '@react-native-firebase/messaging';
import notifee, {
  AndroidImportance,
  AndroidVisibility,
} from '@notifee/react-native';
import RootNavigation from './src/navigations/RootNavigation';
import SplashScreen from 'react-native-splash-screen';
import Store from './src/redux/Store';
import Strings from './src/utils/Strings';
import * as HelperStyles from './src/utils/HelperStyles';
import * as Helpers from './src/utils/Helpers';

const App: FC = () => {
  // Configuration Variables
  // Dark Theme
  const customDarkTheme = {
    ...DarkTheme,
    colors: {
      ...DarkTheme.colors,
      background: Colors.black,
      primary: Colors.primary,
      text: Colors.white,
    },
  };

  // Light Theme
  const customLightTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: Colors.white,
      primary: Colors.primary,
      text: Colors.black,
    },
  };

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();
  const Theme = useTheme().colors;

  useEffect(() => {
    let isFocus: boolean = true;

    // For react-native-maps configuration
    enableLatestRenderer();

    // For react-native-splash-screen configuration
    SplashScreen.hide();

    // For @react-native-firebase/in-app-messaging configuration
    inAppMessageConfig();

    // For @react-native-firebase/messaging configuration
    requestUserPermission();

    return (): void => {
      isFocus = false;
    };
  }, []);

  async function inAppMessageConfig() {
    await inAppMessaging().setMessagesDisplaySuppressed(false);
  }

  async function requestUserPermission(): Promise<void> {
    const authStatus: FirebaseMessagingTypes.AuthorizationStatus =
      await messaging().requestPermission();

    const enabled: boolean =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    Boolean(enabled) && getFCMToken();
  }

  async function getFCMToken(): Promise<void> {
    const fcmToken = await messaging().getToken();

    Store.dispatch(storeFCMToken(fcmToken));
  }

  useEffect(() => {
    let isFocus: boolean = true;

    const unsubscribe = messaging().onMessage(async remoteMessage => {
      const userId = await AsyncStorage.getItem('userId');

      notifeeDisplayNotification(remoteMessage);

      Helpers.pushNotify(remoteMessage, userId);
    });

    return (): void => {
      isFocus = false;

      unsubscribe();
    };
  }, []);

  async function notifeeDisplayNotification(
    remoteMessage: FirebaseMessagingTypes.RemoteMessage,
  ) {
    await notifee.requestPermission();

    const channelId = await notifee.createChannel({
      id: 'default',
      lights: true,
      lightColor: Colors.primary,
      name: 'Default Channel',
      vibration: true,
    });

    Boolean(remoteMessage?.notification) &&
      (await notifee.displayNotification({
        title: remoteMessage.notification?.title ?? Strings.notifications,
        body: remoteMessage.notification?.body,
        android: {
          channelId,
          importance: AndroidImportance.HIGH,
          visibility: AndroidVisibility.PRIVATE,
        },
      }));
  }

  return (
    <GestureHandlerRootView style={HelperStyles.flex(1)}>
      <SafeAreaView style={HelperStyles.screenContainer(Theme.background)}>
        <StatusBar barStyle={'default'} backgroundColor={Colors.white} />

        <Provider store={Store}>
          <NavigationContainer
            fallback={<Indicator />}
            theme={
              themeScheme == Strings.dark ? customDarkTheme : customLightTheme
            }>
            <RootNavigation />
          </NavigationContainer>
        </Provider>

        {/* Custom Plugin Provider */}
        <FlashMessage duration={2000} floating={true} position={'top'} />
      </SafeAreaView>
    </GestureHandlerRootView>
  );
};

export default Security(App);
