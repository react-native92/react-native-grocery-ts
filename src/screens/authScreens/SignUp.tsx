import React, {FC, Fragment, ReactElement, useState} from 'react';
import {Text, View} from 'react-native';
import {checkAvailability, registerUser} from '../../configs/jsons/Users';
import {connect} from 'react-redux';
import {loadingStatus} from '../../redux/Root.Actions';
import {ResponseInterface, UserInterface} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useTheme} from '@react-navigation/native';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import CustomTextInput from '../../components/appComponents/CustomTextInput';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/SignUp';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const SignUp: FC = (props: any) => {
  // SignUp Variables
  const [username, setUsername] = useState<any>(null);
  const [emailAddress, setEmailAddress] = useState<any>(null);
  const [password, setPassword] = useState<any>(null);
  const [passwordVisibility, setPasswordVisibility] = useState<boolean>(true);

  // Error Validation
  const [usernameError, setUsernameError] = useState<boolean>(false);
  const [usernameAvailibilityError, setUsernameAvailibilityError] =
    useState<boolean>(false);
  const [emailAddressError, setEmailAddressError] = useState<boolean>(false);
  const [emailAddressInvalidError, setEmailAddressInvalidError] =
    useState<boolean>(false);
  const [emailAddressAvailibilityError, setEmailAddressAvailibilityError] =
    useState<boolean>(false);
  const [passwordError, setPasswordError] = useState<boolean>(false);
  const [passwordInvalidError, setPasswordInvalidError] =
    useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  function renderHeaderText(): ReactElement {
    return (
      <Fragment>
        <Text
          style={HelperStyles.textView(
            18,
            '600',
            Colors.ebony,
            'left',
            'none',
          )}>
          {Strings.singUpText}
        </Text>

        <Text
          style={[
            HelperStyles.textView(14, '600', Colors.manatee, 'left', 'none'),
            HelperStyles.margin(0, 4),
          ]}>
          {Strings.subSignUpText}
        </Text>
      </Fragment>
    );
  }

  function renderUsername(): ReactElement {
    function checkUsernameAvailability(): void {
      const availability: boolean = checkAvailability(username);

      setUsernameAvailibilityError(availability);
    }

    return (
      <CustomTextInput
        autoComplete={'username'}
        error={usernameAvailibilityError || usernameError}
        errorMessage={
          usernameAvailibilityError
            ? Strings.usernameAvailibilityError
            : usernameError
            ? Strings.usernameError
            : null
        }
        onChangeText={(txt: string): void => {
          usernameError && setUsernameError(false);

          usernameAvailibilityError && setUsernameAvailibilityError(false);

          setUsername(Boolean(txt) ? txt : null);
        }}
        onEndEditing={() => {
          checkUsernameAvailability();
        }}
        placeholder={Strings.username}
        textContentType={'username'}
        value={username}
      />
    );
  }

  function renderEmailAddress(): ReactElement {
    function checkEmailAddressAvailability(): void {
      const isValidEmailAddress: boolean = Helpers.validateEmail(emailAddress);

      setEmailAddressInvalidError(isValidEmailAddress);

      if (!Boolean(isValidEmailAddress)) {
        const availability: boolean = checkAvailability(emailAddress);

        setEmailAddressAvailibilityError(availability);
      } else {
        setEmailAddressAvailibilityError(false);
      }
    }

    return (
      <CustomTextInput
        autoComplete={'email'}
        error={
          emailAddressInvalidError ||
          emailAddressAvailibilityError ||
          emailAddressError
        }
        errorMessage={
          emailAddressInvalidError
            ? Strings.emailAddressInvalidError
            : emailAddressAvailibilityError
            ? Strings.emailAddressAvailibilityError
            : emailAddressError
            ? Strings.emailAddressError
            : null
        }
        keyboardType={'email-address'}
        onChangeText={(txt: string): void => {
          emailAddressError && setEmailAddressError(false);

          emailAddressAvailibilityError &&
            setEmailAddressAvailibilityError(false);

          setEmailAddress(Boolean(txt) ? txt : null);
        }}
        onEndEditing={() => {
          checkEmailAddressAvailability();
        }}
        placeholder={Strings.emailAddress}
        textContentType={'emailAddress'}
        value={emailAddress}
      />
    );
  }

  function renderPassword(): ReactElement {
    function handlePassword(txt: string): void {
      const isValidPassword = Helpers.validatePassword(txt);

      passwordError && setPasswordError(false);

      setPassword(Boolean(txt) ? txt : null);

      setPasswordInvalidError(isValidPassword);
    }

    return (
      <CustomTextInput
        autoComplete={'password'}
        error={passwordInvalidError || passwordError}
        errorMessage={
          passwordInvalidError
            ? Strings.passwordInvalidError
            : passwordError
            ? Strings.passwordError
            : null
        }
        onChangeText={(txt: string): void => {
          handlePassword(txt);
        }}
        onPassword={(): void => {
          setPasswordVisibility(!passwordVisibility);
        }}
        placeholder={Strings.password}
        secureTextEntry={passwordVisibility}
        showPasswordIcon={true}
        textContentType={'password'}
        value={password}
      />
    );
  }

  function renderSignUpButton(): ReactElement {
    return (
      <Button
        containerStyle={Styles.signUpButtonContainer}
        loading={props.loadingStatus}
        onPress={(): void => {
          handleSignUp();
        }}
        title={Strings.signUp}
      />
    );
  }

  function handleSignUp(): void {
    if (checkSignUp()) {
      Store.dispatch(loadingStatus(true));

      setTimeout((): void => {
        handleAPI();

        Store.dispatch(loadingStatus(false));
      }, 1000);
    } else {
      handleErrors();
    }
  }

  function checkSignUp(): boolean {
    return (
      Boolean(Helpers.checkField(username)) &&
      !usernameAvailibilityError &&
      Boolean(Helpers.checkField(emailAddress)) &&
      !emailAddressInvalidError &&
      !emailAddressAvailibilityError &&
      Boolean(Helpers.checkField(password)) &&
      !passwordInvalidError
    );
  }

  function handleAPI(): void {
    const requestData: UserInterface = {
      username,
      emailAddress,
      password,
    };

    const response: ResponseInterface = registerUser(requestData);

    if (response.status == StatusCodes.Success) {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.success,
        type: 'success',
      });

      handleReset();

      props.navigation.navigate(Strings.mobileNumber, {emailAddress});
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  function handleReset(): void {
    // SignUp Variables
    setUsername(null);
    setEmailAddress(null);
    setPassword(null);
    setPasswordVisibility(true);

    // Error Validation
    setUsernameError(false);
    setUsernameAvailibilityError(false);
    setEmailAddressError(false);
    setEmailAddressInvalidError(false);
    setEmailAddressAvailibilityError(false);
    setPasswordError(false);
    setPasswordInvalidError(false);
  }

  function handleErrors(): void {
    setUsernameError(!Helpers.checkField(username));

    setEmailAddressError(!Helpers.checkField(emailAddress));

    setPasswordError(!Helpers.checkField(password));
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={HelperStyles.margin(0, 20)}>
        {renderHeaderText()}

        {renderUsername()}

        {renderEmailAddress()}

        {renderPassword()}

        {renderSignUpButton()}
      </View>
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loadingStatus: state?.other?.loadingStatus ?? false,
  };
}

export default connect(mapStateToProps, null)(SignUp);
