import React, {FC, Fragment, ReactElement, useState} from 'react';
import {Text, View} from 'react-native';
import {connect} from 'react-redux';
import {
  loadingStatus,
  storeCartItemCount,
  storeLoginUserInfo,
  storeWishItemCount,
} from '../../redux/Root.Actions';
import {LoginInterFace, ResponseInterface} from '../../configs/ts/Interfaces';
import {loginUser} from '../../configs/jsons/Users';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useTheme} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import CustomTextInput from '../../components/appComponents/CustomTextInput';
import moment from 'moment';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/SignIn';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const SignIn: FC = (props: any) => {
  // SignIn Variables
  const [emailAddress, setEmailAddress] = useState<any>(null);
  const [password, setPassword] = useState<any>(null);
  const [passwordVisibility, setPasswordVisibility] = useState<boolean>(true);

  // Error Validation
  const [emailAddressError, setEmailAddressError] = useState<boolean>(false);
  const [emailAddressInvalidError, setEmailAddressInvalidError] =
    useState<boolean>(false);
  const [passwordError, setPasswordError] = useState<boolean>(false);
  const [passwordInvalidError, setPasswordInvalidError] = useState(false);

  // Theme Variables
  const Theme = useTheme().colors;

  function renderHeaderText(): ReactElement {
    return (
      <Fragment>
        <Text
          style={HelperStyles.textView(
            18,
            '600',
            Colors.ebony,
            'left',
            'none',
          )}>
          {Strings.singInText}
        </Text>

        <Text
          style={[
            HelperStyles.textView(14, '600', Colors.manatee, 'left', 'none'),
            HelperStyles.margin(0, 4),
          ]}>
          {Strings.subSignInText}
        </Text>
      </Fragment>
    );
  }

  function renderEmailAddress(): ReactElement {
    function checkEmailAddress(): void {
      const isValidEmailAddress: boolean = Helpers.validateEmail(emailAddress);

      setEmailAddressInvalidError(isValidEmailAddress);
    }

    return (
      <CustomTextInput
        autoComplete={'email'}
        error={emailAddressInvalidError || emailAddressError}
        errorMessage={
          emailAddressInvalidError
            ? Strings.emailAddressInvalidError
            : emailAddressError
            ? Strings.emailAddressError
            : null
        }
        keyboardType={'email-address'}
        onChangeText={(txt: string): void => {
          emailAddressError && setEmailAddressError(false);

          setEmailAddress(Boolean(txt) ? txt : null);
        }}
        onEndEditing={() => {
          checkEmailAddress();
        }}
        placeholder={Strings.emailAddress}
        textContentType={'emailAddress'}
        value={emailAddress}
      />
    );
  }

  function renderPassword(): ReactElement {
    function handlePassword(txt: string): void {
      const isValidPassword = Helpers.validatePassword(txt);

      passwordError && setPasswordError(false);

      setPassword(Boolean(txt) ? txt : null);

      setPasswordInvalidError(isValidPassword);
    }

    return (
      <CustomTextInput
        autoComplete={'password'}
        error={passwordInvalidError || passwordError}
        errorMessage={
          passwordInvalidError
            ? Strings.passwordInvalidError
            : passwordError
            ? Strings.passwordError
            : null
        }
        onChangeText={(txt: string): void => {
          handlePassword(txt);
        }}
        onPassword={(): void => {
          setPasswordVisibility(!passwordVisibility);
        }}
        placeholder={Strings.password}
        secureTextEntry={passwordVisibility}
        showPasswordIcon={true}
        textContentType={'password'}
        value={password}
      />
    );
  }

  function renderForgotPasswordSignInButton(): ReactElement {
    return (
      <View style={Styles.forgotPasswordSignInContainer}>
        <View
          style={[
            HelperStyles.flex(0.4875),
            HelperStyles.alignItemsCenteredView('flex-start'),
          ]}>
          <Text
            onPress={(): void => {
              props.navigation.navigate(Strings.forgotPassword);
            }}
            style={HelperStyles.textView(
              14,
              '600',
              Colors.manatee,
              'left',
              'none',
            )}>
            {Strings.forgotPassword}?
          </Text>
        </View>

        <View
          style={[
            HelperStyles.flex(0.4875),
            HelperStyles.alignItemsCenteredView('flex-end'),
          ]}>
          <Button
            containerStyle={Styles.signInButtonContainer}
            loading={props.loadingStatus}
            onPress={(): void => {
              handleSignIn();
            }}
            title={Strings.signIn}
          />
        </View>
      </View>
    );
  }

  function handleSignIn(): void {
    if (checkSignIn()) {
      Store.dispatch(loadingStatus(true));

      setTimeout((): void => {
        handleAPI();

        Store.dispatch(loadingStatus(false));
      }, 1000);
    } else {
      handleErrors();
    }
  }

  function checkSignIn(): boolean {
    return (
      Boolean(Helpers.checkField(emailAddress)) &&
      Boolean(Helpers.checkField(password))
    );
  }

  function handleAPI(): void {
    const requestData: LoginInterFace = {
      emailAddress,
      password,
    };

    const response: ResponseInterface = loginUser(requestData);

    if (response.status == StatusCodes.Success) {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.success,
        type: 'success',
      });

      handleReset();

      if (!Boolean(response?.data?.mobileNumber ?? null)) {
        props.navigation.navigate(Strings.mobileNumber, {
          emailAddress,
          fromScreen: Strings.signIn,
        });
      } else if (
        Boolean(response?.data?.mobileNumber ?? null) &&
        Boolean(response?.data?.otp ?? null) &&
        !Boolean(response?.data?.isVerified ?? false)
      ) {
        props.navigation.navigate(Strings.mobileNumberVerification, {
          fromScreen: Strings.signIn,
          mobileNumber: response?.data?.mobileNumber,
          otp: response?.data?.otp
            ? Helpers.decrypt(response?.data?.otp)
            : null,
        });
      } else {
        AsyncStorage.setItem('userId', String(response?.data?.id));
        AsyncStorage.setItem('userLoggedIn', String(moment()));

        Store.dispatch(storeLoginUserInfo(response?.data));

        Store.dispatch(
          storeWishItemCount(response?.data?.favorites.length ?? 0),
        );

        Store.dispatch(storeCartItemCount(response?.data?.cart.length ?? 0));

        Helpers.initNotifyBG(response?.data);

        props.navigation.navigate(Strings.dashboard, {
          initialRouteName: Strings.store,
        });
      }
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  function handleReset(): void {
    // SignIn Variables
    setEmailAddress(null);
    setPassword(null);
    setPasswordVisibility(true);

    // Error Validation
    setEmailAddressError(false);
    setEmailAddressInvalidError(false);
    setPasswordError(false);
    setPasswordInvalidError(false);
  }

  function handleErrors(): void {
    setEmailAddressError(!Helpers.checkField(emailAddress));

    setPasswordError(!Helpers.checkField(password));
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={HelperStyles.margin(0, 20)}>
        {renderHeaderText()}

        {renderEmailAddress()}

        {renderPassword()}

        {renderForgotPasswordSignInButton()}
      </View>
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loadingStatus: state?.other?.loadingStatus ?? false,
  };
}

export default connect(mapStateToProps, null)(SignIn);
