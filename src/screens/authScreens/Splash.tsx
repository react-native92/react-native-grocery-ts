import React, {FC, useCallback} from 'react';
import {Image, View} from 'react-native';
import {connect} from 'react-redux';
import {fetchUser} from '../../configs/jsons/Users';
import {
  restoreStates,
  storeCartItemCount,
  storeLoginUserInfo,
  storeWishItemCount,
} from '../../redux/Root.Actions';
import {showMessage} from 'react-native-flash-message';
import {useFocusEffect} from '@react-navigation/core';
import {UserInterface} from '../../configs/ts/Interfaces';
import {useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/Splash';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const Splash: FC = (props: any) => {
  // Splash Variables

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      init();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function init(): void {
    checkFile();
  }

  function checkFile(): void {
    Helpers.checkJSON(Strings.JSON)
      .then((status: boolean): void => {
        if (Boolean(status)) {
          jsonToStore();
        } else {
          storeToJSON();
        }

        checkUser();
      })
      .catch((): void => {
        showMessage({
          description: Strings.checkJSONCache,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      });
  }

  function jsonToStore(): void {
    Helpers.readJSON(Strings.JSON)
      .then((content: string): void => {
        content = JSON.parse(content);

        props.restoreStates(content);
      })
      .catch((): void => {
        showMessage({
          description: Strings.readJSONCache,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      });
  }

  function storeToJSON(): void {
    Helpers.writeJSON(Strings.JSON, JSON.stringify(props.appContent));
  }

  async function checkUser(): Promise<void> {
    const userId: null | string = await AsyncStorage.getItem('userId');

    if (Boolean(userId)) {
      const userData: undefined | UserInterface = fetchUser(userId);

      if (Boolean(userData)) {
        Store.dispatch(storeLoginUserInfo(userData));

        Store.dispatch(storeWishItemCount(userData?.favorites.length ?? 0));

        Store.dispatch(storeCartItemCount(userData?.cart.length ?? 0));

        Helpers.initNotifyBG(userData);

        setTimeout((): void => {
          props.navigation.navigate(Strings.dashboard, {
            initialRouteName: Strings.store,
          });
        }, 2500);
      } else {
        setTimeout((): void => {
          props.navigation.navigate(Strings.onBoard);
        }, 2500);
      }
    } else {
      setTimeout((): void => {
        props.navigation.navigate(Strings.onBoard);
      }, 2500);
    }
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={Styles.screenContainer}>
        <View style={HelperStyles.screenSubContainer}>
          <Image
            resizeMode={'contain'}
            source={Assets.logo}
            style={HelperStyles.imageView(120, 120)}
          />
        </View>
      </View>
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    appContent: state?.app ?? null,
  };
}

function mapDispatchToProps(dispatch: any) {
  return {
    restoreStates: (requestData: any[]) => {
      dispatch(restoreStates(requestData));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
