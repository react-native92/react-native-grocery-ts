import React, {FC, Fragment, ReactElement, useState} from 'react';
import {Text, View} from 'react-native';
import {checkAvailability, getOTP} from '../../configs/jsons/Users';
import {connect} from 'react-redux';
import {loadingStatus} from '../../redux/Root.Actions';
import {OTPInterFace, ResponseInterface} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useTheme} from '@react-navigation/native';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import CustomTextInput from '../../components/appComponents/CustomTextInput';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/MobileNumber';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const MobileNumber: FC = (props: any) => {
  // Props Variables
  const emailAddress: string = props?.route?.params?.emailAddress ?? null;
  const fromScreen: string = props?.route?.params?.fromScreen ?? null;

  // MobileNumber Variables
  const [mobileNumber, setMobileNumber] = useState<any>(null);

  // Error Variables
  const [mobileNumberError, setMobileNumberError] = useState<boolean>(false);
  const [mobileNumberInvalidError, setMobileNumberInvalidError] =
    useState<boolean>(false);
  const [mobileNumberAvailibilityError, setMobileNumberAvailibilityError] =
    useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  function renderHeaderText(): ReactElement {
    return (
      <Fragment>
        <Text
          style={HelperStyles.textView(
            18,
            '600',
            Colors.ebony,
            'left',
            'none',
          )}>
          {Strings.mobileNumberText}
        </Text>

        <Text
          style={[
            HelperStyles.textView(14, '600', Colors.manatee, 'left', 'none'),
            HelperStyles.margin(0, 4),
          ]}>
          {Strings.subMobileNumberText}
        </Text>
      </Fragment>
    );
  }

  function renderMobileNumber(): ReactElement {
    function checkMobileNumber(txt: string): void {
      const availability: boolean = Boolean(txt)
        ? checkAvailability(txt)
        : false;

      const isValidMobileNumber = Boolean(txt)
        ? !Boolean(txt.length == 10)
        : false;

      mobileNumberError && setMobileNumberError(false);

      mobileNumberAvailibilityError && setMobileNumberAvailibilityError(false);

      setMobileNumber(Boolean(txt) ? txt : null);

      setMobileNumberInvalidError(isValidMobileNumber);

      setMobileNumberAvailibilityError(
        !isValidMobileNumber ? availability : false,
      );
    }

    return (
      <View style={Styles.mobileNumberTextInputContainer}>
        <View
          style={[
            HelperStyles.flex(0.1375),
            HelperStyles.alignItemsCenteredView('flex-start'),
          ]}>
          <CustomTextInput
            autoComplete={'tel-country-code'}
            defaultErrorMessage={null}
            editable={false}
            error={
              mobileNumberAvailibilityError ||
              mobileNumberInvalidError ||
              mobileNumberError
            }
            iconContainer={HelperStyles.flex(0.375)}
            keyboardType={'number-pad'}
            renderIcon={(): ReactElement => {
              return (
                <MaterialIcons
                  color={Colors.manatee}
                  name={'keyboard-arrow-down'}
                  size={16}
                />
              );
            }}
            showIcon={true}
            textInputContainerStyle={HelperStyles.flex(0.625)}
            value={'+91'}
          />
        </View>

        <View
          style={[
            HelperStyles.flex(0.8375),
            HelperStyles.alignItemsCenteredView('flex-start'),
          ]}>
          <CustomTextInput
            autoComplete={'tel'}
            error={
              mobileNumberAvailibilityError ||
              mobileNumberInvalidError ||
              mobileNumberError
            }
            errorMessage={
              mobileNumberInvalidError
                ? Strings.mobileNumberInvalidError
                : mobileNumberAvailibilityError
                ? Strings.mobileNumberAvailibilityError
                : mobileNumberError
                ? Strings.mobileNumberError
                : null
            }
            keyboardType={'number-pad'}
            maxLength={10}
            onChangeText={(txt: string): void => {
              checkMobileNumber(txt);
            }}
            placeholder={Strings.mobileNumber}
            textContentType={'telephoneNumber'}
            value={mobileNumber}
          />
        </View>
      </View>
    );
  }

  function renderSendButton(): ReactElement {
    return (
      <Button
        containerStyle={Styles.sendButtonContainer}
        loading={props.loadingStatus}
        onPress={(): void => {
          handleSend();
        }}
        title={Strings.send}
      />
    );
  }

  function handleSend(): void {
    if (checkSignUp()) {
      Store.dispatch(loadingStatus(true));

      setTimeout((): void => {
        handleAPI();

        Store.dispatch(loadingStatus(false));
      }, 1000);
    } else {
      handleErrors();
    }
  }

  function checkSignUp(): boolean {
    return (
      Boolean(Helpers.checkField(mobileNumber)) &&
      !mobileNumberInvalidError &&
      !mobileNumberAvailibilityError
    );
  }

  function handleAPI(): void {
    const requestData: OTPInterFace = {
      emailAddress,
      mobileNumber,
    };

    const response: ResponseInterface = getOTP(requestData);

    if (response.status == StatusCodes.Success) {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.success,
        type: 'success',
      });

      handleReset();

      props.navigation.navigate(Strings.mobileNumberVerification, {
        fromScreen,
        mobileNumber,
        otp: response?.data?.otp ?? null,
      });
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  function handleReset(): void {
    // MobileNumber Variables
    setMobileNumber(null);

    // Error Validation
    setMobileNumberError(false);
    setMobileNumberInvalidError(false);
    setMobileNumberAvailibilityError(false);
  }

  function handleErrors(): void {
    setMobileNumberError(!Helpers.checkField(mobileNumber));
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={HelperStyles.screenSubContainer}>
        {renderHeaderText()}

        {renderMobileNumber()}

        {renderSendButton()}
      </View>
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loadingStatus: state?.other?.loadingStatus ?? false,
  };
}

export default connect(mapStateToProps, null)(MobileNumber);
