import React, {FC, Fragment, ReactElement, useCallback, useState} from 'react';
import {BackHandler, Image, Modal, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {
  loadingStatus,
  storeCartItemCount,
  storeLoginUserInfo,
  storeWishItemCount,
} from '../../redux/Root.Actions';
import {resendOTP, verifyOTP} from '../../configs/jsons/Users';
import {
  ResendOTPInterFace,
  ResponseInterface,
  VerifyOTPInterFace,
} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/MobileNumberVerification';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const MobileNumberVerification: FC = (props: any) => {
  // Props Variables
  const fromScreen: string = props?.route?.params?.fromScreen ?? null;
  const mobileNumber: string = props?.route?.params?.mobileNumber ?? null;
  const [code, setCode] = useState<any>(props?.route?.params?.otp ?? null);

  // MobileNumberVerification Variables
  const [timer, setTimer] = useState<number>(Strings.timerSeconds);
  const [otp, setOTP] = useState<string>('');
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  // Error Variables
  const [otpError, setOTPError] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      const timerId = setInterval((): void => {
        timer <= 0 ? handleResendAPI() : setTimer(s => s - 1);
      }, 1000);

      return (): void => {
        isFocus = false;

        clearInterval(timerId);
      };
    }, [timer]),
  );

  function handleResendAPI(): void {
    setOTPError(false);

    setOTP('');

    const requestData: ResendOTPInterFace = {
      mobileNumber,
    };

    const response: ResponseInterface = resendOTP(requestData);

    setTimer(Strings.timerSeconds);

    if (response.status == StatusCodes.Success) {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.success,
        type: 'success',
      });

      setCode(response?.data?.otp ?? code);
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  function renderHeaderText(): ReactElement {
    return (
      <Fragment>
        <Text
          style={HelperStyles.textView(
            18,
            '600',
            Colors.ebony,
            'left',
            'none',
          )}>
          {Strings.mobileNumberVerificationText}
        </Text>

        <Text
          style={[
            HelperStyles.textView(14, '600', Colors.manatee, 'left', 'none'),
            HelperStyles.margin(0, 4),
          ]}>
          {Strings.subMobileNumberVerificationText}{' '}
          <Text
            style={HelperStyles.textView(
              14,
              '800',
              Colors.primary,
              'left',
              'none',
            )}>{` +91 ${mobileNumber}`}</Text>
        </Text>
      </Fragment>
    );
  }

  function renderOTP(): ReactElement {
    return (
      <Fragment>
        <OTPInputView
          code={otp}
          codeInputFieldStyle={Styles.otpInactiveView}
          codeInputHighlightStyle={Styles.otpActiveView}
          keyboardType={'number-pad'}
          onCodeChanged={(otpCode: string): void => {
            setOTPError(Boolean(otpCode) ? code != otpCode : false);

            setOTP(Boolean(otpCode) ? otpCode : '');
          }}
          pinCount={Strings.otpLength}
          style={Styles.otpContainer}
        />

        {otpError && (
          <Text style={HelperStyles.errorText}>
            {Boolean(otp) ? Strings.otpInvalidError : Strings.otpError}
          </Text>
        )}

        <View style={HelperStyles.margin(0, 4)}>
          <Text
            style={HelperStyles.textView(
              14,
              '600',
              Colors.manatee,
              'center',
              'none',
            )}>
            {code}
          </Text>
        </View>
      </Fragment>
    );
  }

  function renderResendVerifyButton(): ReactElement {
    return (
      <View style={Styles.resendVerifyContainer}>
        <View
          style={[
            HelperStyles.flex(0.4875),
            HelperStyles.alignItemsCenteredView('flex-start'),
          ]}>
          <Text
            style={HelperStyles.textView(
              14,
              '600',
              Colors.manatee,
              'left',
              'none',
            )}>
            {Boolean(timer)
              ? `${Strings.resend} ${Strings.in.toLowerCase()} 00:${
                  timer < 10 ? `0${timer}s` : `${timer}s`
                }`
              : Strings.resend}
          </Text>
        </View>

        <View
          style={[
            HelperStyles.flex(0.4875),
            HelperStyles.alignItemsCenteredView('flex-end'),
          ]}>
          <Button
            containerStyle={Styles.verifyButtonContainer}
            loading={props.loadingStatus}
            onPress={(): void => {
              handleVerify();
            }}
            title={Strings.verify}
          />
        </View>
      </View>
    );
  }

  function handleVerify(): void {
    if (checkVerify()) {
      Store.dispatch(loadingStatus(true));

      setTimeout((): void => {
        handleAPI();

        Store.dispatch(loadingStatus(false));
      }, 1000);
    } else {
      handleErrors();
    }
  }

  function checkVerify(): boolean {
    return Boolean(Helpers.checkField(otp)) && code == otp;
  }

  function handleAPI(): void {
    const requestData: VerifyOTPInterFace = {
      mobileNumber,
      otp,
    };

    const response: ResponseInterface = verifyOTP(requestData);

    if (response.status == StatusCodes.Success) {
      handleReset();

      if (fromScreen == Strings.signIn) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        AsyncStorage.setItem('userId', String(response?.data?.id));
        AsyncStorage.setItem('userLoggedIn', String(moment()));

        Store.dispatch(storeLoginUserInfo(response?.data));

        Store.dispatch(
          storeWishItemCount(response?.data?.favorites.length ?? 0),
        );

        Store.dispatch(storeCartItemCount(response?.data?.cart.length ?? 0));

        Helpers.initNotifyBG(response?.data);

        props.navigation.navigate(Strings.dashboard, {
          initialRouteName: Strings.store,
        });
      } else {
        setModalVisible(!modalVisible);
      }
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  function handleReset(): void {
    // MobileNumberVerification Variables
    setTimer(Strings.timerSeconds);
    setOTP('');

    // Error Variables
    setOTPError(false);
  }

  function handleErrors(): void {
    setOTPError(!(Helpers.checkField(otp) && code == otp));
  }

  function renderModal(): ReactElement {
    function renderLogoSection(): ReactElement {
      return (
        <View
          style={[
            HelperStyles.flex(0.75),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <View
            style={[
              HelperStyles.imageView(112, 112),
              HelperStyles.justifyContentCenteredView('center'),
              Styles.modalSquareContainer,
            ]}
          />

          <Image
            resizeMode={'contain'}
            source={Assets.logoRounded}
            style={[
              HelperStyles.justView('position', 'absolute'),
              HelperStyles.imageView(120, 120),
            ]}
          />
        </View>
      );
    }

    function renderMessageSection(): ReactElement {
      function renderMessageContent(): ReactElement {
        return (
          <View
            style={[
              HelperStyles.flexDirection('row'),
              HelperStyles.justView('justifyContent', 'space-between'),
            ]}>
            <View style={Styles.modalMesssageIconContainer}>
              <MaterialIcons
                color={Colors.primary}
                name={'check-circle'}
                size={48}
              />
            </View>

            <View
              style={[HelperStyles.flex(0.8375), HelperStyles.padding(8, 8)]}>
              <Text
                style={HelperStyles.textView(
                  20,
                  '600',
                  Colors.ebony,
                  'left',
                  'none',
                )}>
                {Strings.congratulations}!
              </Text>

              <Text
                style={[
                  HelperStyles.textView(
                    16,
                    '600',
                    Colors.manatee,
                    'left',
                    'none',
                  ),
                  HelperStyles.margin(0, 4),
                ]}>
                {Strings.verifyModalText}
              </Text>
            </View>
          </View>
        );
      }

      function renderMessageButton(): ReactElement {
        return (
          <View
            style={[
              HelperStyles.flexDirection('row'),
              HelperStyles.justifyContentCenteredView('flex-end'),
            ]}>
            <Button
              containerStyle={Styles.modalMesssageLightButtonContainer}
              mode={'light'}
              onPress={(): void => {
                setModalVisible(!modalVisible);

                BackHandler.exitApp();
              }}
              textStyle={HelperStyles.justView('fontSize', 14)}
              title={Strings.later}
            />

            <Button
              containerStyle={Styles.modalMesssageSolidButtonContainer}
              onPress={(): void => {
                setModalVisible(!modalVisible);

                props.navigation.navigate(Strings.signIn);
              }}
              textStyle={HelperStyles.justView('fontSize', 14)}
              title={Strings.signIn}
            />
          </View>
        );
      }

      return (
        <View style={Styles.modalMessageContainer}>
          {renderMessageContent()}

          {renderMessageButton()}
        </View>
      );
    }

    return (
      <Modal
        animationType={'slide'}
        onRequestClose={(): void => {
          setModalVisible(!modalVisible);
        }}
        transparent={true}
        visible={modalVisible}>
        <View style={HelperStyles.screenContainer(Colors.primary)}>
          {renderLogoSection()}

          {renderMessageSection()}
        </View>
      </Modal>
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={HelperStyles.screenSubContainer}>
        {renderHeaderText()}

        {renderOTP()}

        {renderResendVerifyButton()}
      </View>

      {renderModal()}
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loadingStatus: state?.other?.loadingStatus ?? false,
  };
}

export default connect(mapStateToProps, null)(MobileNumberVerification);
