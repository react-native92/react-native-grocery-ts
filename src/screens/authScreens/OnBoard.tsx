import React, {FC, ReactElement, useCallback, useState} from 'react';
import {BackHandler, Image, Text, View} from 'react-native';
import {showMessage} from 'react-native-flash-message';
import {useFocusEffect} from '@react-navigation/core';
import Assets from '../../assets/Index';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import Strings from '../../utils/Strings';
import Styles from '../../styles/authStyles/OnBoard';
import * as HelperStyles from '../../utils/HelperStyles';

const OnBoard: FC = (props: any) => {
  // OnBoard Variables
  const [exitCount, setExitCount] = useState<number>(0);

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      BackHandler.addEventListener('hardwareBackPress', backAction);

      return (): void => {
        isFocus = false;

        BackHandler.removeEventListener('hardwareBackPress', backAction);
      };
    }, [exitCount]),
  );

  function backAction(): boolean {
    setTimeout((): void => {
      setExitCount(0);
    }, 2000);

    switch (exitCount) {
      case 0:
        setExitCount(exitCount + 1);

        showMessage({
          icon: 'auto',
          message: Strings.exitText,
          position: 'bottom',
          type: 'default',
        });
        break;

      case 1:
        BackHandler.exitApp();
        break;

      default:
        break;
    }

    return true;
  }

  function renderLogoSection(): ReactElement {
    return (
      <View
        style={[
          HelperStyles.flex(0.5),
          HelperStyles.justifyContentCenteredView('center'),
        ]}>
        <View
          style={[
            HelperStyles.imageView(112, 112),
            HelperStyles.justifyContentCenteredView('center'),
            Styles.squareContainer,
          ]}
        />

        <Image
          resizeMode={'contain'}
          source={Assets.logoRounded}
          style={[
            HelperStyles.justView('position', 'absolute'),
            HelperStyles.imageView(120, 120),
          ]}
        />
      </View>
    );
  }

  function renderTextSection(): ReactElement {
    return (
      <View
        style={[
          HelperStyles.flex(0.25),
          HelperStyles.justifyContentCenteredView('center'),
        ]}>
        <Text
          style={HelperStyles.textView(
            32,
            '400',
            Colors.white,
            'center',
            'none',
          )}>
          {Strings.welcomeTo}
        </Text>

        <Text
          style={HelperStyles.textView(
            32,
            '400',
            Colors.white,
            'center',
            'none',
          )}>
          <Text
            style={HelperStyles.textView(
              32,
              '800',
              Colors.white,
              'center',
              'none',
            )}>
            {Strings.grocery}
          </Text>{' '}
          {Strings.shoppping}
        </Text>
      </View>
    );
  }

  function renderButtonSection(): ReactElement {
    function handleNavigation(initialRouteName: string): void {
      props.navigation.navigate(Strings.signInUp, {initialRouteName});
    }

    return (
      <View
        style={[
          HelperStyles.flex(0.25),
          HelperStyles.justifyContentCenteredView('center'),
        ]}>
        <Button
          containerStyle={Styles.buttonContainer}
          title={Strings.signIn}
          mode={'light'}
          onPress={(): void => {
            handleNavigation(Strings.signIn);
          }}
        />

        <Button
          containerStyle={Styles.buttonContainer}
          title={Strings.signUp}
          onPress={(): void => {
            handleNavigation(Strings.signUp);
          }}
        />
      </View>
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Colors.primary)}>
      <View style={[HelperStyles.flex(1), HelperStyles.screenSubContainer]}>
        {renderLogoSection()}

        {renderTextSection()}

        {renderButtonSection()}
      </View>
    </View>
  );
};

export default OnBoard;
