import React, {FC} from 'react';
import {View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useTheme} from '@react-navigation/native';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Strings from '../../utils/Strings';
import TabBar from '../../components/appComponents/TabBar';
import * as HelperStyles from '../../utils/HelperStyles';

const SignInUp: FC = (props: any) => {
  // Props Variables
  const initialRouteName: string =
    props?.route?.params?.initialRouteName ?? Strings.signIn;

  // SignInUp Variables
  const TopTabs = createMaterialTopTabNavigator();

  // Theme Variables
  const Theme = useTheme().colors;

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <TopTabs.Navigator
        backBehavior={'history'}
        initialRouteName={initialRouteName}
        screenOptions={{animationEnabled: true, swipeEnabled: true}}
        style={[HelperStyles.flex(1), HelperStyles.screenSubContainer]}
        tabBar={props => <TabBar {...props} mode={'space-between'} />}>
        <TopTabs.Screen name={Strings.signIn} component={SignIn} />
        <TopTabs.Screen name={Strings.signUp} component={SignUp} />
      </TopTabs.Navigator>
    </View>
  );
};

export default SignInUp;
