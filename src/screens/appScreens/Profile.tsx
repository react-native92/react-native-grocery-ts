import React, {FC} from 'react';
import {Text, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Colors from '../../utils/Colors';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Profile';
import * as HelperStyles from '../../utils/HelperStyles';

const Profile: FC = (props: any) => {
  // Profile Variables

  // Theme Variables
  const Theme = useTheme().colors;

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <View style={Styles.screenContainer}>
        <View style={HelperStyles.screenSubContainer}>
          <Text
            style={HelperStyles.textView(
              16,
              '600',
              Colors.primary,
              'center',
              'none',
            )}>
            {Strings.profile}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Profile;
