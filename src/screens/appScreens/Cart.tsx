import React, {FC, ReactElement, useCallback, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';
import {connect} from 'react-redux';
import {getCart, updateCart} from '../../configs/jsons/Category';
import {
  CartInterFace,
  CartItemInterFace,
  ResponseInterface,
} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import ImageViewer from '../../components/appComponents/ImageViewer';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NoResponse from '../../components/appComponents/NoResponse';
import SkeletonPlaceholder from '../../containers/SkeletonPlaceholder';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Cart';
import * as HelperStyles from '../../utils/HelperStyles';

const Cart: FC = (props: any) => {
  // Cart Variables
  const [cart, setCart] = useState<any>(null);
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [imageViewer, setImageViewer] = useState<any>(null);

  // Other Variables
  const userId: number | string = props.loginUserInfo?.id ?? null;
  const [refreshing, setRefreshing] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      fetchCart();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function fetchCart(): void {
    const response: ResponseInterface = getCart(userId);

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = [...response.data];

      setCart(responseData);
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message:
          response.status == StatusCodes.NotFound
            ? Strings.info
            : Strings.error,
        type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
      });

      setCart([]);
    }
  }

  function onRefresh(): void {
    setRefreshing(true);

    setCart(null);

    setTimeout((): void => {
      fetchCart();

      setRefreshing(false);
    }, 1000);
  }

  function renderCartItem(itemData: CartItemInterFace): ReactElement {
    const price: any = itemData?.price ?? 0;
    const qty: any = itemData?.qty ?? 0;
    const value: any = price * qty;

    function handleQuantity(qty: number | string): void {
      const stocks: number = itemData.stocks;

      if (Boolean(stocks) && qty <= stocks) {
        const requestData: CartInterFace = {
          itemId: itemData.itemId,
          quantity: qty,
          userId,
        };

        const response: ResponseInterface = updateCart(requestData);

        if (response.status == StatusCodes.Success) {
          showMessage({
            description: response.message,
            icon: 'auto',
            message: Strings.success,
            type: 'success',
          });

          fetchCart();
        } else {
          showMessage({
            description: response.message,
            icon: 'auto',
            message: Strings.error,
            type: 'danger',
          });
        }
      } else {
        showMessage({
          description: Strings.stockLimitExceed,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      }
    }

    function renderDelete(): ReactElement {
      function handleDelete(): void {
        const requestData: CartInterFace = {
          itemId: itemData.itemId,
          quantity: 0,
          userId,
        };

        const response: ResponseInterface = updateCart(requestData);

        if (response.status == StatusCodes.Success) {
          showMessage({
            description: response.message,
            icon: 'auto',
            message: Strings.success,
            type: 'success',
          });

          fetchCart();
        } else {
          showMessage({
            description: response.message,
            icon: 'auto',
            message: Strings.error,
            type: 'danger',
          });
        }
      }

      return (
        <TouchableOpacity
          onPress={(): void => {
            handleDelete();
          }}
          style={Styles.cardDeleteContainer}>
          <MaterialIcons color={Colors.red} name={'delete'} size={24} />
        </TouchableOpacity>
      );
    }

    function renderStockStatus(): ReactElement {
      const status =
        itemData.qty < itemData.stocks ? Strings.inStock : Strings.outOfStock;

      return (
        <View
          style={[
            Styles.cardStockStatusContainer,
            HelperStyles.justView(
              'backgroundColor',
              status == Strings.outOfStock ? Colors.red : Colors.tealGreen,
            ),
          ]}>
          <Text
            style={HelperStyles.textView(
              12,
              '600',
              Colors.white,
              'center',
              'none',
            )}>
            {status}
          </Text>
        </View>
      );
    }

    return (
      <TouchableOpacity
        onPress={(): void => {
          props.navigation.navigate(Strings.item, {
            itemData,
          });
        }}
        style={Styles.cardContainer}>
        <View
          style={[
            HelperStyles.flex(0.275),
            HelperStyles.justifyContentCenteredView('center'),
            HelperStyles.padding(4, 4),
          ]}>
          <TouchableOpacity
            onPress={(): void => {
              setImageViewer(itemData);
            }}
            style={HelperStyles.justifyContentCenteredView('center')}>
            <Image
              onLoadEnd={(): void => {
                setImageLoader(false);
              }}
              onLoadStart={(): void => {
                setImageLoader(true);
              }}
              resizeMode={'contain'}
              source={
                Boolean(itemData.image)
                  ? {uri: itemData.image}
                  : Assets.logoRounded
              }
              style={[
                HelperStyles.imageView(88, 80),
                HelperStyles.justView('borderRadius', 8),
              ]}
            />

            {imageLoader && (
              <ActivityIndicator
                color={Colors.manatee}
                size={24}
                style={Styles.activityIndicatorContainer}
              />
            )}
          </TouchableOpacity>

          <View
            style={[
              HelperStyles.justView('marginTop', 8),
              HelperStyles.justView('marginBottom', 4),
            ]}>
            <View style={Styles.cardQuantityIncDecContainer}>
              <TouchableOpacity
                onPress={(): void => {
                  itemData.qty > 1 && handleQuantity(Number(itemData.qty) - 1);
                }}
                style={Styles.cardQuantityIconContainer}>
                <MaterialIcons color={Colors.white} name={'remove'} size={14} />
              </TouchableOpacity>

              <View
                style={[
                  HelperStyles.justView('width', '40%'),
                  HelperStyles.justifyContentCenteredView('center'),
                  HelperStyles.justView('borderColor', Colors.manatee),
                  HelperStyles.justView('borderWidth', 1),
                ]}>
                <Text
                  style={HelperStyles.textView(
                    10,
                    '800',
                    Colors.stowaway,
                    'center',
                    'none',
                  )}>
                  {itemData.qty ?? 0}
                </Text>
              </View>

              <TouchableOpacity
                onPress={(): void => {
                  handleQuantity(Number(itemData.qty) + 1);
                }}
                style={Styles.cardQuantityIconContainer}>
                <MaterialIcons color={Colors.white} name={'add'} size={14} />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={[
            HelperStyles.flex(0.725),
            HelperStyles.alignItemsCenteredView('flex-start'),
            HelperStyles.padding(8, 4),
          ]}>
          <Text
            ellipsizeMode={'tail'}
            numberOfLines={1}
            style={HelperStyles.textView(
              16,
              '800',
              Colors.ebony,
              'center',
              'none',
            )}>
            {itemData.item}
          </Text>

          <View style={Styles.cardRatingContainer}>
            <AirbnbRating
              count={Strings.ratingsCount}
              defaultRating={itemData.ratings}
              isDisabled={true}
              showRating={false}
              size={20}
            />

            {Boolean(itemData.totalRatings) && (
              <Text
                style={[
                  HelperStyles.textView(
                    14,
                    '600',
                    Colors.manatee,
                    'left',
                    'none',
                  ),
                  HelperStyles.margin(4, 0),
                ]}>
                ({itemData.totalRatings})
              </Text>
            )}
          </View>

          <View style={HelperStyles.margin(0, 4)}>
            <Text
              style={HelperStyles.textView(
                12,
                '600',
                Colors.manatee,
                'center',
                'none',
              )}>
              Rs.{' '}
              <Text
                style={HelperStyles.textView(
                  18,
                  '600',
                  Colors.stowaway,
                  'left',
                  'none',
                )}>
                {parseInt(String(value))}.
              </Text>
              <Text
                style={HelperStyles.textView(
                  14,
                  '600',
                  Colors.stowaway,
                  'left',
                  'none',
                )}>
                {parseFloat(String(value)).toFixed(2).split('.')[1]}{' '}
              </Text>
            </Text>
          </View>

          <View style={Styles.cardWeightContainer}>
            <Text
              style={HelperStyles.textView(
                12,
                '800',
                Colors.white,
                'center',
                'none',
              )}>
              {itemData.weight}
            </Text>
          </View>
        </View>

        {renderDelete()}

        {renderStockStatus()}
      </TouchableOpacity>
    );
  }

  const SkeletonCard: FC<{marginVertical?: number}> = ({
    marginVertical = 8,
  }): ReactElement => {
    return (
      <View style={HelperStyles.margin(0, marginVertical)}>
        <SkeletonPlaceholder>
          <View style={Styles.skeletonCardContainer} />
        </SkeletonPlaceholder>
      </View>
    );
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={(): void => {
              onRefresh();
            }}
          />
        }
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={[HelperStyles.flex(1), HelperStyles.screenSubContainer]}>
          {Boolean(cart) ? (
            Array.isArray(cart) && cart.length != 0 ? (
              cart.map((itemData: CartItemInterFace, index: number) => (
                <View
                  key={index}
                  style={HelperStyles.margin(0, index != 0 ? 8 : 0)}>
                  {renderCartItem(itemData)}
                </View>
              ))
            ) : (
              <View style={Styles.screenContainer}>
                <NoResponse />
              </View>
            )
          ) : (
            <>
              <SkeletonCard marginVertical={0} />

              <SkeletonCard />

              <SkeletonCard />

              <SkeletonCard />

              <SkeletonCard />
            </>
          )}
        </View>
      </ScrollView>

      {Boolean(cart) && Array.isArray(cart) && cart.length != 0 && (
        <Button
          containerStyle={Styles.proceedToBuyContainer}
          textStyle={Styles.proceedToBuyText}
          title={`${Strings.proceedToBuy} (${cart.length} items)`}
        />
      )}

      <ImageViewer
        onClose={(): void => {
          setImageViewer(null);
        }}
        onRequestClose={(): void => {
          setImageViewer(null);
        }}
        title={imageViewer?.item}
        url={imageViewer?.image}
        visible={Boolean(imageViewer)}
      />
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(Cart);
