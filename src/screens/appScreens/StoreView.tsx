import React, {
  FC,
  ReactElement,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {
  ActivityIndicator,
  Image,
  ScrollView,
  Share,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';
import {connect} from 'react-redux';
import {StoreInterface} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {useHeaderHeight} from '@react-navigation/elements';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import ImageViewer from '../../components/appComponents/ImageViewer';
import Indicator from '../../components/appComponents/Indicator';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment, {Moment} from 'moment';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/StoreView';
import Tts from 'react-native-tts';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const StoreView: FC = (props: any) => {
  // Props Variables
  const storeData: StoreInterface = props?.route?.params?.storeData ?? null;

  // StoreView Variables
  const [ttsStatus, setTtsStatus] = useState<boolean>(false);
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [imageViewerStatus, setImageViewerStatus] = useState<boolean>(false);

  // Other Variables
  const headerHeight: number = useHeaderHeight();
  const userId: number | string = props.loginUserInfo?.id ?? null;

  // Theme Variables
  const Theme = useTheme().colors;

  useLayoutEffect((): void => {
    props.navigation.setOptions({
      headerTitle: storeData?.name ?? Strings.storeView,
    });
  }, []);

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      init();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function init(): void {
    initTtsAddListener();

    Tts.getInitStatus().then(initTts);

    Tts.setDefaultLanguage('en-IE');

    Tts.setDefaultPitch(1);

    Tts.setDefaultRate(0.5);

    Tts.setDucking(true);
  }

  function initTtsAddListener(): void {
    Tts.addEventListener('tts-start', (): void => setTtsStatus(true));

    Tts.addEventListener('tts-finish', (): void => setTtsStatus(false));

    Tts.addEventListener('tts-cancel', (): void => setTtsStatus(false));
  }

  async function initTts(): Promise<void> {
    const voices = await Tts.voices();

    await Tts.setDefaultVoice(voices[0].id);
  }

  function renderImage(): ReactElement {
    return (
      <View
        style={HelperStyles.imageView(Helpers.windowHeight * 0.375, '100%')}>
        <Image
          onLoadEnd={(): void => {
            setImageLoader(false);
          }}
          onLoadStart={(): void => {
            setImageLoader(true);
          }}
          resizeMode={'cover'}
          source={
            Boolean(storeData?.image) ? {uri: storeData.image} : Assets.logo
          }
          style={HelperStyles.imageView('100%', '100%')}
        />

        {imageLoader && (
          <ActivityIndicator
            color={Colors.primary}
            size={'large'}
            style={Styles.activityIndicatorContainer}
          />
        )}

        <TouchableOpacity
          onPress={(): void => {
            setImageViewerStatus(!imageViewerStatus);
          }}
          style={Styles.callToActionContainer}>
          <MaterialIcons
            color={Colors.lightText}
            name={'call-to-action'}
            size={28}
          />

          <ImageViewer
            onClose={(): void => {
              setImageViewerStatus(!imageViewerStatus);
            }}
            onRequestClose={(): void => {
              setImageViewerStatus(!imageViewerStatus);
            }}
            title={storeData.name}
            url={storeData.image}
            visible={imageViewerStatus}
          />
        </TouchableOpacity>
      </View>
    );
  }

  function renderItem(actionFor: string): ReactElement {
    let icon: string = '',
      isLinking: boolean = false,
      value: any = null;

    switch (actionFor) {
      case Strings.address:
        icon = 'location-on';
        value =
          storeData?.address ??
          `${Strings.add} ${Strings.address.toLowerCase()}`;
        break;

      case Strings.mobileNumber:
        icon = 'phone';
        isLinking = Boolean(storeData?.mobileNumber);
        value =
          storeData?.mobileNumber ??
          `${Strings.add} ${Strings.mobileNumber.toLowerCase()}`;
        break;

      case Strings.storeStatus:
        icon = 'schedule';
        value = storeData?.address ?? Strings.yetToBeOpened;
        break;

      case Strings.website:
        icon = 'public';
        isLinking = Boolean(storeData?.website);
        value =
          storeData?.website ??
          `${Strings.add} ${Strings.website.toLowerCase()}`;
        break;

      default:
        break;
    }

    function handleIconColor(): string {
      return Boolean(value) &&
        !value.includes(Strings.add) &&
        !value.includes(Strings.yet)
        ? Colors.royalBlue
        : Colors.manatee;
    }

    function renderValue(): ReactElement {
      function handleStoreStatus(
        openTime: string | undefined,
        closeTime: string | undefined,
      ): string {
        const beginningTime: Moment = moment(openTime, Strings.H2m2);
        const endTime: Moment = moment(closeTime, Strings.H2m2);

        if (Boolean(moment().isBetween(beginningTime, endTime))) {
          return Strings.open;
        } else {
          return Strings.closed;
        }
      }

      function handleOpenCloseTime(
        openTime: string | undefined,
        closeTime: string | undefined,
      ): string {
        const beginningTime: Moment = moment(openTime, Strings.H2m2);
        const endTime: Moment = moment(closeTime, Strings.H2m2);

        if (Boolean(moment().isBetween(beginningTime, endTime))) {
          return ` - Closes ${closeTime}`;
        } else {
          return ` - Opens ${openTime}`;
        }
      }

      function handleTextColor(): string {
        return Boolean(value) &&
          !value.includes(Strings.add) &&
          !value.includes(Strings.yet)
          ? Theme.text
          : Colors.manatee;
      }

      switch (actionFor) {
        case Strings.storeStatus:
          return Boolean(storeData?.openTime) &&
            Boolean(storeData?.closeTime) ? (
            <Text
              style={HelperStyles.textView(
                14,
                '400',
                handleStoreStatus(storeData.openTime, storeData.closeTime) ==
                  Strings.open
                  ? Colors.tealGreen
                  : Colors.red,
                'center',
                'none',
              )}>
              {handleStoreStatus(storeData.openTime, storeData.closeTime)}
              <Text
                style={HelperStyles.textView(
                  14,
                  '400',
                  Colors.lightText,
                  'center',
                  'none',
                )}>
                {handleOpenCloseTime(storeData.openTime, storeData.closeTime)}
              </Text>
            </Text>
          ) : (
            <Text
              style={HelperStyles.textView(
                14,
                '400',
                Colors.manatee,
                'center',
                'none',
              )}>
              {Strings.yetToBeOpened}
            </Text>
          );

        default:
          return (
            <Text
              style={HelperStyles.textView(
                14,
                '400',
                handleTextColor(),
                'center',
                'none',
              )}>
              {value}
            </Text>
          );
      }
    }

    function speak(): void {
      Tts.stop();

      Tts.speak(value);
    }

    function stop(): void {
      Tts.stop();
    }

    return Boolean(icon) && Boolean(value) ? (
      <View style={Styles.itemContainer}>
        <TouchableOpacity
          disabled={!isLinking}
          onPress={(): void => {
            switch (actionFor) {
              case Strings.mobileNumber:
                Helpers.handleLinking(`tel:+91${storeData.mobileNumber}`);
                break;

              case Strings.website:
                Helpers.handleLinking(storeData?.website ?? '');
                break;

              default:
                break;
            }
          }}
          style={Styles.itemSubContainer}>
          <View
            style={[
              HelperStyles.flex(0.0875),
              HelperStyles.justifyContentCenteredView('center'),
            ]}>
            <MaterialIcons
              color={handleIconColor()}
              name={icon}
              size={22}
              style={HelperStyles.padding(4, 4)}
            />
          </View>

          <View
            style={[
              HelperStyles.flex(actionFor == Strings.address ? 0.7875 : 0.8875),
              HelperStyles.alignItemsCenteredView('flex-start'),
              HelperStyles.padding(4, 4),
            ]}>
            {renderValue()}
          </View>

          {actionFor == Strings.address && (
            <View
              style={[
                HelperStyles.flex(0.0875),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              <MaterialIcons
                color={Colors.manatee}
                name={ttsStatus ? 'volume-up' : 'volume-off'}
                onPress={(): void => {
                  setTtsStatus(!ttsStatus);

                  ttsStatus ? stop() : speak();
                }}
                size={20}
                style={HelperStyles.padding(4, 4)}
              />
            </View>
          )}
        </TouchableOpacity>
      </View>
    ) : (
      <></>
    );
  }

  function renderRatings(): ReactElement {
    return (
      <View style={[Styles.ratingsContainer, HelperStyles.padding(16, 16)]}>
        <View
          style={[
            HelperStyles.flexDirection('row'),
            HelperStyles.justView('justifyContent', 'space-between'),
          ]}>
          <View
            style={[
              HelperStyles.flex(0.75),
              HelperStyles.flexDirection('row'),
              HelperStyles.justifyContentCenteredView('flex-start'),
            ]}>
            <Text
              style={HelperStyles.textView(
                16,
                '600',
                Theme.text,
                'center',
                'none',
              )}>
              {Strings.ratings}
            </Text>

            <Text
              style={HelperStyles.textView(
                14,
                '400',
                Colors.manatee,
                'center',
                'none',
              )}>
              {`\t(${storeData?.totalRatings ?? 0})`}
            </Text>
          </View>

          <View
            style={[
              HelperStyles.flex(0.25),
              HelperStyles.alignItemsCenteredView('flex-end'),
            ]}>
            <Button
              containerStyle={Styles.reviewsButtonContainer}
              mode={'light'}
              onPress={(): void => {
                props.navigation.navigate(Strings.storeReviews, {storeData});
              }}
              textStyle={HelperStyles.textView(
                12,
                'bold',
                Colors.primary,
                'center',
                'none',
              )}
              title={Strings.reviews}
            />
          </View>
        </View>

        <AirbnbRating
          count={5}
          defaultRating={parseFloat(String(storeData?.ratings ?? 0))}
          isDisabled={true}
          ratingContainerStyle={HelperStyles.margin(8, 8)}
          showRating={false}
          starContainerStyle={HelperStyles.margin(8, 8)}
          size={36}
        />
      </View>
    );
  }

  function renderFeatures(): ReactElement {
    const checkPaymentFeatures =
      Boolean(storeData?.payments) &&
      Array.isArray(storeData?.payments) &&
      storeData.payments.length != 0;

    const checkServiceFeatures =
      Boolean(storeData?.services) &&
      Array.isArray(storeData?.services) &&
      storeData.services.length != 0;

    function renderSection(section: string, sectionItems: any[]): ReactElement {
      return (
        <View style={HelperStyles.padding(0, 8)}>
          <Text
            style={HelperStyles.textView(
              14,
              '600',
              Colors.ebony,
              'left',
              'none',
            )}>
            {section}
          </Text>

          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
            }}>
            {sectionItems.map((lol: string, index: number) => (
              <View
                key={index}
                style={[
                  HelperStyles.flexDirection('row'),
                  HelperStyles.justView('marginLeft', index != 0 ? 16 : 0),
                  HelperStyles.justView('marginTop', 16),
                ]}>
                <MaterialIcons
                  color={Colors.tealGreen}
                  name={'done'}
                  size={20}
                />

                <Text
                  style={[
                    HelperStyles.textView(
                      14,
                      '400',
                      Theme.text,
                      'left',
                      'none',
                    ),
                    HelperStyles.justView('marginLeft', 4),
                  ]}>
                  {lol}
                </Text>
              </View>
            ))}
          </View>
        </View>
      );
    }

    return Boolean(checkPaymentFeatures) || Boolean(checkServiceFeatures) ? (
      <View style={[Styles.featuresContainer, HelperStyles.padding(16, 16)]}>
        <Text
          style={HelperStyles.textView(16, '600', Theme.text, 'left', 'none')}>
          {Strings.features}
        </Text>

        <View style={HelperStyles.margin(0, 8)}>
          {Boolean(storeData?.services) &&
            Array.isArray(storeData?.services) &&
            storeData.services.length != 0 &&
            renderSection(Strings.serviceOptions, storeData.services)}

          {Boolean(storeData?.payments) &&
            Array.isArray(storeData?.payments) &&
            storeData.payments.length != 0 &&
            renderSection(Strings.payments, storeData.payments)}
        </View>
      </View>
    ) : (
      <></>
    );
  }

  function renderFooter(): ReactElement {
    function renderDirection(): ReactElement {
      return (
        <View style={Styles.footerIconContainer}>
          <TouchableOpacity
            onPress={(): void => {
              Helpers.handleLinking(
                `${Strings.googleMap}${storeData.coordinates.latitude}, ${storeData.coordinates.longitude}`,
              );
            }}
            style={HelperStyles.padding(4, 4)}>
            <MaterialIcons color={Colors.white} name={'directions'} size={24} />
          </TouchableOpacity>
        </View>
      );
    }

    function renderDrive(): ReactElement {
      return (
        <View style={Styles.footerIconContainer}>
          <TouchableOpacity
            onPress={(): void => {
              Helpers.handleLinking(
                `${Strings.googleMapNavigation}${storeData.coordinates.latitude}, ${storeData.coordinates.longitude}`,
              );
            }}
            style={HelperStyles.padding(4, 4)}>
            <MaterialIcons color={Colors.white} name={'navigation'} size={24} />
          </TouchableOpacity>
        </View>
      );
    }

    function renderShare(): ReactElement {
      async function onShare(storeData: StoreInterface): Promise<void> {
        const url: string = `${Strings.googleMap}${storeData.coordinates.latitude}, ${storeData.coordinates.longitude}`;

        Share.share({
          message: `${storeData.name}\n\nClick the link below to reach the store: ${url}`,
          title: Strings.storeLocation,
          url,
        }).catch((): void => {
          showMessage({
            description: 'Sorry! Unable to share the store location!',
            icon: 'auto',
            message: Strings.error,
            type: 'danger',
          });
        });
      }

      return (
        <Button
          containerStyle={Styles.shareButtonContainer}
          isImage={true}
          mode={'light'}
          renderImage={(): ReactElement => (
            <MaterialIcons
              color={Colors.primary}
              name={'share'}
              size={16}
              style={HelperStyles.justView('marginRight', 4)}
            />
          )}
          onPress={(): void => {
            onShare(storeData);
          }}
          textStyle={HelperStyles.textView(
            14,
            '600',
            Colors.primary,
            'center',
            'none',
          )}
          title={Strings.share}
        />
      );
    }

    return (
      <View
        style={[
          HelperStyles.justView('height', headerHeight),
          Styles.footerContainer,
          HelperStyles.justView('backgroundColor', Theme.background),
        ]}>
        {renderDirection()}

        {renderDrive()}

        {renderShare()}
      </View>
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        {Boolean(storeData) ? (
          <>
            {renderImage()}

            {renderItem(Strings.address)}

            {renderItem(Strings.storeStatus)}

            {renderItem(Strings.mobileNumber)}

            {renderItem(Strings.website)}

            {renderRatings()}

            {renderFeatures()}
          </>
        ) : (
          <Indicator />
        )}
      </ScrollView>

      {renderFooter()}
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(StoreView);
