import React, {
  FC,
  Fragment,
  ReactElement,
  useCallback,
  useRef,
  useState,
} from 'react';
import {
  Alert,
  Animated,
  BackHandler,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  CategoryInterface,
  CategoryItemInterface,
  FavoriteInterFace,
  ResponseInterface,
} from '../../configs/ts/Interfaces';
import {connect} from 'react-redux';
import {
  getAppDeals,
  getMemberDeals,
  updateFavorite,
} from '../../configs/jsons/Category';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import CardItem from '../../components/appComponents/CardItem';
import Colors from '../../utils/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SkeletonPlaceholder from '../../containers/SkeletonPlaceholder';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Store';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const Store: FC = (props: any) => {
  // Store Variables
  const [categories, setCategories] = useState<any>(null);
  const [expand, setExpand] = useState<boolean>(false);
  const [memberDeals, setMemberDeals] = useState<any>(null);
  const [appDeals, setAppDeals] = useState<any>(null);

  // Other Variables
  const userId: number | string = props.loginUserInfo?.id ?? null;
  const [refreshing, setRefreshing] = useState<boolean>(false);

  // Ref Variables
  const animationHeight: Animated.Value = useRef<Animated.Value>(
    new Animated.Value(0),
  ).current;

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      BackHandler.addEventListener('hardwareBackPress', backAction);

      return (): void => {
        isFocus = false;

        BackHandler.removeEventListener('hardwareBackPress', backAction);
      };
    }, []),
  );

  function backAction(): boolean {
    Alert.alert(Strings.exitAccount, Strings.exitAlertText, [
      {
        text: Strings.cancel,
        onPress: (): void => {},
        style: 'cancel',
      },
      {
        text: Strings.confirm,
        onPress: (): void => {
          BackHandler.exitApp();
        },
      },
    ]);

    return true;
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      organizeCategories();

      fetchMemberDeals();

      fetchAppDeals();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function organizeCategories(): void {
    const categories: any[] = props.categories;

    if (Array.isArray(categories) && categories.length != 0) {
      let helperSum: number = 0,
        helperOuterArray: any[] = [];

      for (let i = 0; i < categories.length / Strings.categoryRowItems; i++) {
        let helperInnerArray: any[] = [];

        for (let j = 0; j < Strings.categoryRowItems; j++) {
          const element: any = categories[helperSum];

          Boolean(element) && helperInnerArray.push(element);

          helperSum++;
        }

        helperOuterArray.push(helperInnerArray);
      }

      setCategories(helperOuterArray);
    } else {
      setCategories([]);
    }
  }

  function fetchMemberDeals(): void {
    const response: ResponseInterface = getMemberDeals();

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = [...response.data];

      setMemberDeals(responseData.slice(0, Strings.categorySlice));
    } else {
      setMemberDeals([]);
    }
  }

  function fetchAppDeals(): void {
    const response: ResponseInterface = getAppDeals();

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = [...response.data];

      setAppDeals(responseData.slice(0, Strings.categorySlice));
    } else {
      setAppDeals([]);
    }
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      if (expand) {
        handleExpand();
      } else {
        handleCollapse();
      }

      return (): void => {
        isFocus = false;
      };
    }, [expand]),
  );

  function handleCollapse(): void {
    Animated.timing(animationHeight, {
      duration: 1000,
      toValue: Helpers.windowHeight * 0.2375,
      useNativeDriver: false,
    }).start();
  }

  function handleExpand(): void {
    Animated.timing(animationHeight, {
      duration: 1000,
      toValue: Helpers.windowHeight * 0.625,
      useNativeDriver: false,
    }).start();
  }

  function onRefresh(): void {
    setRefreshing(true);

    setMemberDeals(null);

    setAppDeals(null);

    setTimeout((): void => {
      fetchMemberDeals();

      fetchAppDeals();

      setRefreshing(false);
    }, 1000);
  }

  function renderCategories(): ReactElement {
    function renderCategory(
      categoryData: CategoryInterface,
      index: number,
    ): ReactElement {
      return (
        <TouchableOpacity
          key={index}
          onPress={(): void => {
            props.navigation.navigate(Strings.subCategory, {categoryData});
          }}
          style={Styles.categoryContainer}>
          <View style={Styles.categoryIconContainer}>
            {categoryData.iconType == Strings.fontAwesomeIcons ? (
              <FontAwesomeIcons
                color={Colors.primary}
                name={categoryData.icon}
                size={24}
              />
            ) : (
              <MaterialIcons
                color={Colors.primary}
                name={categoryData.icon}
                size={24}
              />
            )}
          </View>

          <View style={HelperStyles.margin(0, 4)}>
            <Text
              style={HelperStyles.textView(
                12,
                '800',
                Colors.manatee,
                'center',
                'capitalize',
              )}>
              {categoryData.category}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }

    return (
      <View style={HelperStyles.justView('overflow', 'hidden')}>
        <Animated.View
          style={[
            HelperStyles.justView('maxHeight', animationHeight),
            Styles.categoriesContainer,
            {backgroundColor: Theme.card},
          ]}>
          <ScrollView
            contentContainerStyle={HelperStyles.flexGrow(1)}
            keyboardShouldPersistTaps={'handled'}
            nestedScrollEnabled={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={HelperStyles.screenSubContainer}>
            <Text
              style={HelperStyles.textView(
                14,
                '600',
                themeScheme == Strings.dark ? Colors.lightText : Colors.ebony,
                'left',
                'none',
              )}>
              {Strings.allCategories}
            </Text>

            <View style={HelperStyles.margin(0, 8)}>
              {Boolean(categories) &&
                categories.map(
                  (rowData: any, rowIndex: number): ReactElement => (
                    <View
                      key={rowIndex}
                      style={[
                        Styles.categoriesRowContainer,
                        HelperStyles.margin(0, rowIndex != 0 ? 8 : 0),
                      ]}>
                      {rowData.map(
                        (
                          categoryData: CategoryInterface,
                          index: number,
                        ): ReactElement => renderCategory(categoryData, index),
                      )}
                    </View>
                  ),
                )}
            </View>
          </ScrollView>
        </Animated.View>

        {Boolean(categories) && (
          <TouchableOpacity
            onPress={(): void => {
              setExpand(!expand);
            }}
            style={Styles.knobContainer}
          />
        )}
      </View>
    );
  }

  function renderMemberDeals(): ReactElement {
    return (
      <Fragment>
        <View
          style={[
            HelperStyles.flexDirection('row'),
            HelperStyles.justView('justifyContent', 'space-between'),
          ]}>
          <View
            style={[
              HelperStyles.flex(0.775),
              HelperStyles.alignItemsCenteredView('flex-start'),
            ]}>
            <Text
              style={HelperStyles.textView(
                14,
                '600',
                themeScheme == Strings.dark ? Colors.lightText : Colors.ebony,
                'left',
                'none',
              )}>
              {Strings.groceryAppMemberDeals}
            </Text>
          </View>

          <View
            style={[
              HelperStyles.flex(0.225),
              HelperStyles.justView('justifyContent', 'center'),
            ]}>
            <TouchableOpacity
              onPress={(): void => {
                props.navigation.navigate(Strings.subCategoryItems, {
                  from: Strings.groceryAppMemberDeals,
                });
              }}
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justifyContentCenteredView('flex-end'),
              ]}>
              <Text
                style={HelperStyles.textView(
                  14,
                  '600',
                  themeScheme == Strings.dark ? Colors.ebony : Colors.manatee,
                  'center',
                  'none',
                )}>
                {Strings.viewAll}
              </Text>

              <MaterialIcons
                color={
                  themeScheme == Strings.dark ? Colors.ebony : Colors.manatee
                }
                name={'chevron-right'}
                size={16}
                style={HelperStyles.padding(4, 4)}
              />
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView
          contentContainerStyle={HelperStyles.flexGrow(1)}
          horizontal={true}
          keyboardShouldPersistTaps={'handled'}
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={HelperStyles.margin(0, 12)}>
          <View
            style={[
              HelperStyles.flexDirection('row'),
              HelperStyles.justifyContentCenteredView('flex-start'),
            ]}>
            {Boolean(memberDeals) ? (
              memberDeals.map((memberDealData: any, index: number) => (
                <View
                  key={index}
                  style={[
                    HelperStyles.justView('marginLeft', index != 0 ? 8 : 0),
                    HelperStyles.padding(0, 4),
                  ]}>
                  <CardItem
                    data={memberDealData}
                    onFavorite={(): void => {
                      handleFavorite(
                        memberDealData,
                        Strings.groceryAppMemberDeals,
                      );
                    }}
                    onPress={(): void => {
                      props.navigation.navigate(Strings.item, {
                        itemData: {
                          ...memberDealData,
                          from: Strings.groceryAppMemberDeals,
                        },
                      });
                    }}
                    style={HelperStyles.justView('width', 140)}
                  />
                </View>
              ))
            ) : (
              <>
                <SkeletonCard marginLeft={0} />

                <SkeletonCard />

                <SkeletonCard />

                <SkeletonCard />

                <SkeletonCard />
              </>
            )}
          </View>
        </ScrollView>
      </Fragment>
    );
  }

  function renderAppDeals(): ReactElement {
    return (
      <Fragment>
        <View
          style={[
            HelperStyles.flexDirection('row'),
            HelperStyles.justView('justifyContent', 'space-between'),
          ]}>
          <View
            style={[
              HelperStyles.flex(0.775),
              HelperStyles.alignItemsCenteredView('flex-start'),
            ]}>
            <Text
              style={HelperStyles.textView(
                14,
                '600',
                themeScheme == Strings.dark ? Colors.lightText : Colors.ebony,
                'left',
                'none',
              )}>
              {Strings.groceryAppDeals}
            </Text>
          </View>

          <View
            style={[
              HelperStyles.flex(0.225),
              HelperStyles.justView('justifyContent', 'center'),
            ]}>
            <TouchableOpacity
              onPress={(): void => {
                props.navigation.navigate(Strings.subCategoryItems, {
                  from: Strings.groceryAppDeals,
                });
              }}
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justifyContentCenteredView('flex-end'),
              ]}>
              <Text
                style={HelperStyles.textView(
                  14,
                  '600',
                  themeScheme == Strings.dark ? Colors.ebony : Colors.manatee,
                  'center',
                  'none',
                )}>
                {Strings.viewAll}
              </Text>

              <MaterialIcons
                color={
                  themeScheme == Strings.dark ? Colors.ebony : Colors.manatee
                }
                name={'chevron-right'}
                size={16}
                style={HelperStyles.padding(4, 4)}
              />
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView
          contentContainerStyle={HelperStyles.flexGrow(1)}
          horizontal={true}
          keyboardShouldPersistTaps={'handled'}
          nestedScrollEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={HelperStyles.margin(0, 12)}>
          <View
            style={[
              HelperStyles.flexDirection('row'),
              HelperStyles.justifyContentCenteredView('flex-start'),
            ]}>
            {Boolean(appDeals) ? (
              appDeals.map((appDealData: any, index: number) => (
                <View
                  key={index}
                  style={[
                    HelperStyles.justView('marginLeft', index != 0 ? 8 : 0),
                    HelperStyles.padding(0, 4),
                  ]}>
                  <CardItem
                    data={appDealData}
                    onFavorite={(): void => {
                      handleFavorite(appDealData, Strings.groceryAppDeals);
                    }}
                    onPress={(): void => {
                      props.navigation.navigate(Strings.item, {
                        itemData: {
                          ...appDealData,
                          from: Strings.groceryAppDeals,
                        },
                      });
                    }}
                    style={HelperStyles.justView('width', 140)}
                  />
                </View>
              ))
            ) : (
              <>
                <SkeletonCard marginLeft={0} />

                <SkeletonCard />

                <SkeletonCard />

                <SkeletonCard />

                <SkeletonCard />
              </>
            )}
          </View>
        </ScrollView>
      </Fragment>
    );
  }

  function handleFavorite(data: CategoryItemInterface, action: string): void {
    const requestData: FavoriteInterFace = {
      itemId: data?.itemId,
      userId,
    };

    const response: ResponseInterface = updateFavorite(requestData);

    if (response.status == StatusCodes.Success) {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.success,
        type: 'success',
      });

      switch (action) {
        case Strings.groceryAppDeals:
          fetchAppDeals();
          break;

        case Strings.groceryAppMemberDeals:
          fetchMemberDeals();
          break;

        default:
          break;
      }
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  const SkeletonCard: FC<{marginLeft?: number}> = ({
    marginLeft = 8,
  }): ReactElement => {
    return (
      <View style={HelperStyles.justView('marginLeft', marginLeft)}>
        <SkeletonPlaceholder>
          <View style={Styles.skeletonCardContainer} />
        </SkeletonPlaceholder>
      </View>
    );
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={(): void => {
              onRefresh();
            }}
          />
        }
        scrollEnabled={!expand}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        {renderCategories()}

        <View style={HelperStyles.margin(16, 8)}>
          {renderMemberDeals()}

          {renderAppDeals()}
        </View>
      </ScrollView>
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    categories: state?.app?.categories ?? [],
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(Store);
