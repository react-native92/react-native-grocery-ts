import React, {FC, ReactElement, useCallback, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  RefreshControl,
  ScrollView,
  Share,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';
import {fetchAllStores} from '../../configs/jsons/Stores';
import {ResponseInterface, StoreInterface} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import ImageViewer from '../../components/appComponents/ImageViewer';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import moment, {Moment} from 'moment';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SkeletonPlaceholder from '../../containers/SkeletonPlaceholder';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/StoreLocator';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const StoreLocator: FC = (props: any) => {
  // StoreLocator Variables
  const [region, setRegion] = useState<any>(null);
  const [stores, setStores] = useState<any>(null);
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [selectedStore, setSelectedStore] = useState<any>(null);
  const [imageViewerStatus, setImageViewerStatus] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  // Other Variables
  const [refreshing, setRefreshing] = useState<boolean>(false);

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      fetchStores();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function fetchStores(): void {
    const response: ResponseInterface = fetchAllStores();

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = [...response.data];

      if (responseData.length != 0) {
        setRegion(responseData[0].coordinates);

        setSelectedStore(responseData[0]);
      } else {
        setRegion(null);

        setSelectedStore(null);
      }

      setStores(responseData);
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message:
          response.status == StatusCodes.NotFound
            ? Strings.info
            : Strings.error,
        type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
      });

      setRegion(null);

      setSelectedStore(null);

      setStores([]);
    }
  }

  function onRefresh(): void {
    setRefreshing(true);

    setRegion(null);

    setSelectedStore(null);

    setStores(null);

    setTimeout((): void => {
      fetchStores();

      setRefreshing(false);
    }, 1000);
  }

  function renderStoreCard(
    storeData: StoreInterface,
    index: number,
  ): ReactElement {
    function handleStoreStatus(
      openTime: string | undefined,
      closeTime: string | undefined,
    ): string {
      const beginningTime: Moment = moment(openTime, Strings.H2m2);
      const endTime: Moment = moment(closeTime, Strings.H2m2);

      if (Boolean(moment().isBetween(beginningTime, endTime))) {
        return Strings.open;
      } else {
        return Strings.closed;
      }
    }

    function handleOpenCloseTime(
      openTime: string | undefined,
      closeTime: string | undefined,
    ): string {
      const beginningTime: Moment = moment(openTime, Strings.H2m2);
      const endTime: Moment = moment(closeTime, Strings.H2m2);

      if (Boolean(moment().isBetween(beginningTime, endTime))) {
        return ` - Closes ${closeTime}`;
      } else {
        return ` - Opens ${openTime}`;
      }
    }

    async function onShare(storeData: StoreInterface): Promise<void> {
      const url: string = `${Strings.googleMap}${storeData.coordinates.latitude}, ${storeData.coordinates.longitude}`;

      Share.share({
        message: `${storeData.name}\n\nClick the link below to reach the store: ${url}`,
        title: Strings.storeLocation,
        url,
      }).catch((): void => {
        showMessage({
          description: 'Sorry! Unable to share the store location!',
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      });
    }

    return (
      <View
        style={[
          Styles.storeContainer,
          HelperStyles.justView(
            'borderColor',
            Boolean(selectedStore) &&
              selectedStore?.coordinates == storeData.coordinates
              ? Colors.primary75
              : Colors.transparent,
          ),
          HelperStyles.justView('marginLeft', index != 0 ? 16 : 0),
        ]}>
        <View style={Styles.storeInfoContainer}>
          <View
            style={[
              HelperStyles.flex(0.8),
              HelperStyles.justView('alignItems', 'flex-start'),
            ]}>
            <Text
              style={HelperStyles.textView(
                14,
                '600',
                Colors.black,
                'center',
                'none',
              )}>
              {storeData.name}
            </Text>

            <View style={Styles.storeRatingContainer}>
              {Boolean(storeData?.ratings) ? (
                <>
                  <Text
                    style={HelperStyles.textView(
                      12,
                      '600',
                      Colors.ebony,
                      'center',
                      'none',
                    )}>
                    {storeData.ratings}
                  </Text>

                  <AirbnbRating
                    count={5}
                    defaultRating={parseFloat(String(storeData.ratings))}
                    isDisabled={true}
                    ratingContainerStyle={HelperStyles.margin(4, 0)}
                    showRating={false}
                    size={12}
                  />

                  {Boolean(storeData?.totalRatings) && (
                    <Text
                      style={HelperStyles.textView(
                        12,
                        '600',
                        Colors.manatee,
                        'center',
                        'none',
                      )}>
                      ({storeData.totalRatings})
                    </Text>
                  )}
                </>
              ) : (
                <Text
                  style={HelperStyles.textView(
                    12,
                    '600',
                    Colors.stowaway,
                    'center',
                    'none',
                  )}>
                  {Strings.noRatings}
                </Text>
              )}
            </View>

            <View style={HelperStyles.justView('marginTop', 2)}>
              <Text
                ellipsizeMode={'tail'}
                numberOfLines={1}
                style={HelperStyles.textView(
                  12,
                  '400',
                  Colors.manatee,
                  'center',
                  'none',
                )}>
                {storeData.address}
              </Text>
            </View>

            <View style={HelperStyles.justView('marginTop', 2)}>
              {Boolean(storeData?.openTime) && Boolean(storeData?.closeTime) ? (
                <Text
                  style={HelperStyles.textView(
                    12,
                    '400',
                    handleStoreStatus(
                      storeData.openTime,
                      storeData.closeTime,
                    ) == Strings.open
                      ? Colors.tealGreen
                      : Colors.red,
                    'center',
                    'none',
                  )}>
                  {handleStoreStatus(storeData.openTime, storeData.closeTime)}
                  <Text
                    style={HelperStyles.textView(
                      12,
                      '400',
                      Colors.manatee,
                      'center',
                      'none',
                    )}>
                    {handleOpenCloseTime(
                      storeData.openTime,
                      storeData.closeTime,
                    )}
                  </Text>
                </Text>
              ) : (
                <Text
                  style={HelperStyles.textView(
                    12,
                    '400',
                    Colors.manatee,
                    'center',
                    'none',
                  )}>
                  {Strings.yetToBeOpened}
                </Text>
              )}
            </View>
          </View>

          <TouchableOpacity
            onPress={(): void => {
              setImageViewerStatus(!imageViewerStatus);
            }}
            style={[
              HelperStyles.flex(0.2),
              HelperStyles.justifyContentCenteredView('center'),
            ]}>
            <Image
              onLoadEnd={(): void => {
                setImageLoader(false);
              }}
              onLoadStart={(): void => {
                setImageLoader(true);
              }}
              resizeMode={'cover'}
              source={
                Boolean(storeData?.image)
                  ? {uri: storeData.image}
                  : Assets.logoRounded
              }
              style={[
                HelperStyles.imageView(60, 60),
                HelperStyles.justView('borderRadius', 8),
              ]}
            />

            {imageLoader && (
              <ActivityIndicator
                color={Colors.manatee}
                size={24}
                style={HelperStyles.justView('position', 'absolute')}
              />
            )}

            <ImageViewer
              onClose={(): void => {
                setImageViewerStatus(!imageViewerStatus);
              }}
              onRequestClose={(): void => {
                setImageViewerStatus(!imageViewerStatus);
              }}
              title={storeData.name}
              url={storeData.image}
              visible={imageViewerStatus}
            />
          </TouchableOpacity>
        </View>

        <View style={Styles.storeButtonContainer}>
          <Button
            containerStyle={Styles.solidButtonContainer}
            isImage={true}
            renderImage={(): ReactElement => (
              <MaterialIcons
                color={Colors.white}
                name={'directions'}
                size={16}
                style={HelperStyles.justView('marginRight', 4)}
              />
            )}
            onPress={(): void => {
              Helpers.handleLinking(
                `${Strings.googleMap}${storeData.coordinates.latitude}, ${storeData.coordinates.longitude}`,
              );
            }}
            textStyle={HelperStyles.textView(
              12,
              '600',
              Colors.white,
              'center',
              'none',
            )}
            title={Strings.directions}
          />

          {Boolean(storeData?.mobileNumber) && (
            <Button
              containerStyle={Styles.lightButtonContainer}
              isImage={true}
              mode={'light'}
              onPress={(): void => {
                Helpers.handleLinking(`tel:+91${storeData.mobileNumber}`);
              }}
              renderImage={(): ReactElement => (
                <MaterialIcons
                  color={Colors.primary}
                  name={'call'}
                  size={16}
                  style={HelperStyles.justView('marginRight', 4)}
                />
              )}
              textStyle={HelperStyles.textView(
                12,
                '600',
                Colors.primary,
                'center',
                'none',
              )}
              title={Strings.call}
            />
          )}

          <Button
            containerStyle={Styles.lightButtonContainer}
            isImage={true}
            mode={'light'}
            renderImage={(): ReactElement => (
              <MaterialIcons
                color={Colors.primary}
                name={'share'}
                size={16}
                style={HelperStyles.justView('marginRight', 4)}
              />
            )}
            onPress={(): void => {
              onShare(storeData);
            }}
            textStyle={HelperStyles.textView(
              12,
              '600',
              Colors.primary,
              'center',
              'none',
            )}
            title={Strings.share}
          />
        </View>
      </View>
    );
  }

  const SkeletonCard: FC<{marginLeft?: number}> = ({
    marginLeft = 16,
  }): ReactElement => {
    return (
      <View style={HelperStyles.justView('marginLeft', marginLeft)}>
        <SkeletonPlaceholder>
          <View style={Styles.skeletonCardContainer} />
        </SkeletonPlaceholder>
      </View>
    );
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        refreshControl={
          <RefreshControl
            tintColor={Colors.primary}
            refreshing={refreshing}
            onRefresh={() => {
              onRefresh();
            }}
          />
        }
        scrollEnabled={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <MapView
          focusable={true}
          followsUserLocation={true}
          loadingEnabled={true}
          loadingIndicatorColor={Colors.primary}
          paddingAdjustmentBehavior={'automatic'}
          provider={PROVIDER_GOOGLE}
          region={region}
          showsIndoorLevelPicker={true}
          showsUserLocation={true}
          showsMyLocationButton={true}
          style={Styles.mapView}
          userLocationPriority={'high'}>
          {Boolean(stores) &&
            Array.isArray(stores) &&
            stores.length != 0 &&
            stores.map((storeData: StoreInterface, index: number) => (
              <Marker
                key={index}
                coordinate={storeData.coordinates}
                onPress={(): void => {
                  setRegion(storeData.coordinates);

                  setSelectedStore(storeData);
                }}
                pinColor={Colors.red}
                title={storeData.name}
              />
            ))}

          {Boolean(selectedStore) && (
            <Marker
              coordinate={selectedStore?.coordinates}
              pinColor={Colors.primary}
              title={selectedStore?.name}
            />
          )}
        </MapView>

        <View style={Styles.storeListContainer}>
          <ScrollView
            contentContainerStyle={HelperStyles.flexGrow(1)}
            horizontal={true}
            keyboardShouldPersistTaps={'handled'}
            nestedScrollEnabled={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            {Boolean(stores) && Array.isArray(stores) && stores.length != 0 ? (
              stores.map((storeData: StoreInterface, index: number) => (
                <TouchableWithoutFeedback
                  key={index}
                  onPress={(): void => {
                    if (
                      Boolean(selectedStore) &&
                      selectedStore?.storeId == storeData.storeId
                    ) {
                      props.navigation.navigate(Strings.storeView, {storeData});
                    } else {
                      setRegion(storeData.coordinates);

                      setSelectedStore(storeData);
                    }
                  }}>
                  {renderStoreCard(storeData, index)}
                </TouchableWithoutFeedback>
              ))
            ) : (
              <View style={{flexDirection: 'row'}}>
                <SkeletonCard marginLeft={0} />

                <SkeletonCard />

                <SkeletonCard />

                <SkeletonCard />

                <SkeletonCard />
              </View>
            )}
          </ScrollView>
        </View>
      </ScrollView>
    </View>
  );
};

export default StoreLocator;
