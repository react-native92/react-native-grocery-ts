import React, {
  FC,
  ReactElement,
  ReactNode,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {
  RefreshControl,
  ScrollView,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  clearNotification,
  clearNotifications,
  getUserNotifications,
} from '../../configs/jsons/Notifications';
import {connect} from 'react-redux';
import {
  NotificationInterface,
  ResponseInterface,
} from '../../configs/ts/Interfaces';
import {notificationStatus} from '../../redux/Root.Actions';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Colors from '../../utils/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import Indicator from '../../components/appComponents/Indicator';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import NoResponse from '../../components/appComponents/NoResponse';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Notifications';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import * as HelperStyles from '../../utils/HelperStyles';

const Notifications: FC = (props: any) => {
  // Notifications Variables
  const [notifications, setNotifications] = useState<any>(null);

  // Other Variables
  const userId: number | string = props.loginUserInfo?.id ?? null;
  const [refreshing, setRefreshing] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useLayoutEffect(() => {
    props.navigation.setOptions({
      headerLeft: (): ReactNode => renderLeft(),
      headerRight: (): ReactNode => renderRight(),
    });
  }, []);

  function renderLeft(): ReactNode {
    return (
      <TouchableOpacity
        onPress={(): void => {
          props.navigation.goBack();
        }}
        style={HelperStyles.justView('marginRight', 16)}>
        <MaterialIcons color={Colors.white} name={'close'} size={24} />
      </TouchableOpacity>
    );
  }

  function renderRight(): ReactNode {
    return (
      <TouchableOpacity
        onPress={(): void => {
          props.navigation.navigate(Strings.notifications);
        }}>
        <MaterialIcons color={Colors.white} name={'notifications'} size={24} />
      </TouchableOpacity>
    );
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      init();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function init(): void {
    Store.dispatch(notificationStatus(false));

    fetchNotifications();
  }

  function fetchNotifications(): void {
    const response: ResponseInterface = getUserNotifications(userId);

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = [...response.data];

      organizeNotifications(responseData);
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message:
          response.status == StatusCodes.NotFound
            ? Strings.info
            : Strings.error,
        type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
      });

      setNotifications([]);
    }
  }

  function organizeNotifications(notifyArray: any[]): void {
    let helperObject: {
      today: any[];
      yesterday: any[];
      lastWeek: any[];
      others: any[];
    } = {
      today: [],
      yesterday: [],
      lastWeek: [],
      others: [],
    };

    notifyArray.map((lol: NotificationInterface) => {
      if (moment(lol.createdAt).isSame(moment(), 'day')) {
        helperObject.today.push(lol);
      } else if (
        moment(lol.createdAt).isSame(moment().subtract(1, 'day'), 'day')
      ) {
        helperObject.yesterday.push(lol);
      } else if (
        moment(lol.createdAt).isSame(moment().subtract(1, 'week'), 'week')
      ) {
        helperObject.lastWeek.push(lol);
      } else {
        helperObject.others.push(lol);
      }
    });

    setNotifications(notifyArray.length != 0 ? helperObject : []);
  }

  function onRefresh(): void {
    setRefreshing(true);

    setNotifications(null);

    setTimeout((): void => {
      fetchNotifications();

      setRefreshing(false);
    }, 1000);
  }

  function renderToday(): ReactElement {
    return (
      <View style={HelperStyles.margin(8, 8)}>
        {renderHeader(Strings.today, notifications.today)}

        {renderNotications(notifications.today, Strings.today)}
      </View>
    );
  }

  function renderYesterday(): ReactElement {
    return (
      <View style={HelperStyles.margin(8, 8)}>
        {renderHeader(Strings.yesterday, notifications.yesterday)}

        {renderNotications(notifications.yesterday, Strings.yesterday)}
      </View>
    );
  }

  function renderLastWeek(): ReactElement {
    return (
      <View style={HelperStyles.margin(8, 8)}>
        {renderHeader(Strings.lastWeek, notifications.lastWeek)}

        {renderNotications(notifications.lastWeek, Strings.lastWeek)}
      </View>
    );
  }

  function renderOthers(): ReactElement {
    return (
      <View style={HelperStyles.margin(8, 8)}>
        {renderHeader(Strings.others, notifications.others)}

        {renderNotications(notifications.others, Strings.others)}
      </View>
    );
  }

  function renderHeader(label: string, notifyArray: any[]): ReactElement {
    function handleClearNotifications(notifyArray: any[]): void {
      const clearIds: any[] = notifyArray.map(
        (notificationData: NotificationInterface) =>
          notificationData.notificationId,
      );

      const response: ResponseInterface = clearNotifications(clearIds);

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        setTimeout((): void => {
          fetchNotifications();
        }, 1000);
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message:
            response.status == StatusCodes.NotFound
              ? Strings.info
              : Strings.error,
          type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
        });
      }
    }

    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View
          style={[
            HelperStyles.flex(0.8),
            HelperStyles.alignItemsCenteredView('flex-start'),
          ]}>
          <Text
            style={HelperStyles.textView(
              16,
              '800',
              Colors.manatee,
              'center',
              'none',
            )}>
            {label}
          </Text>
        </View>

        <View
          style={[
            HelperStyles.flex(0.2),
            HelperStyles.alignItemsCenteredView('flex-end'),
          ]}>
          <TouchableOpacity
            onPress={(): void => {
              handleClearNotifications(notifyArray);
            }}>
            <Text
              style={HelperStyles.textView(
                14,
                '600',
                Colors.manatee,
                'center',
                'none',
              )}>
              {label != Strings.today ? Strings.clear : Strings.clearAll}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderNotications(
    notifyArray: any[],
    actionFor?: string,
  ): ReactElement {
    let prevOpenedRow: any,
      row: any[] = [];

    function handleRowOpenClose(index: number): void {
      if (Boolean(prevOpenedRow) && prevOpenedRow !== row[index]) {
        prevOpenedRow.close();
      }

      prevOpenedRow = row[index];
    }

    function renderRightActions(notificationId: any): ReactElement {
      function handleClearNotification(notificationId: number | string): void {
        Boolean(prevOpenedRow) && prevOpenedRow.close();

        const response: ResponseInterface = clearNotification(notificationId);

        if (response.status == StatusCodes.Success) {
          showMessage({
            description: response.message,
            icon: 'auto',
            message: Strings.success,
            type: 'success',
          });

          setTimeout((): void => {
            fetchNotifications();
          }, 1000);
        } else {
          showMessage({
            description: response.message,
            icon: 'auto',
            message:
              response.status == StatusCodes.NotFound
                ? Strings.info
                : Strings.error,
            type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
          });
        }
      }

      return (
        <TouchableHighlight
          onPress={(): void => {
            handleClearNotification(notificationId);
          }}
          style={Styles.deleteContainer}>
          <Text
            style={HelperStyles.textView(
              14,
              '800',
              Colors.white,
              'center',
              'none',
            )}>
            Delete
          </Text>
        </TouchableHighlight>
      );
    }

    return (
      <>
        {notifyArray.map(
          (notificationData: NotificationInterface, index: number) => (
            <Swipeable
              key={index}
              childrenContainerStyle={Styles.cardContainer}
              onSwipeableOpen={(): void => handleRowOpenClose(index)}
              ref={(ref: Swipeable): Swipeable => (row[index] = ref)}
              renderRightActions={(): ReactNode =>
                renderRightActions(notificationData.notificationId)
              }
              shouldCancelWhenOutside={true}>
              <View
                style={[
                  HelperStyles.flex(0.15),
                  HelperStyles.justifyContentCenteredView('center'),
                  HelperStyles.padding(4, 4),
                ]}>
                <View style={Styles.iconContainer}>
                  <FontAwesomeIcons
                    color={Colors.tealGreen}
                    name={'bell'}
                    size={20}
                  />
                </View>
              </View>

              <View
                style={[
                  HelperStyles.flex(0.85),
                  HelperStyles.justView('justifyContent', 'flex-start'),
                  HelperStyles.padding(4, 4),
                ]}>
                <View
                  style={[
                    HelperStyles.flexDirection('row'),
                    HelperStyles.justifyContentCenteredView('space-between'),
                  ]}>
                  <View
                    style={[
                      HelperStyles.flex(0.7),
                      HelperStyles.alignItemsCenteredView('flex-start'),
                    ]}>
                    <Text
                      style={HelperStyles.textView(
                        14,
                        '600',
                        Colors.ebony,
                        'center',
                        'none',
                      )}>
                      {notificationData.title}
                    </Text>
                  </View>

                  <View
                    style={[
                      HelperStyles.flex(0.3),
                      HelperStyles.alignItemsCenteredView('flex-end'),
                    ]}>
                    <Text
                      style={HelperStyles.textView(
                        10,
                        '800',
                        Colors.tealGreen,
                        'center',
                        'none',
                      )}>
                      {notifyTimestamp(notificationData.createdAt, actionFor)}
                    </Text>
                  </View>
                </View>

                <View style={HelperStyles.justView('marginTop', 4)}>
                  <Text
                    style={[
                      HelperStyles.textView(
                        12,
                        '600',
                        Colors.stowaway,
                        'left',
                        'none',
                      ),
                      HelperStyles.justView('lineHeight', 18),
                    ]}>
                    {notificationData.description}
                  </Text>
                </View>
              </View>
            </Swipeable>
          ),
        )}
      </>
    );
  }

  function notifyTimestamp(
    timeStamp: number | string,
    actionFor?: string,
  ): string {
    switch (actionFor) {
      case Strings.lastWeek:
        return moment(timeStamp).format(`${Strings.d3}, ${Strings.h2M1A}`);

      case Strings.others:
        return moment(timeStamp).format(Strings.l2);

      case Strings.today:
      default:
        const value: string = moment(
          timeStamp,
          `${Strings.Y4M2D2} ${Strings.H2m2s2}`,
        ).fromNow();

        return value == 'a few seconds ago'
          ? Strings.now
          : value.replace(/minutes|minute/gi, 'min') ||
              value.replace(/seconds|second/gi, 'sec');

      case Strings.yesterday:
        return moment(timeStamp).format(Strings.h2M1A);
    }
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={(): void => {
              onRefresh();
            }}
          />
        }
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={[HelperStyles.flex(1), HelperStyles.margin(8, 8)]}>
          {Boolean(notifications) ? (
            !Array.isArray(notifications) ? (
              <>
                {notifications?.today &&
                  Array.isArray(notifications.today) &&
                  notifications.today.length != 0 &&
                  renderToday()}

                {notifications?.yesterday &&
                  Array.isArray(notifications.yesterday) &&
                  notifications.yesterday.length != 0 &&
                  renderYesterday()}

                {notifications?.lastWeek &&
                  Array.isArray(notifications.lastWeek) &&
                  notifications.lastWeek.length != 0 &&
                  renderLastWeek()}

                {notifications?.others &&
                  Array.isArray(notifications.others) &&
                  notifications.others.length != 0 &&
                  renderOthers()}
              </>
            ) : (
              <NoResponse />
            )
          ) : (
            <Indicator />
          )}
        </View>
      </ScrollView>
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(Notifications);
