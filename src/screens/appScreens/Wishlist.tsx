import React, {FC, ReactElement, useCallback, useState} from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';
import {connect} from 'react-redux';
import {getWishList, updateFavorite} from '../../configs/jsons/Category';
import {
  CategoryItemInterface,
  FavoriteInterFace,
  ResponseInterface,
} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Colors from '../../utils/Colors';
import ImageViewer from '../../components/appComponents/ImageViewer';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NoResponse from '../../components/appComponents/NoResponse';
import SkeletonPlaceholder from '../../containers/SkeletonPlaceholder';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Wishlist';
import * as HelperStyles from '../../utils/HelperStyles';

const Wishlist: FC = (props: any) => {
  // Wishlist Variables
  const [wishlist, setWishList] = useState<any>(null);
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [imageViewer, setImageViewer] = useState<any>(null);

  // Other Variables
  const userId: number | string = props.loginUserInfo?.id ?? null;
  const [refreshing, setRefreshing] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      fetchWishList();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function fetchWishList(): void {
    const response: ResponseInterface = getWishList(userId);

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = [...response.data];

      setWishList(responseData);
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message:
          response.status == StatusCodes.NotFound
            ? Strings.info
            : Strings.error,
        type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
      });

      setWishList([]);
    }
  }

  function onRefresh(): void {
    setRefreshing(true);

    setWishList(null);

    setTimeout((): void => {
      fetchWishList();

      setRefreshing(false);
    }, 1000);
  }

  function renderWishListItem(itemData: CategoryItemInterface): ReactElement {
    function handleDeleteWishItem(itemId: number | string | undefined): void {
      const requestData: FavoriteInterFace = {
        itemId,
        userId,
      };

      const response: ResponseInterface = updateFavorite(requestData);

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        fetchWishList();
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      }
    }

    return (
      <TouchableOpacity
        onPress={(): void => {
          props.navigation.navigate(Strings.item, {
            itemData,
          });
        }}
        style={Styles.cardContainer}>
        <TouchableOpacity
          onPress={(): void => {
            setImageViewer(itemData);
          }}
          style={Styles.cardImageContainer}>
          <ImageBackground
            onLoadEnd={(): void => {
              setImageLoader(false);
            }}
            onLoadStart={(): void => {
              setImageLoader(true);
            }}
            resizeMode={'contain'}
            source={
              Boolean(itemData?.image)
                ? {uri: itemData.image}
                : Assets.logoSquare
            }
            style={HelperStyles.flex(1)}>
            {imageLoader && (
              <ActivityIndicator
                color={Colors.manatee}
                size={24}
                style={[
                  HelperStyles.flex(1),
                  HelperStyles.justifyContentCenteredView('center'),
                ]}
              />
            )}
          </ImageBackground>
        </TouchableOpacity>

        <View
          style={[
            HelperStyles.flex(0.75),
            HelperStyles.alignItemsCenteredView('flex-start'),
            HelperStyles.padding(8, 8),
          ]}>
          <Text
            ellipsizeMode={'tail'}
            numberOfLines={1}
            style={HelperStyles.textView(
              14,
              '800',
              Colors.ebony,
              'center',
              'none',
            )}>
            {itemData.item}
          </Text>

          <View
            style={[
              HelperStyles.flexDirection('row'),
              HelperStyles.justifyContentCenteredView('space-between'),
            ]}>
            <View
              style={[
                HelperStyles.flex(0.85),
                HelperStyles.alignItemsCenteredView('flex-start'),
              ]}>
              <View style={Styles.cardRatingContainer}>
                <AirbnbRating
                  count={Strings.ratingsCount}
                  defaultRating={itemData.ratings}
                  isDisabled={true}
                  showRating={false}
                  size={14}
                />

                {Boolean(itemData.totalRatings) && (
                  <Text
                    style={[
                      HelperStyles.textView(
                        12,
                        '600',
                        Colors.manatee,
                        'center',
                        'none',
                      ),
                      HelperStyles.margin(4, 0),
                    ]}>
                    ({itemData.totalRatings})
                  </Text>
                )}
              </View>

              <Text
                style={HelperStyles.textView(
                  12,
                  '600',
                  Colors.ebony,
                  'center',
                  'none',
                )}>
                Rs. {parseFloat(String(itemData?.price)).toFixed(2)}{' '}
                <Text
                  style={HelperStyles.textView(
                    12,
                    '800',
                    Colors.manatee,
                    'center',
                    'none',
                  )}>
                  / {itemData.weight}
                </Text>
              </Text>
            </View>

            <View
              style={[
                HelperStyles.flex(0.15),
                HelperStyles.alignItemsCenteredView('flex-end'),
              ]}>
              <TouchableOpacity
                onPress={(): void => {
                  handleDeleteWishItem(itemData.itemId);
                }}
                style={HelperStyles.padding(2, 2)}>
                <MaterialIcons color={Colors.red} name={'delete'} size={24} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  const SkeletonCard: FC<{marginVertical?: number}> = ({
    marginVertical = 8,
  }): ReactElement => {
    return (
      <View style={HelperStyles.margin(0, marginVertical)}>
        <SkeletonPlaceholder>
          <View style={Styles.skeletonCardContainer} />
        </SkeletonPlaceholder>
      </View>
    );
  };

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={(): void => {
              onRefresh();
            }}
          />
        }
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={[HelperStyles.flex(1), HelperStyles.screenSubContainer]}>
          {Boolean(wishlist) ? (
            Array.isArray(wishlist) && wishlist.length != 0 ? (
              wishlist.map((itemData: CategoryItemInterface, index: number) => (
                <View
                  key={index}
                  style={HelperStyles.margin(0, index != 0 ? 8 : 0)}>
                  {renderWishListItem(itemData)}
                </View>
              ))
            ) : (
              <View style={Styles.screenContainer}>
                <NoResponse />
              </View>
            )
          ) : (
            <>
              <SkeletonCard marginVertical={0} />

              <SkeletonCard />

              <SkeletonCard />

              <SkeletonCard />

              <SkeletonCard />
            </>
          )}
        </View>
      </ScrollView>

      <ImageViewer
        onClose={(): void => {
          setImageViewer(null);
        }}
        onRequestClose={(): void => {
          setImageViewer(null);
        }}
        title={imageViewer?.item}
        url={imageViewer?.image}
        visible={Boolean(imageViewer)}
      />
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(Wishlist);
