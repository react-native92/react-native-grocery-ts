import React, {
  FC,
  ReactElement,
  ReactNode,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  RefreshControl,
  ScrollView,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  CategoryInterface,
  SubCategoryInterface,
} from '../../configs/ts/Interfaces';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Colors from '../../utils/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NoResponse from '../../components/appComponents/NoResponse';
import SearchModal from '../../components/appComponents/SearchModal';
import SkeletonPlaceholder from '../../containers/SkeletonPlaceholder';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/SubCategory';
import * as HelperStyles from '../../utils/HelperStyles';

const SubCategory: FC = (props: any) => {
  // Props Variables
  const categoryData: CategoryInterface =
    props?.route?.params?.categoryData ?? null;

  // SubCategory Variables
  const [subCategory, setSubCategory] = useState<any>(null);
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [searchStatus, setSearchStatus] = useState<boolean>(false);

  // Other Variables
  const [refreshing, setRefreshing] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useLayoutEffect((): void => {
    props.navigation.setOptions({
      headerRight: (): ReactNode => renderHeaderRight(),
      headerTitle: categoryData?.category ?? Strings.subCategory,
    });
  }, []);

  function renderHeaderRight(): ReactNode {
    return (
      <TouchableOpacity
        onPress={(): void => {
          setSearchStatus(!searchStatus);
        }}>
        <MaterialIcons color={Colors.white} name={'search'} size={24} />
      </TouchableOpacity>
    );
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      fetchSubCategory();

      return (): void => {
        isFocus = false;

        setSearchStatus(false);
      };
    }, []),
  );

  function fetchSubCategory(): void {
    setSubCategory(categoryData?.subCategories ?? []);
  }

  function onRefresh(): void {
    setRefreshing(true);

    setSubCategory(null);

    setTimeout((): void => {
      fetchSubCategory();

      setRefreshing(false);
    }, 1000);
  }

  function renderImage(): ReactElement {
    return (
      <ImageBackground
        onLoadEnd={(): void => {
          setImageLoader(false);
        }}
        onLoadStart={(): void => {
          setImageLoader(true);
        }}
        resizeMode={'cover'}
        source={
          Boolean(categoryData?.image)
            ? {uri: categoryData.image}
            : Assets.logoSquare
        }
        style={HelperStyles.imageView('100%', '100%')}>
        {imageLoader && (
          <ActivityIndicator
            color={Colors.manatee}
            size={24}
            style={[
              HelperStyles.flex(1),
              HelperStyles.justifyContentCenteredView('center'),
            ]}
          />
        )}
      </ImageBackground>
    );
  }

  function renderSubCategoryCard(): ReactElement {
    function renderItems(
      subCategoryData: SubCategoryInterface,
      index: number,
    ): ReactElement {
      return (
        <TouchableOpacity
          key={index}
          onPress={(): void => {
            props.navigation.navigate(Strings.subCategoryItems, {
              subCategoryData,
            });
          }}
          style={[
            HelperStyles.flexDirection('row'),
            HelperStyles.justifyContentCenteredView('space-between'),
            HelperStyles.margin(4, 4),
            HelperStyles.padding(4, 4),
          ]}>
          <View
            style={[
              HelperStyles.flex(0.9),
              HelperStyles.alignItemsCenteredView('flex-start'),
            ]}>
            <Text
              style={HelperStyles.textView(
                16,
                '400',
                Colors.ebony,
                'center',
                'capitalize',
              )}>
              {subCategoryData.subCategory}
            </Text>
          </View>

          <View
            style={[
              HelperStyles.flex(0.1),
              HelperStyles.justifyContentCenteredView('center'),
            ]}>
            <MaterialIcons
              color={Colors.manatee}
              name={'chevron-right'}
              size={24}
            />
          </View>
        </TouchableOpacity>
      );
    }

    const SkeletonCard: FC = (): ReactElement => {
      return (
        <View style={HelperStyles.padding(4, 4)}>
          <SkeletonPlaceholder>
            <View style={Styles.skeletonCardContainer} />
          </SkeletonPlaceholder>
        </View>
      );
    };

    return (
      <View style={Styles.cardContainer}>
        {Boolean(subCategory) ? (
          Array.isArray(subCategory) && subCategory.length != 0 ? (
            <ScrollView
              contentContainerStyle={HelperStyles.flexGrow(1)}
              keyboardShouldPersistTaps={'handled'}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={(): void => {
                    onRefresh();
                  }}
                />
              }
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}>
              {subCategory.map(
                (
                  subCategoryData: SubCategoryInterface,
                  index: number,
                ): ReactElement => renderItems(subCategoryData, index),
              )}
            </ScrollView>
          ) : (
            <View style={Styles.screenContainer}>
              <NoResponse />
            </View>
          )
        ) : (
          <>
            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />

            <SkeletonCard />
          </>
        )}
      </View>
    );
  }

  function renderSerachModal(): ReactElement {
    return (
      <SearchModal
        categoryId={categoryData?.categoryId}
        onClose={(): void => {
          setSearchStatus(!searchStatus);
        }}
        onRequestClose={(): void => {
          setSearchStatus(!searchStatus);
        }}
        searchFor={'category'}
        visible={searchStatus}
      />
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      {Boolean(searchStatus) && (
        <StatusBar
          barStyle={'light-content'}
          backgroundColor={Colors.primary75}
        />
      )}

      <View style={Styles.imageContainer}>{renderImage()}</View>

      <View
        style={[
          HelperStyles.flex(0.725),
          HelperStyles.justView('backgroundColor', Theme.background),
        ]}>
        {renderSubCategoryCard()}
      </View>

      {renderSerachModal()}
    </View>
  );
};

export default SubCategory;
