import React, {
  FC,
  ReactElement,
  ReactNode,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {RefreshControl, ScrollView, TouchableOpacity, View} from 'react-native';
import {
  CategoryItemInterface,
  FavoriteInterFace,
  ResponseInterface,
  SubCategoryInterface,
} from '../../configs/ts/Interfaces';
import {connect} from 'react-redux';
import {
  getAppDeals,
  getMemberDeals,
  getSubCategoryItems,
  updateFavorite,
} from '../../configs/jsons/Category';
import {SearchForType} from '../../configs/ts/Types';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import CardItem from '../../components/appComponents/CardItem';
import Colors from '../../utils/Colors';
import Indicator from '../../components/appComponents/Indicator';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NoResponse from '../../components/appComponents/NoResponse';
import SearchModal from '../../components/appComponents/SearchModal';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/SubCategoryItems';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const SubCategoryItems: FC = (props: any) => {
  // Props Variables
  const from: string = props?.route?.params?.from ?? null;
  const subCategoryData: SubCategoryInterface =
    props?.route?.params?.subCategoryData ?? null;

  // SubCategoryItems Variables
  const [items, setItems] = useState<any>(null);
  const [searchStatus, setSearchStatus] = useState<boolean>(false);

  // Other Variables
  const userId: number | string = props.loginUserInfo?.id ?? null;
  const [refreshing, setRefreshing] = useState<boolean>(false);

  // Theme Variables
  const Theme = useTheme().colors;

  useLayoutEffect((): void => {
    props.navigation.setOptions({
      headerRight: (): ReactNode => renderHeaderRight(),
      headerTitle:
        from ?? subCategoryData?.subCategory ?? Strings.subCategoryItems,
    });
  }, []);

  function renderHeaderRight(): ReactNode {
    return (
      <TouchableOpacity
        onPress={(): void => {
          setSearchStatus(!searchStatus);
        }}>
        <MaterialIcons color={Colors.white} name={'search'} size={24} />
      </TouchableOpacity>
    );
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      fetchItems();

      return (): void => {
        isFocus = false;

        setSearchStatus(false);
      };
    }, []),
  );

  function fetchItems(): void {
    const response: ResponseInterface = handleItems();

    if (
      response.status == StatusCodes.Success &&
      Boolean(response.data) &&
      Array.isArray(response.data)
    ) {
      const responseData: any[] = Boolean(from)
        ? [...Helpers.sort(response.data, 'item').reverse()]
        : [...response.data];

      organizeItems(responseData);
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message:
          response.status == StatusCodes.NotFound
            ? Strings.info
            : Strings.error,
        type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
      });

      setItems([]);
    }
  }

  function handleItems(): ResponseInterface {
    switch (from) {
      case Strings.groceryAppDeals:
        return getAppDeals();

      case Strings.groceryAppMemberDeals:
        return getMemberDeals();

      default:
        return getSubCategoryItems(subCategoryData?.subCategoryId);
    }
  }

  function organizeItems(subCategoryItems: any[]): void {
    if (Array.isArray(subCategoryItems) && subCategoryItems.length != 0) {
      let helperSum: number = 0,
        helperOuterArray: any[] = [];

      for (
        let i = 0;
        i < subCategoryItems.length / Strings.subCategoryRowItems;
        i++
      ) {
        let helperInnerArray: any[] = [];

        for (let j = 0; j < Strings.subCategoryRowItems; j++) {
          const element: any = subCategoryItems[helperSum];

          Boolean(element) && helperInnerArray.push(element);

          helperSum++;
        }

        helperOuterArray.push(helperInnerArray);
      }

      setItems(helperOuterArray);
    } else {
      setItems([]);
    }
  }

  function onRefresh(): void {
    setRefreshing(true);

    setItems(null);

    setTimeout((): void => {
      fetchItems();

      setRefreshing(false);
    }, 100);
  }

  function handleFavorite(data: CategoryItemInterface): void {
    const requestData: FavoriteInterFace = {
      itemId: data?.itemId,
      userId,
    };

    const response: ResponseInterface = updateFavorite(requestData);

    if (response.status == StatusCodes.Success) {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.success,
        type: 'success',
      });

      fetchItems();
    } else {
      showMessage({
        description: response.message,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  function renderSerachModal(): ReactElement {
    function handleSearchFor(): SearchForType {
      switch (from) {
        case Strings.groceryAppDeals:
          return 'app-deals';

        case Strings.groceryAppMemberDeals:
          return 'member-deals';

        default:
          return 'sub-category';
      }
    }

    return (
      <SearchModal
        onClose={(): void => {
          setSearchStatus(!searchStatus);
        }}
        onRequestClose={(): void => {
          setSearchStatus(!searchStatus);
        }}
        searchFor={handleSearchFor()}
        subCategoryId={subCategoryData?.subCategoryId}
        visible={searchStatus}
      />
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={(): void => {
              onRefresh();
            }}
          />
        }
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={[HelperStyles.flex(1), HelperStyles.margin(8, 8)]}>
          {Boolean(items) ? (
            Array.isArray(items) && items.length != 0 ? (
              items.map(
                (rowData: any, rowIndex: number): ReactElement => (
                  <View
                    key={rowIndex}
                    style={[
                      Styles.subCategoriesRowContainer,
                      HelperStyles.margin(8, 8),
                    ]}>
                    {rowData.map(
                      (
                        itemData: CategoryItemInterface,
                        index: number,
                      ): ReactElement => (
                        <CardItem
                          data={itemData}
                          key={index}
                          onFavorite={(): void => {
                            handleFavorite(itemData);
                          }}
                          onPress={(): void => {
                            props.navigation.navigate(Strings.item, {itemData});
                          }}
                          style={[
                            HelperStyles.justView(
                              'width',
                              `${(
                                100 / Strings.subCategoryRowItems
                              ).toFixed()}%`,
                            ),
                            HelperStyles.margin(index % 3 == 1 ? 4 : 0, 0),
                          ]}
                        />
                      ),
                    )}
                  </View>
                ),
              )
            ) : (
              <NoResponse />
            )
          ) : (
            <Indicator />
          )}
        </View>
      </ScrollView>

      {renderSerachModal()}
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(SubCategoryItems);
