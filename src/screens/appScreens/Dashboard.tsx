import React, {
  FC,
  Fragment,
  ReactElement,
  ReactNode,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {connect} from 'react-redux';
import {
  createMaterialTopTabNavigator,
  MaterialTopTabNavigationOptions,
} from '@react-navigation/material-top-tabs';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Cart from './Cart';
import Colors from '../../utils/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MenuBar from '../../components/appComponents/MenuBar';
import Profile from './Profile';
import SearchModal from '../../components/appComponents/SearchModal';
import Store from './Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/Dashboard';
import Wishlist from './Wishlist';
import * as HelperStyles from '../../utils/HelperStyles';

const Dashboard: FC = (props: any) => {
  // Props Variables
  const initialRouteName: string =
    props?.route?.params?.initialRouteName ?? Strings.store;

  // Dashboard Variables
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [searchStatus, setSearchStatus] = useState<boolean>(false);
  const TopTabs = createMaterialTopTabNavigator();

  // Theme Variables
  const Theme = useTheme().colors;

  useLayoutEffect((): void => {
    props.navigation.setOptions({
      headerLeft: (): ReactNode => renderHeaderLeft(),
      headerRight: (): ReactNode => renderHeaderRight(),
    });
  }, [props.notificationStatus]);

  function renderHeaderLeft(): ReactNode {
    return (
      <TouchableOpacity
        onPress={(): void => {
          setModalVisible(!modalVisible);
        }}
        style={HelperStyles.justView('marginRight', 16)}>
        <MaterialIcons color={Colors.white} name={'menu'} size={24} />
      </TouchableOpacity>
    );
  }

  function renderHeaderRight(): ReactNode {
    return (
      <View
        style={[
          HelperStyles.flexDirection('row'),
          HelperStyles.justifyContentCenteredView('space-evenly'),
        ]}>
        <TouchableOpacity
          onPress={(): void => {
            setSearchStatus(!searchStatus);
          }}
          style={HelperStyles.justView('right', 16)}>
          <MaterialIcons color={Colors.white} name={'search'} size={24} />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={(): void => {
            props.navigation.navigate(Strings.notifications);
          }}>
          <FontAwesomeIcons color={Colors.white} name={'bell'} size={20} />

          {Boolean(props.notificationStatus) && (
            <View style={Styles.notificationDot} />
          )}
        </TouchableOpacity>
      </View>
    );
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      return (): void => {
        isFocus = false;

        setSearchStatus(false);
      };
    }, []),
  );

  function handleRouteCounts(routeName: string): number {
    switch (routeName) {
      case Strings.cart:
        return props.cartItemCount ?? 0;

      case Strings.wishlist:
        return props.wishItemCount ?? 0;

      default:
        return 0;
    }
  }

  function renderSerachModal(): ReactElement {
    return (
      <SearchModal
        onClose={(): void => {
          setSearchStatus(!searchStatus);
        }}
        onRequestClose={(): void => {
          setSearchStatus(!searchStatus);
        }}
        searchFor={'all'}
        visible={searchStatus}
      />
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <MenuBar
        onRequestClose={(): void => {
          setModalVisible(!modalVisible);
        }}
        visible={modalVisible}
      />

      <TopTabs.Navigator
        backBehavior={'history'}
        initialRouteName={initialRouteName}
        screenOptions={({route}): MaterialTopTabNavigationOptions => ({
          animationEnabled: true,
          swipeEnabled: true,
          tabBarActiveTintColor: Colors.white,
          tabBarIcon: ({color}): ReactNode => {
            const icons: any = {
              [Strings.store]: 'storefront',
              [Strings.cart]: 'shopping-basket',
              [Strings.wishlist]: 'favorite',
              [Strings.profile]: 'account-circle',
            };

            return (
              <Fragment>
                {(route.name == Strings.cart ||
                  route.name == Strings.wishlist) && (
                  <View style={Styles.iconBadgeContainer}>
                    <Text
                      style={HelperStyles.textView(
                        10,
                        '800',
                        Colors.white,
                        'center',
                        'none',
                      )}>
                      {handleRouteCounts(route.name)}
                    </Text>
                  </View>
                )}

                <MaterialIcons
                  color={color}
                  name={icons[route.name]}
                  size={24}
                />
              </Fragment>
            );
          },
          tabBarInactiveTintColor: Colors.tealGreen,
          tabBarIndicatorStyle: {
            borderBottomColor: Colors.white,
            borderBottomWidth: 2,
          },
          tabBarPressColor: Colors.transparent,
          tabBarShowIcon: true,
          tabBarShowLabel: false,
          tabBarStyle: {backgroundColor: Colors.primary, borderWidth: 0},
        })}
        style={HelperStyles.flex(1)}>
        <TopTabs.Screen name={Strings.store} component={Store} />

        <TopTabs.Screen name={Strings.cart} component={Cart} />

        <TopTabs.Screen name={Strings.wishlist} component={Wishlist} />

        <TopTabs.Screen name={Strings.profile} component={Profile} />
      </TopTabs.Navigator>

      {renderSerachModal()}
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    cartItemCount: state?.other?.cartItemCount ?? null,
    notificationStatus: state?.other?.notificationStatus ?? null,
    wishItemCount: state?.other?.wishItemCount ?? null,
  };
}

export default connect(mapStateToProps, null)(Dashboard);
