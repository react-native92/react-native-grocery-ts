import React, {
  FC,
  ReactElement,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';
import {
  checkRatingsReviews,
  createRatingReview,
  deleteRatingReview,
  updateReview,
} from '../../configs/jsons/Stores';
import {connect} from 'react-redux';
import {
  RatingReviewCheckInterface,
  RatingReviewInterface,
  ResponseInterface,
  ReviewInterface,
  StoreInterface,
  UserInterface,
} from '../../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Button from '../../components/appComponents/Button';
import Colors from '../../utils/Colors';
import CustomTextInput from '../../components/appComponents/CustomTextInput';
import Indicator from '../../components/appComponents/Indicator';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import Strings from '../../utils/Strings';
import Styles from '../../styles/appStyles/StoreReviews';
import * as HelperStyles from '../../utils/HelperStyles';

const StoreReviews: FC = (props: any) => {
  // Props Variables
  const storeData: StoreInterface = props?.route?.params?.storeData ?? null;

  // StoreReviews Variables
  const [userRatingReview, setUserRatingReview] =
    useState<null | RatingReviewInterface>(null);
  const [rating, setRating] = useState<number>(0);
  const [review, setReview] = useState<any>(null);
  const [reviewStatus, setReviewStatus] = useState<boolean>(false);

  // Other Variables
  const userData: null | UserInterface = props.loginUserInfo ?? null;

  // Theme Variables
  const Theme = useTheme().colors;

  useLayoutEffect((): void => {
    props.navigation.setOptions({
      headerTitle: storeData?.name ?? Strings.storeReviews,
    });
  }, []);

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      checkUserRatingReview();

      return (): void => {
        isFocus = false;

        handleReset();
      };
    }, []),
  );

  function checkUserRatingReview(): void {
    const requestData: RatingReviewCheckInterface = {
      storeId: storeData.storeId,
      userId: 1,
    };

    const response: null | RatingReviewInterface =
      checkRatingsReviews(requestData);

    setUserRatingReview(response);
  }

  function handleReset(): void {
    setUserRatingReview(null);

    setRating(0);

    setReview(null);
  }

  function renderRatings(): ReactElement {
    return (
      <View style={[Styles.ratingsContainer, HelperStyles.padding(16, 8)]}>
        <Text
          style={HelperStyles.textView(14, '600', Theme.text, 'left', 'none')}>
          {Strings.ratings}
        </Text>

        <View
          style={[
            HelperStyles.flexDirection('row'),
            HelperStyles.justView('justifyContent', 'space-between'),
            HelperStyles.justView('marginTop', 8),
          ]}>
          <View
            style={[
              HelperStyles.flex(0.25),
              HelperStyles.justifyContentCenteredView('center'),
              HelperStyles.padding(4, 4),
            ]}>
            <Text
              style={HelperStyles.textView(
                20,
                '600',
                Colors.primary,
                'center',
                'none',
              )}>
              {parseFloat(String(storeData?.averageRatings ?? 0)).toFixed(1)}
            </Text>

            <Text
              style={HelperStyles.textView(
                12,
                '400',
                Colors.manatee,
                'center',
                'none',
              )}>
              {`(${storeData?.totalRatings ?? 0})`}
            </Text>
          </View>

          <View
            style={[
              HelperStyles.flex(0.75),
              HelperStyles.justifyContentCenteredView('center'),
            ]}>
            <AirbnbRating
              count={5}
              defaultRating={Number(storeData?.ratings) ?? 0}
              isDisabled={true}
              ratingContainerStyle={HelperStyles.padding(4, 4)}
              showRating={false}
              size={36}
            />
          </View>
        </View>
      </View>
    );
  }

  function renderRateAndReview(): ReactElement {
    function handlePostRatingReview(): void {
      const requestData: RatingReviewInterface = {
        createdAt: moment().format(`${Strings.Y4M2D2} ${Strings.H2m2s2}`),
        isDelete: false,
        rating,
        review,
        storeId: storeData.storeId,
        userId: 1,
      };

      const response: ResponseInterface = createRatingReview(requestData);

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        setRating(0);

        setReview(null);

        checkUserRatingReview();
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message:
            response.status == StatusCodes.NotFound
              ? Strings.info
              : Strings.error,
          type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
        });
      }
    }

    function handleUpdateReview(): void {
      const requestData: ReviewInterface = {
        createdAt: moment().format(`${Strings.Y4M2D2} ${Strings.H2m2s2}`),
        review,
        reviewId: userRatingReview?.ratingReviewId,
      };

      const response: ResponseInterface = updateReview(requestData);

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        setReview(null);

        setReviewStatus(false);

        checkUserRatingReview();
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message:
            response.status == StatusCodes.NotFound
              ? Strings.info
              : Strings.error,
          type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
        });
      }
    }

    function handleDeleteRatingReview(): void {
      const response: ResponseInterface = deleteRatingReview(
        userRatingReview?.ratingReviewId,
      );

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        checkUserRatingReview();
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message:
            response.status == StatusCodes.NotFound
              ? Strings.info
              : Strings.error,
          type: response.status == StatusCodes.NotFound ? 'info' : 'danger',
        });
      }
    }

    function renderReviewTextArea(): ReactElement {
      return (
        <CustomTextInput
          autoCapitalize={'sentences'}
          containerStyle={Styles.reviewContainer}
          multiline={true}
          onChangeText={(txt: string): void => {
            setReview(Boolean(txt) ? txt : null);
          }}
          placeholder={Strings.shareDetailsExperience}
          textContentType={'none'}
          textInputStyle={HelperStyles.margin(4, 0)}
          value={review}
        />
      );
    }

    return (
      <View style={[Styles.rateReviewContainer, HelperStyles.padding(16, 8)]}>
        {Boolean(userRatingReview) ? (
          <>
            <View
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justView('justifyContent', 'space-between'),
              ]}>
              <View
                style={[
                  HelperStyles.flex(0.0875),
                  HelperStyles.alignItemsCenteredView('flex-start'),
                  HelperStyles.padding(0, 4),
                ]}>
                <View style={Styles.userLogoContainer}>
                  <Image
                    source={Assets.logo}
                    style={HelperStyles.imageView('100%', '100%')}
                  />
                </View>
              </View>

              <View
                style={[
                  HelperStyles.flex(
                    Boolean(reviewStatus) && Boolean(review) ? 0.725 : 0.825,
                  ),
                  HelperStyles.alignItemsCenteredView('flex-start'),
                  HelperStyles.padding(8, 4),
                ]}>
                <Text
                  style={HelperStyles.textView(
                    16,
                    '600',
                    Theme.text,
                    'center',
                    'capitalize',
                  )}>
                  {userData?.username}
                </Text>
              </View>

              {Boolean(reviewStatus) && Boolean(review) ? (
                <View
                  style={[
                    HelperStyles.flex(0.2),
                    HelperStyles.alignItemsCenteredView('flex-end'),
                  ]}>
                  <Button
                    containerStyle={Styles.postButtonContainer}
                    onPress={(): void => {
                      handleUpdateReview();
                    }}
                    textStyle={HelperStyles.textView(
                      12,
                      '600',
                      Colors.white,
                      'center',
                      'uppercase',
                    )}
                    title={Strings.post}
                  />
                </View>
              ) : (
                <View
                  style={[
                    HelperStyles.flex(0.1),
                    HelperStyles.justifyContentCenteredView('center'),
                  ]}>
                  <TouchableOpacity
                    onPress={(): void => {
                      Boolean(reviewStatus)
                        ? [setReview(null), setReviewStatus(!reviewStatus)]
                        : handleDeleteRatingReview();
                    }}
                    style={HelperStyles.padding(4, 4)}>
                    <MaterialIcons
                      color={Colors.red}
                      name={Boolean(reviewStatus) ? 'close' : 'delete'}
                      size={20}
                    />
                  </TouchableOpacity>
                </View>
              )}
            </View>

            <View
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justifyContentCenteredView('flex-start'),
                HelperStyles.justView('marginTop', 4),
              ]}>
              <AirbnbRating
                count={5}
                defaultRating={Number(userRatingReview?.rating) ?? 0}
                isDisabled={true}
                ratingContainerStyle={HelperStyles.justView(
                  'alignItems',
                  'flex-start',
                )}
                showRating={false}
                size={16}
              />

              <View style={HelperStyles.margin(4, 0)}>
                <Text
                  style={HelperStyles.textView(
                    14,
                    '400',
                    Colors.lightText,
                    'center',
                    'none',
                  )}>
                  {moment(
                    userRatingReview?.createdAt,
                    `${Strings.Y4M2D2} ${Strings.H2m2s2}`,
                  ).fromNow()}
                </Text>
              </View>
            </View>

            <View
              style={[
                HelperStyles.alignItemsCenteredView('flex-start'),
                HelperStyles.justView('marginTop', 8),
                HelperStyles.justView('marginBottom', 4),
              ]}>
              {Boolean(userRatingReview?.review) ? (
                <Text
                  style={HelperStyles.textView(
                    14,
                    '400',
                    Colors.ebony,
                    'center',
                    'none',
                  )}>
                  {userRatingReview?.review}
                </Text>
              ) : Boolean(reviewStatus) ? (
                renderReviewTextArea()
              ) : (
                <TouchableOpacity
                  onPress={(): void => {
                    setReviewStatus(!reviewStatus);
                  }}
                  style={HelperStyles.padding(2, 8)}>
                  <Text
                    style={HelperStyles.textView(
                      14,
                      '400',
                      Colors.royalBlue,
                      'center',
                      'none',
                    )}>
                    {Strings.describeYourExperience}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </>
        ) : (
          <>
            <View
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justView('justifyContent', 'space-between'),
              ]}>
              <View
                style={[
                  HelperStyles.flex(Boolean(rating) ? 0.8 : 1),
                  HelperStyles.alignItemsCenteredView('flex-start'),
                ]}>
                <Text
                  style={HelperStyles.textView(
                    14,
                    '600',
                    Theme.text,
                    'left',
                    'none',
                  )}>
                  {Strings.rateAndReview}
                </Text>

                <Text
                  style={HelperStyles.textView(
                    12,
                    '400',
                    Colors.stowaway,
                    'left',
                    'none',
                  )}>
                  {Strings.shareExperience}
                </Text>
              </View>

              {Boolean(rating) && (
                <View
                  style={[
                    HelperStyles.flex(0.2),
                    HelperStyles.alignItemsCenteredView('flex-end'),
                  ]}>
                  <Button
                    containerStyle={Styles.postButtonContainer}
                    onPress={(): void => {
                      handlePostRatingReview();
                    }}
                    textStyle={HelperStyles.textView(
                      12,
                      '600',
                      Colors.white,
                      'center',
                      'uppercase',
                    )}
                    title={Strings.post}
                  />
                </View>
              )}
            </View>

            <View
              style={[
                HelperStyles.flexDirection('row'),
                HelperStyles.justView('justifyContent', 'space-between'),
                HelperStyles.justView('marginTop', 8),
              ]}>
              <View
                style={[
                  HelperStyles.flex(0.125),
                  HelperStyles.alignItemsCenteredView('flex-start'),
                  HelperStyles.padding(0, 4),
                ]}>
                <View style={Styles.profileLogoContainer}>
                  <Image
                    source={Assets.logo}
                    style={HelperStyles.imageView('100%', '100%')}
                  />
                </View>
              </View>

              <View
                style={[
                  HelperStyles.flex(0.875),
                  HelperStyles.alignItemsCenteredView('flex-start'),
                ]}>
                <AirbnbRating
                  count={5}
                  defaultRating={rating}
                  onFinishRating={(txt: any) => {
                    Boolean(txt) ? setRating(txt) : setRating(0);
                  }}
                  ratingContainerStyle={HelperStyles.padding(4, 4)}
                  showRating={false}
                  size={20}
                />
              </View>
            </View>

            {renderReviewTextArea()}
          </>
        )}
      </View>
    );
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      {Boolean(storeData) ? (
        <>
          {renderRatings()}

          {renderRateAndReview()}
        </>
      ) : (
        <Indicator />
      )}
    </View>
  );
};

function mapStateToProps(state: any) {
  return {
    loginUserInfo: state?.other?.loginUserInfo ?? null,
  };
}

export default connect(mapStateToProps, null)(StoreReviews);
