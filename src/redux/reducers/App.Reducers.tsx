import {StoreActionsInterface} from '../../configs/ts/Interfaces';
import FakeData from '../../utils/FakeData';
import * as Types from '../Root.Types';

const initialState: any = {
  categories: FakeData.categories,
  items: FakeData.items,
  notifications: [],
  ratingsReviews: [],
  stores: FakeData.stores,
  users: [],
};

function Reducer(
  state: any = initialState,
  action: StoreActionsInterface,
): any {
  switch (action.type) {
    case Types.RESTORE_STATES:
      return action?.payload ?? initialState;

    case Types.STORE_NOTIFICATION:
      return {
        ...state,
        notifications: [...state.notifications, action.payload],
      };

    case Types.STORE_NOTIFICATIONS:
      return {
        ...state,
        notifications: [
          ...new Set([...state.notifications, ...action.payload]),
        ],
      };

    case Types.STORE_RATING_REVIEW:
      return {
        ...state,
        ratingsReviews: [...state.ratingsReviews, action.payload],
      };

    case Types.STORE_RATINGS_REVIEWS:
      return {
        ...state,
        ratingsReviews: [
          ...new Set([...state.ratingsReviews, ...action.payload]),
        ],
      };

    case Types.STORE_USER:
      return {...state, users: [...state.users, action.payload]};

    default:
      return state;
  }
}

export default Reducer;
