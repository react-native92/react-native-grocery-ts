import {StoreActionsInterface} from '../../configs/ts/Interfaces';
import * as Types from '../Root.Types';

const initialState: any = {
  cartItemCount: null,
  fcmToken: null,
  loadingStatus: false,
  loginUserInfo: null,
  notificationStatus: false,
  wishItemCount: null,
};

function OtherReducer(
  state: any = initialState,
  action: StoreActionsInterface,
): any {
  switch (action.type) {
    case Types.CART_ITEM_COUNT:
      return {...state, cartItemCount: action.payload};

    case Types.FCM_TOKEN:
      return {...state, fcmToken: action.payload};

    case Types.LOADING_STATUS:
      return {...state, loadingStatus: action.payload};

    case Types.LOGIN_USER_INFO:
      return {...state, loginUserInfo: action.payload};

    case Types.NOTIFICATION_STATUS:
      return {...state, notificationStatus: action.payload};

    case Types.WISH_ITEM_COUNT:
      return {...state, wishItemCount: action.payload};

    default:
      return state;
  }
}

export default OtherReducer;
