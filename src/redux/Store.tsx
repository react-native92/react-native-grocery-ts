import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import rootReducer from './Root.Reducers';
import thunk from 'redux-thunk';

const middleware: any = applyMiddleware(logger, thunk);

export const initStore: any = () => createStore(rootReducer, middleware);

const store: any = initStore();

export default store;
