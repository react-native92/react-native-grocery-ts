import {combineReducers} from 'redux';
import AppReducer from './reducers/App.Reducers';
import OtherReducer from './reducers/Other.Reducers';
import * as Types from './Root.Types';

const appReducer: any = combineReducers({
  app: AppReducer,
  other: OtherReducer,
});

function rootReducer(state: any, action: any): any {
  Types.LOGOUT === action.type && [(state = undefined)];

  return appReducer(state, action);
}

export default rootReducer;
