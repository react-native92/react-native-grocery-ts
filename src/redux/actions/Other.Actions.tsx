import {
  StoreActionsInterface,
  UserInterface,
} from '../../configs/ts/Interfaces';
import * as Types from '../Root.Types';

// Other Actions

export function loadingStatus(loadingStatus: boolean): StoreActionsInterface {
  return {
    type: Types.LOADING_STATUS,
    payload: loadingStatus,
  };
}

export function notificationStatus(
  notificationStatus: boolean,
): StoreActionsInterface {
  return {
    type: Types.NOTIFICATION_STATUS,
    payload: notificationStatus,
  };
}

export function storeCartItemCount(count: number): StoreActionsInterface {
  return {
    type: Types.CART_ITEM_COUNT,
    payload: count,
  };
}

export function storeFCMToken(fcmToken: string): StoreActionsInterface {
  return {
    type: Types.FCM_TOKEN,
    payload: fcmToken,
  };
}

export function storeLoginUserInfo(
  userData: undefined | UserInterface,
): StoreActionsInterface {
  return {
    type: Types.LOGIN_USER_INFO,
    payload: userData,
  };
}

export function storeWishItemCount(count: number): StoreActionsInterface {
  return {
    type: Types.WISH_ITEM_COUNT,
    payload: count,
  };
}
