import {
  NotificationInterface,
  RatingReviewInterface,
  StoreActionsInterface,
  UserInterface,
} from '../../configs/ts/Interfaces';
import * as Types from '../Root.Types';

// App Actions

export function restoreStates(users: any[]): StoreActionsInterface {
  return {type: Types.RESTORE_STATES, payload: users};
}

export function storeNotification(
  notificationData: NotificationInterface,
): StoreActionsInterface {
  return {type: Types.STORE_NOTIFICATION, payload: notificationData};
}

export function storeNotifications(
  notifications: any[],
): StoreActionsInterface {
  return {type: Types.STORE_NOTIFICATIONS, payload: notifications};
}

export function storeRatingReview(
  ratingData: RatingReviewInterface,
): StoreActionsInterface {
  return {type: Types.STORE_RATING_REVIEW, payload: ratingData};
}

export function storeRatingsReviews(stores: any[]): StoreActionsInterface {
  return {type: Types.STORE_RATINGS_REVIEWS, payload: stores};
}

export function storeUser(userData: UserInterface): StoreActionsInterface {
  return {type: Types.STORE_USER, payload: userData};
}
