import React, {
  FC,
  MutableRefObject,
  ReactElement,
  useEffect,
  useRef,
} from 'react';
import {
  Animated,
  ScrollView,
  StyleProp,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {TabBarInterface} from '../../configs/ts/Interfaces';
import Colors from '../../utils/Colors';
import * as HelperStyles from '../../utils/HelperStyles';

const TabBar: FC<TabBarInterface> = ({
  descriptors,
  getCurrentRoute = () => {},
  horizontalScrollView = false,
  mode = 'default',
  navigation,
  state,
  tabBarActiveTintColor = Colors.primary,
  tabBarInactiveTintColor = Colors.manatee,
  tabBarStyle = null,
  tabBarTextStyle = null,
}) => {
  // Ref Variables
  const scrollViewRef: MutableRefObject<any> = useRef<any>(null);

  // Other Variables
  let customTabBarItem: StyleProp<ViewStyle>;

  useEffect(() => {
    scrollViewRef.current &&
      scrollViewRef.current.scrollTo({
        x: state.index * 100,
        y: 0,
        animated: true,
      });
  }, [state.index]);

  function renderTopTabs(): ReactElement {
    return state.routes.map((route: any, index: number) => {
      const {options}: any = descriptors[route.key];

      const label: string = options.tabBarLabel || options.title || route.name;

      const isFocused: boolean = state.index === index;

      function onPress(): void {
        const event: any = navigation.emit({
          type: 'tabPress',
          target: route.key,
          canPreventDefault: true,
        });

        if (!isFocused && !event.defaultPrevented) {
          getCurrentRoute(route.name);

          navigation.navigate({name: route.name, merge: true});
        }
      }

      switch (mode) {
        case 'default':
        default:
          customTabBarItem = HelperStyles.justifyContentCenteredView('center');
          break;

        case 'space-between':
          customTabBarItem = HelperStyles.justView(
            'justifyContent',
            index % 2 == 0 ? 'flex-start' : 'flex-end',
          );
          break;
      }

      return (
        <TouchableOpacity
          key={index}
          onPress={(): void => onPress()}
          style={HelperStyles.flex(1)}>
          <View style={[HelperStyles.flexDirection('row'), customTabBarItem]}>
            <Animated.Text
              style={[
                HelperStyles.textView(
                  16,
                  isFocused ? '800' : '600',
                  isFocused ? tabBarActiveTintColor : tabBarInactiveTintColor,
                  'center',
                  'capitalize',
                ),
                tabBarTextStyle,
              ]}>
              {label}
            </Animated.Text>
          </View>
        </TouchableOpacity>
      );
    });
  }

  return horizontalScrollView ? (
    <View>
      <ScrollView
        contentContainerStyle={[HelperStyles.flexDirection('row'), tabBarStyle]}
        horizontal={horizontalScrollView}
        ref={list => (scrollViewRef.current = list)}
        showsHorizontalScrollIndicator={false}>
        {renderTopTabs()}
      </ScrollView>
    </View>
  ) : (
    <View style={[HelperStyles.flexDirection('row'), tabBarStyle]}>
      {renderTopTabs()}
    </View>
  );
};

export default TabBar;
