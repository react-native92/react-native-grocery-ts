import React, {FC, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {CardItemInterface} from '../../configs/ts/Interfaces';
import {checkFavorite} from '../../configs/jsons/Category';
import Assets from '../../assets/Index';
import Colors from '../../utils/Colors';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import ImageViewer from './ImageViewer';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Store from '../../redux/Store';
import Styles from '../../styles/otherStyles/CardItem';
import * as HelperStyles from '../../utils/HelperStyles';

const CardItem: FC<CardItemInterface> = ({
  data = null,
  disabled = false,
  onFavorite = (): void => {},
  onPress,
  style,
}) => {
  // CardItem Variables
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [imageViewerStatus, setImageViewerStatus] = useState<boolean>(false);

  // Other Variables
  const userId: number | string =
    Store.getState()?.other?.loginUserInfo?.id ?? null;

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={[Styles.itemContainer, style]}>
      <View style={Styles.imageContainer}>
        {Boolean(data?.weight) && (
          <View style={Styles.weightContainer}>
            <Text
              style={HelperStyles.textView(
                10,
                '800',
                Colors.stowaway,
                'center',
                'uppercase',
              )}>
              {data?.weight}
            </Text>
          </View>
        )}

        <TouchableOpacity
          onPress={(): void => {
            setImageViewerStatus(!imageViewerStatus);
          }}>
          <Image
            onLoadEnd={(): void => {
              setImageLoader(false);
            }}
            onLoadStart={(): void => {
              setImageLoader(true);
            }}
            resizeMode={'contain'}
            source={Boolean(data?.image) ? {uri: data?.image} : Assets.logo}
            style={HelperStyles.imageView(72, 72)}
          />

          {imageLoader && (
            <ActivityIndicator
              color={Colors.manatee}
              size={24}
              style={Styles.activityIndicatorContainer}
            />
          )}

          <ImageViewer
            onClose={(): void => {
              setImageViewerStatus(!imageViewerStatus);
            }}
            onRequestClose={(): void => {
              setImageViewerStatus(!imageViewerStatus);
            }}
            title={data?.item}
            url={data?.image}
            visible={imageViewerStatus}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={(): void => {
            onFavorite();
          }}
          style={Styles.iconContainer}>
          {Boolean(checkFavorite({itemId: data?.itemId, userId})) ? (
            <MaterialIcons color={Colors.primary} name={'favorite'} size={18} />
          ) : (
            <FontAwesomeIcons color={Colors.manatee} name={'heart'} size={18} />
          )}
        </TouchableOpacity>
      </View>

      <View
        style={[
          HelperStyles.flex(0.25),
          HelperStyles.justView('marginTop', 4),
        ]}>
        <Text
          ellipsizeMode={'tail'}
          numberOfLines={1}
          style={HelperStyles.textView(
            12,
            '800',
            Colors.manatee,
            'left',
            'none',
          )}>
          {data?.item}
        </Text>

        <View style={HelperStyles.justView('marginTop', 4)}>
          <Text
            style={HelperStyles.textView(
              12,
              '400',
              Colors.ebony,
              'left',
              'none',
            )}>
            Rs. {parseFloat(String(data?.price)).toFixed(2)}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardItem;
