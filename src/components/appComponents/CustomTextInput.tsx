import React, {FC, Fragment} from 'react';
import {Text, TextInput, View} from 'react-native';
import {CustomTextInputInterface} from '../../configs/ts/Interfaces';
import Colors from '../../utils/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Strings from '../../utils/Strings';
import Styles from '../../styles/otherStyles/CustomTextInput';
import * as HelperStyles from '../../utils/HelperStyles';

const CustomTextInput: FC<CustomTextInputInterface> = ({
  autoCapitalize = 'none',
  autoCorrect = true,
  autoFocus = false,
  color = Colors.manatee,
  containerStyle = null,
  defaultErrorMessage = Strings.errorMessage,
  editable,
  error = false,
  errorMessage = null,
  errorTextStyle = null,
  iconContainer = null,
  keyboardType = 'default',
  maxLength,
  multiline = false,
  onChangeText,
  onEndEditing,
  onPassword,
  passwordIconContainer = null,
  passwordIconStyle = null,
  placeholderTextColor = Colors.manatee,
  placeholder = Strings.textInput,
  renderIcon = (): any => {},
  secureTextEntry = false,
  showIcon = false,
  showPasswordIcon = false,
  size = 20,
  spellCheck = true,
  textContentType = 'none',
  textInputContainerStyle = null,
  textInputStyle = null,
  value,
}) => {
  return (
    <Fragment>
      <View style={[Styles.container, containerStyle]}>
        <View
          style={[
            HelperStyles.flex(showIcon || showPasswordIcon ? 0.9 : 1),
            HelperStyles.justView('alignItems', 'flex-start'),
            textInputContainerStyle,
          ]}>
          <TextInput
            autoCapitalize={autoCapitalize}
            autoCorrect={autoCorrect}
            autoFocus={autoFocus}
            editable={editable}
            keyboardType={keyboardType}
            maxLength={maxLength}
            multiline={multiline}
            onChangeText={onChangeText}
            onEndEditing={onEndEditing}
            placeholderTextColor={placeholderTextColor}
            placeholder={placeholder}
            secureTextEntry={secureTextEntry}
            spellCheck={spellCheck}
            style={[
              HelperStyles.justView('width', '100%'),
              HelperStyles.textView(14, '600', Colors.manatee, 'left', 'none'),
              textInputStyle,
            ]}
            textContentType={textContentType}
            value={value}
          />
        </View>

        {Boolean(value) && showPasswordIcon && (
          <View
            style={[
              HelperStyles.flex(0.1),
              HelperStyles.justifyContentCenteredView('center'),
              passwordIconContainer,
            ]}>
            <MaterialIcons
              color={color}
              name={secureTextEntry ? 'visibility-off' : 'visibility'}
              onPress={onPassword}
              size={size}
              style={passwordIconStyle}
            />
          </View>
        )}

        {showIcon && (
          <View
            style={[
              HelperStyles.flex(0.1),
              HelperStyles.justifyContentCenteredView('center'),
              iconContainer,
            ]}>
            {renderIcon()}
          </View>
        )}
      </View>

      {error && (
        <Text style={[HelperStyles.errorText, errorTextStyle]}>
          {errorMessage ?? defaultErrorMessage}
        </Text>
      )}
    </Fragment>
  );
};

export default CustomTextInput;
