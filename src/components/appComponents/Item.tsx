import React, {
  FC,
  ReactNode,
  useCallback,
  useLayoutEffect,
  useState,
} from 'react';
import {
  ActivityIndicator,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AirbnbRating} from 'react-native-ratings';
import {
  CartInterFace,
  CartItemInterFace,
  CategoryItemInterface,
  FavoriteInterFace,
  ResponseInterface,
} from '../../configs/ts/Interfaces';
import {
  checkCart,
  checkFavorite,
  updateCart,
  updateFavorite,
} from '../../configs/jsons/Category';
import {showMessage} from 'react-native-flash-message';
import {StatusCodes} from '../../configs/ts/Enums';
import {useFocusEffect, useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Button from './Button';
import Colors from '../../utils/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import ImageViewer from './ImageViewer';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import Styles from '../../styles/otherStyles/Item';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const Item: FC = (props: any) => {
  // Props Variables
  const itemData: CategoryItemInterface =
    props?.route?.params?.itemData ?? null;

  // Item Variables
  const [isFavorite, setIsFavorite] = useState<boolean>(false);
  const [quantity, setQuantity] = useState<any>(0);
  const [imageLoader, setImageLoader] = useState<boolean>(true);
  const [imageViewerStatus, setImageViewerStatus] = useState<boolean>(false);

  // Other Variables
  const userId: number | string =
    Store.getState()?.other?.loginUserInfo?.id ?? null;

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();
  const Theme = useTheme().colors;

  useLayoutEffect((): void => {
    props.navigation.setOptions({
      headerRight: (): ReactNode => renderHeaderRight(),
      headerTitle: itemData?.from ?? itemData?.category ?? Strings.item,
    });
  }, [isFavorite]);

  function renderHeaderRight(): ReactNode {
    function handleFavorite(): void {
      const requestData: FavoriteInterFace = {
        itemId: itemData?.itemId,
        userId,
      };

      const response: ResponseInterface = updateFavorite(requestData);

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        setIsFavorite(checkFavorite({itemId: itemData?.itemId, userId}));
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      }
    }

    return (
      <TouchableOpacity
        onPress={(): void => {
          handleFavorite();
        }}>
        {Boolean(isFavorite) ? (
          <MaterialIcons color={Colors.white} name={'favorite'} size={24} />
        ) : (
          <FontAwesomeIcons color={Colors.white} name={'heart'} size={20} />
        )}
      </TouchableOpacity>
    );
  }

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      setIsFavorite(checkFavorite({itemId: itemData?.itemId, userId}));

      getQuatitiesFromCart();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function getQuatitiesFromCart(): void {
    const requestData: Omit<CartInterFace, 'quantity'> = {
      itemId: itemData?.itemId,
      userId,
    };

    const cartItemData: CartItemInterFace | null = checkCart(requestData);

    setQuantity(cartItemData?.qty ?? 1);
  }

  function handleAddToCart(): void {
    const stocks: number = itemData.stocks;

    if (Boolean(stocks) && quantity <= stocks) {
      const requestData: CartInterFace = {
        itemId: itemData?.itemId,
        quantity,
        userId,
      };

      const response: ResponseInterface = updateCart(requestData);

      if (response.status == StatusCodes.Success) {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        props.navigation.goBack();
      } else {
        showMessage({
          description: response.message,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      }
    } else {
      showMessage({
        description: Strings.stockLimitExceed,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    }
  }

  return (
    <View style={HelperStyles.screenContainer(Theme.background)}>
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={[HelperStyles.flex(1), HelperStyles.margin(8, 8)]}>
          <TouchableOpacity
            onPress={(): void => {
              setImageViewerStatus(!imageViewerStatus);
            }}
            style={[
              HelperStyles.justView('height', '25%'),
              HelperStyles.justifyContentCenteredView('center'),
              HelperStyles.padding(4, 4),
            ]}>
            <Image
              onLoadEnd={(): void => {
                setImageLoader(false);
              }}
              onLoadStart={(): void => {
                setImageLoader(true);
              }}
              resizeMode={'contain'}
              source={
                Boolean(itemData?.image)
                  ? {uri: itemData.image}
                  : Assets.logoSquare
              }
              style={HelperStyles.imageView('100%', '100%')}
            />

            {imageLoader && (
              <ActivityIndicator
                color={Colors.manatee}
                size={'large'}
                style={HelperStyles.justView('position', 'absolute')}
              />
            )}

            <ImageViewer
              onClose={(): void => {
                setImageViewerStatus(!imageViewerStatus);
              }}
              onRequestClose={(): void => {
                setImageViewerStatus(!imageViewerStatus);
              }}
              title={itemData?.item}
              url={itemData?.image}
              visible={imageViewerStatus}
            />
          </TouchableOpacity>

          {Boolean(itemData?.weight) && (
            <View style={Styles.weightContainer}>
              <Text
                style={HelperStyles.textView(
                  12,
                  '800',
                  Colors.stowaway,
                  'left',
                  'uppercase',
                )}>
                {itemData.weight}
              </Text>
            </View>
          )}

          <View
            style={[
              HelperStyles.justifyContentCenteredView('center'),
              HelperStyles.padding(4, 4),
            ]}>
            {Boolean(itemData?.item) && (
              <Text
                style={[
                  HelperStyles.textView(
                    20,
                    '400',
                    themeScheme == Strings.dark
                      ? Colors.lightText
                      : Colors.ebony,
                    'center',
                    'none',
                  ),
                  HelperStyles.justView('lineHeight', 20),
                ]}>
                {itemData.item}
              </Text>
            )}

            {Boolean(itemData?.price) && (
              <View style={HelperStyles.justView('marginTop', 8)}>
                <Text
                  style={HelperStyles.textView(
                    18,
                    '400',
                    Colors.stowaway,
                    'center',
                    'none',
                  )}>
                  Rs. {parseFloat(String(itemData.price)).toFixed(2)}
                </Text>
              </View>
            )}

            <View style={HelperStyles.margin(0, 24)}>
              <AirbnbRating
                count={Strings.ratingsCount}
                defaultRating={itemData.ratings}
                isDisabled={true}
                showRating={false}
                size={24}
              />

              {Boolean(itemData?.totalRatings) && (
                <View style={HelperStyles.justView('marginTop', 4)}>
                  <Text
                    style={HelperStyles.textView(
                      12,
                      '400',
                      Colors.lightText,
                      'center',
                      'none',
                    )}>
                    ({itemData.totalRatings})
                  </Text>
                </View>
              )}
            </View>

            <View style={HelperStyles.margin(0, 4)}>
              <Text
                style={HelperStyles.textView(
                  18,
                  '400',
                  themeScheme == Strings.dark ? Colors.ebony : Colors.manatee,
                  'center',
                  'none',
                )}>
                {Strings.quantity}
              </Text>

              <View style={Styles.quantityIncDecContainer}>
                <TouchableOpacity
                  onPress={(): void => {
                    quantity > 1 && setQuantity(quantity - 1);
                  }}
                  style={Styles.iconButtonContainer}>
                  <MaterialIcons
                    color={Colors.white}
                    name={'remove'}
                    size={20}
                  />
                </TouchableOpacity>

                <Text
                  style={HelperStyles.textView(
                    20,
                    '800',
                    themeScheme == Strings.dark
                      ? Colors.lightText
                      : Colors.ebony,
                    'center',
                    'none',
                  )}>
                  {quantity}
                </Text>

                <TouchableOpacity
                  onPress={(): void => {
                    setQuantity(quantity + 1);
                  }}
                  style={Styles.iconButtonContainer}>
                  <MaterialIcons color={Colors.white} name={'add'} size={20} />
                </TouchableOpacity>
              </View>
            </View>

            <View style={HelperStyles.margin(0, 4)}>
              <Button
                containerStyle={Styles.buttonContainer}
                title={Strings.buyNow}
              />

              <Button
                containerStyle={Styles.buttonContainer}
                mode={'light'}
                onPress={(): void => {
                  handleAddToCart();
                }}
                title={Strings.addToCart}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Item;
