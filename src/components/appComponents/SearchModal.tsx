import React, {FC, ReactElement, useCallback, useState} from 'react';
import {Modal, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {
  CategoryItemInterface,
  ResponseInterface,
  SearchModalInterface,
} from '../../configs/ts/Interfaces';
import {
  getAllItems,
  getAppDeals,
  getCategoryItems,
  getMemberDeals,
  getSubCategoryItems,
} from '../../configs/jsons/Category';
import {
  NavigationProp,
  useFocusEffect,
  useNavigation,
  useTheme,
} from '@react-navigation/native';
import {StatusCodes} from '../../configs/ts/Enums';
import {useHeaderHeight} from '@react-navigation/elements';
import Colors from '../../utils/Colors';
import CustomTextInput from './CustomTextInput';
import GoogleCloudSpeechToText, {
  SpeechRecognizeEvent,
} from 'react-native-google-cloud-speech-to-text';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NoResponse from './NoResponse';
import Strings from '../../utils/Strings';
import Styles from '../../styles/otherStyles/SearchModal';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const SearchModal: FC<SearchModalInterface> = ({
  categoryId,
  onClose = (): void => {},
  onRequestClose,
  searchFor = 'all',
  subCategoryId,
  visible,
}) => {
  // SearchModal Variables
  const [searchQuery, setSearchQuery] = useState<any>(null);
  const [started, setStarted] = useState<boolean>(false);
  const [items, setItems] = useState<any>(null);

  // Other Variables
  const headerHeight: number = useHeaderHeight();
  let helperItems: any[] = [];
  const navigation: NavigationProp<any> = useNavigation();

  // Theme Variables
  const Theme = useTheme().colors;

  useFocusEffect(
    useCallback(() => {
      let isFocus: boolean = true;

      fetchItems();

      return (): void => {
        isFocus = false;
      };
    }, []),
  );

  function fetchItems(): void {
    let response: null | ResponseInterface = null;

    switch (searchFor) {
      case 'all':
      default:
        response = getAllItems();
        break;

      case 'app-deals':
        response = getAppDeals();
        break;

      case 'category':
        response = getCategoryItems(categoryId);
        break;

      case 'member-deals':
        response = getMemberDeals();
        break;

      case 'sub-category':
        response = getSubCategoryItems(subCategoryId);
        break;
    }

    if (
      Boolean(response) &&
      response?.status == StatusCodes.Success &&
      Boolean(response?.data) &&
      Array.isArray(response.data)
    ) {
      helperItems =
        searchFor == 'app-deals' || searchFor == 'member-deals'
          ? [...Helpers.sort(response.data, 'item').reverse()]
          : [...response.data];

      setItems(helperItems);
    } else {
      helperItems = [];

      setItems([]);
    }
  }

  useFocusEffect(
    useCallback(() => {
      GoogleCloudSpeechToText.setApiKey(Strings.speechKey);

      GoogleCloudSpeechToText.onSpeechError(onSpeechError);

      GoogleCloudSpeechToText.onSpeechRecognized(onSpeechRecognized);

      GoogleCloudSpeechToText.onSpeechRecognizing(onSpeechRecognizing);

      return () => {
        GoogleCloudSpeechToText.removeListeners();
      };
    }, []),
  );

  function onSpeechError(): void {
    stopRecognizing();

    fetchItems();
  }

  function onSpeechRecognized(result: SpeechRecognizeEvent): void {
    setSearchQuery(result.transcript);

    searchDebounce(result.transcript);

    stopRecognizing();
  }

  function onSpeechRecognizing(result: SpeechRecognizeEvent): void {
    setSearchQuery(result.transcript);

    searchDebounce(result.transcript);
  }

  async function startRecognizing(): Promise<void> {
    setSearchQuery(null);

    setStarted(true);

    await GoogleCloudSpeechToText.start({
      speechToFile: true,
      languageCode: 'en-gb',
    });
  }

  async function stopRecognizing(): Promise<void> {
    setStarted(false);

    await GoogleCloudSpeechToText.stop();
  }

  const searchDebounce = useCallback(
    Helpers.debounce((txt: string) => {
      handleSearch(txt);
    }, Strings.debounceTimeout),
    [],
  );

  function handleSearch(query: string): void {
    let helperArray = [...helperItems];

    helperArray = helperArray.filter((itemData: CategoryItemInterface) =>
      itemData?.item.toLowerCase().includes(query.toLowerCase()),
    );

    setItems(helperArray);
  }

  function renderHeader(): ReactElement {
    return (
      <View
        style={[
          Styles.headerContainer,
          HelperStyles.justView('height', headerHeight),
        ]}>
        <View
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <MaterialIcons color={Colors.white} name={'search'} size={24} />
        </View>

        <View
          style={[
            HelperStyles.flex(0.7),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <CustomTextInput
            autoComplete={'name'}
            containerStyle={Styles.textInputContainerStyle}
            keyboardType={'web-search'}
            onChangeText={(txt: string): void => {
              setSearchQuery(Boolean(txt) ? txt : null);

              searchDebounce(txt);
            }}
            placeholder={Strings.search}
            placeholderTextColor={Colors.white}
            textContentType={'name'}
            textInputStyle={HelperStyles.textView(
              18,
              '600',
              Colors.white,
              'left',
              'none',
            )}
            value={searchQuery}
          />
        </View>

        <View
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <TouchableOpacity
            disabled={true}
            onPress={(): void => {
              started ? stopRecognizing() : startRecognizing();
            }}
            style={HelperStyles.padding(2, 2)}>
            <MaterialIcons
              color={Colors.white}
              name={started ? 'mic-off' : 'mic'}
              size={24}
            />
          </TouchableOpacity>
        </View>

        <View
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <TouchableOpacity
            onPress={(): void => {
              if (Boolean(searchQuery)) {
                setSearchQuery(null);

                fetchItems();
              } else {
                onClose();
              }
            }}
            style={HelperStyles.padding(2, 2)}>
            <MaterialIcons color={Colors.white} name={'close'} size={24} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderItems(): ReactElement {
    return (
      <ScrollView
        contentContainerStyle={HelperStyles.flexGrow(1)}
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        {Boolean(items) && Array.isArray(items) && items.length != 0 ? (
          items.map((itemData: CategoryItemInterface, index: number) => (
            <View key={index} style={HelperStyles.margin(8, 8)}>
              <TouchableOpacity
                onPress={(): void => {
                  setSearchQuery(null);

                  navigation.navigate(Strings.item, {
                    itemData,
                  });
                }}
                style={HelperStyles.padding(8, 8)}>
                <Text
                  style={HelperStyles.textView(
                    14,
                    '600',
                    Colors.lightText,
                    'left',
                    'none',
                  )}>
                  {itemData.item}
                </Text>
              </TouchableOpacity>
            </View>
          ))
        ) : (
          <NoResponse />
        )}
      </ScrollView>
    );
  }

  return (
    <Modal
      animationType={'fade'}
      onRequestClose={onRequestClose}
      transparent={true}
      visible={visible}>
      <View style={HelperStyles.screenContainer(Theme.background)}>
        {renderHeader()}

        {renderItems()}
      </View>
    </Modal>
  );
};

export default SearchModal;
