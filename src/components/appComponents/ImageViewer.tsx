import React, {FC, ReactElement, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  Modal,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {ImageViewerInterface} from '../../configs/ts/Interfaces';
import {useHeaderHeight} from '@react-navigation/elements';
import {useTheme} from '@react-navigation/native';
import Assets from '../../assets/Index';
import Colors from '../../utils/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Styles from '../../styles/otherStyles/ImageViewer';
import WebView from 'react-native-webview';
import * as HelperStyles from '../../utils/HelperStyles';

const ImageViewer: FC<ImageViewerInterface> = ({
  onClose = () => {},
  onRequestClose,
  title,
  url,
  visible,
}) => {
  // ImageViewer Variables
  const [imageLoader, setImageLoader] = useState<boolean>(!Boolean(url));

  // Other Variables
  const headerHeight: number = useHeaderHeight();

  // Theme Variables
  const Theme = useTheme().colors;

  function renderHeader(): ReactElement {
    return (
      <View
        style={[
          Styles.headerContainer,
          HelperStyles.justView('height', headerHeight),
        ]}>
        <View
          style={[
            HelperStyles.flex(0.9),
            HelperStyles.alignItemsCenteredView('flex-start'),
            HelperStyles.padding(8, 0),
          ]}>
          <Text
            ellipsizeMode={'tail'}
            numberOfLines={1}
            style={HelperStyles.textView(
              18,
              '600',
              Colors.white,
              'center',
              'none',
            )}>
            {title}
          </Text>
        </View>

        <View
          style={[
            HelperStyles.flex(0.1),
            HelperStyles.justifyContentCenteredView('center'),
          ]}>
          <TouchableOpacity
            onPress={(): void => {
              onClose();
            }}>
            <MaterialIcons color={Colors.white} name={'close'} size={24} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  function renderImage(): ReactElement {
    function renderLoadingView(): ReactElement {
      return (
        <ActivityIndicator
          color={Colors.primary}
          size={'large'}
          hidesWhenStopped={true}
        />
      );
    }

    return (
      <View style={HelperStyles.flex(1)}>
        {Boolean(url) ? (
          <WebView
            renderLoading={renderLoadingView}
            scalesPageToFit={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            setDisplayZoomControls={true}
            startInLoadingState={true}
            source={{uri: url}}
          />
        ) : (
          <View style={HelperStyles.justifyContentCenteredView('center')}>
            <Image
              onLoadEnd={(): void => {
                setImageLoader(false);
              }}
              onLoadStart={(): void => {
                setImageLoader(true);
              }}
              resizeMode={'contain'}
              source={Assets.logoSquare}
              style={HelperStyles.imageView('100%', '100%')}
            />

            {imageLoader && (
              <ActivityIndicator
                color={Colors.primary}
                size={'large'}
                style={Styles.activityIndicatorContainer}
              />
            )}
          </View>
        )}
      </View>
    );
  }

  return (
    <Modal
      animationType={'slide'}
      onRequestClose={onRequestClose}
      transparent={true}
      visible={visible}>
      <View style={HelperStyles.screenContainer(Theme.background)}>
        {renderHeader()}

        {renderImage()}
      </View>
    </Modal>
  );
};

export default ImageViewer;
