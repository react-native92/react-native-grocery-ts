import React, {FC} from 'react';
import {Image, Text, View} from 'react-native';
import Assets from '../../assets/Index';
import Colors from '../../utils/Colors';
import Strings from '../../utils/Strings';
import * as Helpers from '../../utils/Helpers';
import * as HelperStyles from '../../utils/HelperStyles';

const NoResponse: FC = () => {
  // NoResponse Variables

  return (
    <View
      style={[
        HelperStyles.flex(1),
        HelperStyles.justifyContentCenteredView('center'),
      ]}>
      <Image
        resizeMode={'contain'}
        source={Assets.noResponse}
        style={HelperStyles.imageView(
          Helpers.windowHeight * 0.25,
          Helpers.windowWidth * 0.5,
        )}
      />

      <Text
        style={HelperStyles.textView(
          20,
          '700',
          Colors.primary,
          'center',
          'none',
        )}>
        {Strings.whoops}
      </Text>

      <Text
        style={[
          HelperStyles.textView(16, '600', Colors.lightText, 'center', 'none'),
          HelperStyles.margin(0, 8),
        ]}>
        {Strings.notFindAnything}
      </Text>
    </View>
  );
};

export default NoResponse;
