import React, {FC, ReactElement} from 'react';
import {
  ActivityIndicator,
  Keyboard,
  StyleProp,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import {ButtonInterface} from '../../configs/ts/Interfaces';
import Colors from '../../utils/Colors';
import Strings from '../../utils/Strings';
import Styles from '../../styles/otherStyles/Button';
import * as HelperStyles from '../../utils/HelperStyles';

const Button: FC<ButtonInterface> = ({
  containerStyle = null,
  disabled = false,
  isImage = false,
  title = Strings.button,
  loading = false,
  mode = 'solid',
  onPress,
  renderImage = (): any => {},
  textStyle = null,
}) => {
  // Button Variables

  // Other Variables
  let customContainerStyle: StyleProp<ViewStyle>,
    customTextStyle: StyleProp<TextStyle>;

  switch (mode) {
    case 'light':
      customContainerStyle = {
        backgroundColor: Colors.white,
        borderColor: Colors.white,
        borderRadius: 4,
        borderWidth: 0.5,
      };

      customTextStyle = {color: Colors.primary, bottom: isImage ? 1 : 0};
      break;

    case 'solid':
    default:
      customContainerStyle = {
        backgroundColor: Colors.primary,
        borderColor: Colors.primary,
        borderRadius: 4,
        borderWidth: 0.5,
      };

      customTextStyle = {color: Colors.white, bottom: isImage ? 1 : 0};
      break;
  }

  function renderButton(): ReactElement {
    return (
      <View
        style={[
          HelperStyles.flex(1),
          HelperStyles.justifyContentCenteredView('center'),
        ]}>
        <View style={HelperStyles.justifyContentCenteredView('center')}>
          {loading ? (
            <ActivityIndicator size="small" color={Colors.white} />
          ) : (
            <View
              style={[
                HelperStyles.flex(1),
                HelperStyles.flexDirection('row'),
                HelperStyles.justifyContentCenteredView('center'),
              ]}>
              {isImage && renderImage()}

              <Text
                style={[
                  HelperStyles.textView(
                    16,
                    '600',
                    Colors.white,
                    'center',
                    'uppercase',
                  ),
                  customTextStyle,
                  textStyle,
                ]}>
                {title}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }

  return (
    <TouchableOpacity
      disabled={disabled || loading}
      style={[
        Styles.buttonContainer,
        customContainerStyle,
        containerStyle,
        loading && HelperStyles.justView('backgroundColor', Colors.primary),
      ]}
      onPressIn={() => {
        Keyboard.dismiss();
      }}
      onPress={onPress}>
      {renderButton()}
    </TouchableOpacity>
  );
};

export default Button;
