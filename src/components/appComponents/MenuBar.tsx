import React, {FC, ReactElement} from 'react';
import {Modal, Text, TouchableOpacity, View} from 'react-native';
import {MenuBarInterface} from '../../configs/ts/Interfaces';
import {
  NavigationProp,
  useNavigation,
  useTheme,
} from '@react-navigation/native';
import {showMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Colors from '../../utils/Colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Strings from '../../utils/Strings';
import Styles from '../../styles/otherStyles/MenuBar';
import * as HelperStyles from '../../utils/HelperStyles';

const MenuBar: FC<MenuBarInterface> = ({onRequestClose, visible}) => {
  // MenuBar Variables

  // Other Variables
  const navigation: NavigationProp<any> = useNavigation();
  const menus: any[] = [
    Strings.groceryShopping,
    Strings.orderHistory,
    Strings.trackOrders,
    Strings.currency,
    Strings.storeLocator,
    Strings.termsConditions,
    Strings.help,
    Strings.gotAQuestion,
    Strings.logout,
  ];

  // Theme Variables
  const Theme = useTheme().colors;

  function handleMenuItem(item: string): any {
    let iconName: string = '',
      onPress: () => void;

    switch (item) {
      case Strings.currency:
        iconName = 'monetization-on';

        onPress = (): void => {};
        break;

      case Strings.gotAQuestion:
        iconName = 'live-help';

        onPress = (): void => {};
        break;

      case Strings.groceryShopping:
      default:
        iconName = 'close';

        onPress = (): void => {};
        break;

      case Strings.help:
        iconName = 'help';

        onPress = (): void => {};
        break;

      case Strings.logout:
        iconName = 'logout';

        onPress = (): void => handleLogout();

        break;

      case Strings.orderHistory:
        iconName = 'list-alt';

        onPress = (): void => {};
        break;

      case Strings.storeLocator:
        iconName = 'store';

        onPress = (): void => {
          navigation.navigate(Strings.storeLocator);
        };
        break;

      case Strings.termsConditions:
        iconName = 'error';

        onPress = (): void => {};
        break;

      case Strings.trackOrders:
        iconName = 'pin-drop';

        onPress = (): void => {};
        break;
    }

    return {iconName, onPress};
  }

  function handleLogout() {
    AsyncStorage.getAllKeys()
      .then(keys => AsyncStorage.multiRemove(keys))
      .then(() => {
        showMessage({
          description: Strings.logoutSuccess,
          icon: 'auto',
          message: Strings.success,
          type: 'success',
        });

        navigation.navigate(Strings.onBoard);
      })
      .catch(() => {
        showMessage({
          description: Strings.asyncStorageCache,
          icon: 'auto',
          message: Strings.error,
          type: 'danger',
        });
      });
  }

  return (
    <Modal
      animationType={'fade'}
      onRequestClose={onRequestClose}
      transparent={true}
      visible={visible}>
      <View style={HelperStyles.screenContainer(Colors.stroke50)}>
        <View
          style={[
            HelperStyles.justView('backgroundColor', Theme.card),
            Styles.menuBarModalContainer,
          ]}>
          {menus.map(
            (lol: string, index: number): ReactElement => (
              <TouchableOpacity
                disabled={lol == Strings.groceryShopping}
                key={index}
                onPress={(): void => {
                  onRequestClose();

                  handleMenuItem(lol).onPress();
                }}
                style={[
                  HelperStyles.flexDirection('row'),
                  HelperStyles.justifyContentCenteredView('space-between'),
                  HelperStyles.padding(4, 8),
                ]}>
                <View
                  style={[
                    HelperStyles.flex(0.15),
                    HelperStyles.alignItemsCenteredView(
                      lol == Strings.groceryShopping ? 'flex-start' : 'center',
                    ),
                  ]}>
                  <TouchableOpacity
                    disabled={lol != Strings.groceryShopping}
                    onPress={(): void => {
                      onRequestClose();

                      handleMenuItem(lol).onPress();
                    }}
                    style={[
                      HelperStyles.imageView(40, 40),
                      HelperStyles.justifyContentCenteredView('center'),
                      HelperStyles.justView(
                        'backgroundColor',
                        lol == Strings.groceryShopping
                          ? Colors.white
                          : Colors.iceberg,
                      ),
                      HelperStyles.justView('borderRadius', 40 / 2),
                    ]}>
                    <MaterialIcons
                      color={
                        lol == Strings.groceryShopping
                          ? Colors.manatee
                          : Colors.primary
                      }
                      name={handleMenuItem(lol).iconName}
                      size={lol == Strings.groceryShopping ? 24 : 20}
                    />
                  </TouchableOpacity>
                </View>

                <View
                  style={[
                    HelperStyles.flex(0.875),
                    HelperStyles.alignItemsCenteredView('flex-start'),
                    HelperStyles.padding(4, 4),
                  ]}>
                  <Text
                    style={HelperStyles.textView(
                      lol == Strings.groceryShopping ? 16 : 14,
                      '600',
                      Colors.ebony,
                      'center',
                      'none',
                    )}>
                    {lol}
                  </Text>
                </View>
              </TouchableOpacity>
            ),
          )}
        </View>
      </View>
    </Modal>
  );
};

export default MenuBar;
