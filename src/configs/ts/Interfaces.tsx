import {ReactElement} from 'react';
import {
  ButtonProps,
  GestureResponderEvent,
  ModalProps,
  StyleProp,
  TextInputProps,
  TextStyle,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';
import {
  ButtonModeType,
  KeyStrValAllType,
  KeyStrValNumStrType,
  SearchForType,
  TabBarModeType,
} from './Types';
import {IconProps} from 'react-native-vector-icons/Icon';
import {StatusCodes} from './Enums';

export interface ButtonInterface
  extends Omit<ButtonProps, 'disabled' | 'onPress'>,
    Omit<TouchableOpacityProps, 'disabled' | 'style'> {
  containerStyle?: null | StyleProp<ViewStyle>;
  disabled?: boolean;
  isImage?: boolean;
  loading?: boolean;
  mode?: ButtonModeType;
  renderImage?: () => ReactElement;
  textStyle?: null | StyleProp<TextStyle>;
  touchable?: boolean;
}

export interface CardItemInterface extends TouchableOpacityProps {
  data: null | CategoryItemInterface;
  onFavorite: () => void;
}

export interface CartInterFace extends Pick<CategoryItemInterface, 'itemId'> {
  itemId: number | string | undefined;
  quantity: number | string;
  userId: null | number | string;
}

export interface CartItemInterFace extends CategoryItemInterface {
  qty: number | string;
}

export interface CategoryInterface extends KeyStrValAllType {
  categoryId: number | string;
  category: string;
  icon: string;
  iconType: string;
  image: string;
  subCategories: any[];
}

export interface CategoryItemInterface extends KeyStrValAllType {
  category: string;
  categoryId: number | string;
  itemId: number | string | undefined;
  image: string;
  isAppDeals: boolean;
  isMemberDeals: boolean;
  item: string;
  price: number;
  ratings: number;
  stocks: number;
  subCategory: string;
  subCategoryId: number | string;
  totalRatings: number;
  weight: string;
}

export interface CustomTextInputInterface
  extends Partial<IconProps>,
    Omit<TextInputProps, 'style'> {
  containerStyle?: null | StyleProp<ViewStyle>;
  defaultErrorMessage?: null | string;
  error?: boolean;
  errorMessage?: null | string;
  errorTextStyle?: null | StyleProp<TextStyle> | StyleProp<ViewStyle>;
  iconContainer?: null | StyleProp<ViewStyle>;
  onPassword?: ((event: GestureResponderEvent) => void) | undefined;
  passwordIconContainer?: null | StyleProp<ViewStyle>;
  passwordIconStyle?: null | StyleProp<ViewStyle>;
  renderIcon?: () => ReactElement;
  showIcon?: boolean;
  showPasswordIcon?: boolean;
  textInputContainerStyle?: null | StyleProp<ViewStyle>;
  textInputStyle?: null | StyleProp<TextStyle> | StyleProp<ViewStyle>;
}

export interface FakeDataInterface extends KeyStrValAllType {
  categories: any[];
  items: any[];
}

export interface FavoriteInterFace
  extends Pick<CategoryItemInterface, 'itemId'> {
  itemId: number | string | undefined;
  userId: null | number | string;
}

export interface ImageViewerInterface extends ModalProps {
  onClose: () => void;
  title: null | string | undefined;
  url: any;
}

export interface LoginInterFace
  extends Pick<UserInterface, 'emailAddress' | 'password'> {}

export interface MenuBarInterface extends ModalProps {
  onRequestClose: () => void;
}

export interface NotificationInterface extends KeyStrValAllType {
  createdAt: number | string;
  description: string | undefined;
  isDelete: boolean;
  notificationId?: number | string;
  slug?: null | string;
  title: string | undefined;
  userId: any;
}

export interface OTPInterFace
  extends Pick<UserInterface, 'emailAddress' | 'mobileNumber'> {}

export interface RatingReviewInterface extends KeyStrValAllType {
  createdAt: number | string;
  isDelete: boolean;
  rating: number | string;
  ratingReviewId?: number | string;
  review: null | string;
  storeId: number | string;
  userId: any;
}

export interface RatingReviewCheckInterface
  extends Pick<RatingReviewInterface, 'storeId' | 'userId'> {}

export interface ResendOTPInterFace
  extends Pick<UserInterface, 'mobileNumber'> {}

export interface ResponseInterface {
  data: any;
  message: string;
  status: StatusCodes;
}

export interface ReviewInterface extends KeyStrValAllType {
  createdAt: number | string;
  review: string;
  reviewId?: number | string;
}

export interface SearchModalInterface extends ModalProps {
  categoryId?: number | string;
  onClose: () => void;
  searchFor: SearchForType;
  subCategoryId?: number | string;
}

export interface StoreActionsInterface extends KeyStrValAllType {
  payload?: any;
  type: string;
}

export interface StoreCoordinatesInterface extends KeyStrValNumStrType {
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}

export interface StoreInterface extends KeyStrValAllType {
  address: string;
  averageRatings?: number | string;
  closeTime?: string;
  coordinates: StoreCoordinatesInterface;
  image: string;
  mobileNumber: number | string;
  name: string;
  openTime?: string;
  payments?: any;
  ratings?: number | string;
  services?: any;
  storeId: number | string;
  totalRatings?: number | string;
  website?: string;
}

export interface SubCategoryInterface extends KeyStrValNumStrType {
  category: string;
  categoryId: number | string;
  subCategory: string;
  subCategoryId: number | string;
}

export interface TabBarInterface {
  descriptors?: any;
  getCurrentRoute?: (routeName: string) => void;
  horizontalScrollView?: boolean;
  mode?: TabBarModeType;
  navigation?: any;
  state?: any;
  tabBarActiveTintColor?: string;
  tabBarInactiveTintColor?: string;
  tabBarStyle?: null | StyleProp<ViewStyle>;
  tabBarTextStyle?: null | StyleProp<TextStyle>;
}

export interface UserInterface extends KeyStrValAllType {
  cart?: any;
  emailAddress: string;
  favorites?: any;
  id?: null | number | string;
  isVerified?: boolean;
  mobileNumber?: null | number | string;
  otp?: null | number | string;
  password: string;
  username: string;
}

export interface VerifyOTPInterFace
  extends Pick<UserInterface, 'mobileNumber' | 'otp'> {}
