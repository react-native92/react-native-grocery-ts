export enum StatusCodes {
  Accepted = 202,
  BadRequest = 400,
  Error = 500,
  NotFound = 404,
  Success = 200,
}
