import {StyleProp, ViewStyle} from 'react-native';

export type ButtonModeType = 'light' | 'solid';

export type KeyStrValAllType = {[index: string]: any};

export type KeyStrValNumStrType = {[index: string]: number | string};

export type KeyStrValNumType = {[index: string]: number};

export type KeyStrValStrType = {[index: string]: string};

export type SearchForType =
  | 'all'
  | 'app-deals'
  | 'category'
  | 'member-deals'
  | 'sub-category';

export type TabBarModeType = 'default' | 'space-between';

export type ViewStyleType = (value: number) => StyleProp<ViewStyle>;
