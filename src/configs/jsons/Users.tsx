import {decrypt, encrypt, writeJSON} from '../../utils/Helpers';
import {
  CartItemInterFace,
  LoginInterFace,
  NotificationInterface,
  OTPInterFace,
  ResendOTPInterFace,
  ResponseInterface,
  UserInterface,
  VerifyOTPInterFace,
} from '../ts/Interfaces';
import {createNotification} from './Notifications';
import {StatusCodes} from '../ts/Enums';
import {storeUser} from '../../redux/Root.Actions';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import moment from 'moment';

// Users Variables
let responseData: ResponseInterface;

export function checkAvailability(query: string): boolean {
  if (Boolean(query)) {
    const userData: undefined | UserInterface = fetchUser(query);

    return Boolean(userData) ? true : false;
  } else {
    return false;
  }
}

export function fetchUser(query: any): undefined | UserInterface {
  const users: any[] = getUsers();

  const userData: any = users.find(
    (lol: UserInterface) =>
      lol.id == query ||
      lol.username == query ||
      lol.emailAddress == query ||
      lol.mobileNumber == query,
  );

  return userData;
}

export function getOTP(query: OTPInterFace): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(query.emailAddress);

  if (Boolean(userData)) {
    const otp = generateOTP();

    const userUpdatedData: undefined | UserInterface = updateUser({
      ...query,
      otp,
    });

    if (Boolean(userUpdatedData)) {
      responseData = {
        status: StatusCodes.Success,
        message: Strings.otpGenerateSuccess,
        data: {otp},
      };
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.otpGenerateError,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

function generateOTP(length: number = Strings.otpLength): string {
  const digits: string = '0123456789';

  let otp: string = '';

  for (let i: number = 1; i <= length; i++) {
    const index: number = Math.floor(Math.random() * digits.length);

    otp = otp + digits[index];
  }

  return otp;
}

export function getUsers(): any[] {
  const users: any[] = Store.getState()?.app?.users ?? [];

  return users;
}

export function loginUser(query: LoginInterFace): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(query.emailAddress);

  if (Boolean(userData)) {
    if (decrypt(userData?.password) == query.password) {
      responseData = {
        status: StatusCodes.Success,
        message: Strings.loginSuccess,
        data: userData,
      };
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.loginInvalid,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

export function registerUser(requestData: UserInterface): ResponseInterface {
  requestData = {
    ...requestData,
    id: getUsers().length + 1,
    password: encrypt(requestData.password),
    mobileNumber: null,
    otp: null,
    isVerified: false,
    favorites: [],
    cart: [],
  };

  const userData: undefined | UserInterface = fetchUser(requestData.username);

  if (!Boolean(userData)) {
    try {
      Store.dispatch(storeUser(requestData));

      const notifyRequestData: NotificationInterface = {
        createdAt: moment().format(`${Strings.Y4M2D2} ${Strings.H2m2s2}`),
        description: `Welcome ${requestData.username.toLocaleUpperCase()}! ${
          Strings.accountRegisteredNotify
        }`,
        isDelete: false,
        title: Strings.welcomeBoard,
        userId: requestData.id,
      };

      createNotification(notifyRequestData);

      responseData = {
        status: StatusCodes.Success,
        message: Strings.registerationSuccess,
        data: null,
      };
    } catch (error) {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.registerationCache,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.Error,
      message: Strings.userExists,
      data: null,
    };
  }

  return responseData;
}

export function resendOTP(query: ResendOTPInterFace): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(query.mobileNumber);

  if (Boolean(userData)) {
    const otp = generateOTP();

    const userUpdatedData: undefined | UserInterface = updateUser({
      ...query,
      otp,
    });

    if (Boolean(userUpdatedData)) {
      responseData = {
        status: StatusCodes.Success,
        message: Strings.resendOTPSuccess,
        data: {otp},
      };
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.resendOTPError,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

export function resetPassword(query: LoginInterFace): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(query.emailAddress);

  if (Boolean(userData)) {
    const userUpdatedData: undefined | UserInterface = updateUser(query);

    if (Boolean(userUpdatedData)) {
      responseData = {
        status: StatusCodes.Success,
        message: Strings.resetPasswordSuccess,
        data: null,
      };
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.resetPasswordError,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

export function updateUser(
  query: Partial<UserInterface>,
): undefined | UserInterface {
  const users: any[] = getUsers();

  const index: number = users.findIndex(
    (lol: UserInterface) =>
      lol.id == query.id ||
      lol.username == query.username ||
      lol.emailAddress == query.emailAddress ||
      lol.mobileNumber == query.mobileNumber,
  );

  if (index != -1) {
    users[index].username = query.username ?? users[index].username;

    users[index].password = Boolean(query.password)
      ? encrypt(query.password)
      : users[index].password;

    users[index].mobileNumber = query.mobileNumber ?? users[index].mobileNumber;

    users[index].otp = Boolean(query.otp)
      ? encrypt(query.otp)
      : users[index].otp;

    users[index].isVerified = query.isVerified ?? users[index].isVerified;

    users[index].favorites = Boolean(query?.favorites)
      ? handleUserFavorite(users[index].favorites, query.favorites)
      : users[index].favorites;

    users[index].cart = Boolean(query?.cart)
      ? handleUserCart(users[index].cart, query.cart)
      : users[index].cart;

    writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

    return users[index];
  } else {
    return undefined;
  }
}

export function handleUserFavorite(
  userFavorites: any[],
  queryFavorite: number | string,
): any[] {
  if (!userFavorites.includes(queryFavorite)) {
    userFavorites.push(queryFavorite);
  } else {
    const index: number = userFavorites.indexOf(queryFavorite);

    index > -1 && userFavorites.splice(index, 1);
  }

  return userFavorites;
}

export function handleUserCart(
  userCart: any[],
  queryCart: CartItemInterFace,
): any[] {
  const index: number = userCart.findIndex(
    (lol: CartItemInterFace) => lol.itemId == queryCart.itemId,
  );

  if (index == -1) {
    userCart.push(queryCart);
  } else if (queryCart.qty != 0) {
    userCart[index].qty = queryCart.qty;
  } else {
    userCart.splice(index, 1);
  }

  return userCart;
}

export function verifyOTP(query: VerifyOTPInterFace): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(query.mobileNumber);

  if (Boolean(userData)) {
    const userUpdatedData: undefined | UserInterface = updateUser({
      mobileNumber: query.mobileNumber,
      isVerified: userData?.otp == encrypt(query.otp),
    });

    if (Boolean(userUpdatedData)) {
      const notifyRequestData: NotificationInterface = {
        createdAt: moment().format(`${Strings.Y4M2D2} ${Strings.H2m2s2}`),
        description: `Hey ${userUpdatedData?.username.toLocaleUpperCase()}! ${
          Strings.accountVerifiedNotify
        }`,
        isDelete: false,
        title: Strings.verification,
        userId: userUpdatedData?.id,
      };

      createNotification(notifyRequestData);

      responseData = {
        status: StatusCodes.Success,
        message: Strings.verifyOTPSuccess,
        data: userUpdatedData,
      };
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.verifyOTPError,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}
