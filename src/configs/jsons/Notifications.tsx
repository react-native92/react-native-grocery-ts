import {fetchUser} from './Users';
import {
  NotificationInterface,
  ResponseInterface,
  UserInterface,
} from '../ts/Interfaces';
import {
  notificationStatus,
  storeNotification,
  storeNotifications,
} from '../../redux/Root.Actions';
import {StatusCodes} from '../ts/Enums';
import {writeJSON} from '../../utils/Helpers';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';

// Notifications Variables
let responseData: ResponseInterface;

export function clearNotification(
  notificationId: number | string,
): ResponseInterface {
  const notifications: any[] = getNotifications();

  const index: number = notifications.findIndex(
    (notificationData: NotificationInterface) =>
      notificationData.notificationId == notificationId,
  );

  if (index != -1) {
    notifications[index].isDelete = true;

    try {
      Store.dispatch(storeNotifications(notifications));

      writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

      responseData = {
        status: StatusCodes.Success,
        message: Strings.notificationClearSuccess,
        data: null,
      };
    } catch (error) {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.notificationClearCache,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.notificationNotFound,
      data: null,
    };
  }

  return responseData;
}

export function clearNotifications(clearIds: any[]): ResponseInterface {
  if (clearIds.length != 0) {
    const notifications: any[] = getNotifications();

    clearIds.map((id: number | string) => {
      const index: number | string = notifications.findIndex(
        (notificationData: NotificationInterface) =>
          notificationData.notificationId == id,
      );

      notifications[index].isDelete = true;
    });

    try {
      Store.dispatch(storeNotifications(notifications));

      writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

      responseData = {
        status: StatusCodes.Success,
        message: Strings.notificationsClearSuccess,
        data: null,
      };
    } catch (error) {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.notificationsClearCache,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.notificationIdsNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getNotifications(): any[] {
  const notifications: any[] = Store.getState()?.app?.notifications ?? [];

  return notifications;
}

export function getUserNotifications(
  userId: number | string,
): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(userId);

  if (Boolean(userData)) {
    const notifications: any[] = getNotifications();

    const userNotifications: any[] = notifications.filter(
      (notificationData: NotificationInterface) =>
        notificationData.userId == userId && !notificationData.isDelete,
    );

    if (userNotifications.length != 0) {
      responseData = {
        status: StatusCodes.Success,
        message: Strings.notificationSuccess,
        data: userNotifications.reverse(),
      };
    } else {
      responseData = {
        status: StatusCodes.NotFound,
        message: Strings.notificationEmpty,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

export function createNotification(
  requestData: NotificationInterface,
): ResponseInterface {
  requestData = {
    ...requestData,
    notificationId: getNotifications().length + 1,
  };

  try {
    Store.dispatch(storeNotification(requestData));

    Store.dispatch(notificationStatus(true));

    writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

    responseData = {
      status: StatusCodes.Success,
      message: Strings.createNotificationSuccess,
      data: null,
    };
  } catch (error) {
    responseData = {
      status: StatusCodes.Error,
      message: Strings.createNotificationCache,
      data: null,
    };
  }

  return responseData;
}
