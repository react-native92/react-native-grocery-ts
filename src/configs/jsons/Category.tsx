import {
  CartInterFace,
  CartItemInterFace,
  CategoryItemInterface,
  FavoriteInterFace,
  ResponseInterface,
  UserInterface,
} from '../ts/Interfaces';
import {fetchUser, updateUser} from './Users';
import {StatusCodes} from '../ts/Enums';
import {storeCartItemCount, storeWishItemCount} from '../../redux/Root.Actions';
import FakeData from '../../utils/FakeData';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import * as Helpers from '../../utils/Helpers';

// Category Variables
let responseData: ResponseInterface;

export function checkCart(
  query: Omit<CartInterFace, 'quantity'>,
): CartItemInterFace | null {
  if (Boolean(query)) {
    const userData: undefined | UserInterface = fetchUser(query.userId);

    if (Boolean(userData)) {
      const cart: any[] = userData?.cart;

      const cartItemData: CartItemInterFace = cart.find(
        (lol: CartItemInterFace) => lol.itemId == query.itemId,
      );

      return cartItemData;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function checkFavorite(query: FavoriteInterFace): boolean {
  if (Boolean(query)) {
    const userData: undefined | UserInterface = fetchUser(query.userId);

    if (Boolean(userData)) {
      const favorites: any[] = userData?.favorites;

      return favorites.includes(query.itemId);
    } else {
      return false;
    }
  } else {
    return false;
  }
}

export function fetchItem(query: any): CategoryItemInterface | undefined {
  const items: any[] = getItems();

  const itemData: any = items.find(
    (lol: CategoryItemInterface) => lol.itemId == query,
  );

  return itemData;
}

export function getAllItems(): ResponseInterface {
  let items: any[] = getItems();

  if (items.length != 0) {
    items = Helpers.sort(items, 'item');

    responseData = {
      status: StatusCodes.Success,
      message: Strings.itemsSuccess,
      data: items.reverse(),
    };
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.itemsNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getAppDeals(): ResponseInterface {
  const items: any[] = getItems();

  let appDeals: any[] = items.filter((lol: CategoryItemInterface) =>
    Boolean(lol.isAppDeals),
  );

  if (appDeals.length != 0) {
    appDeals = Helpers.randomizeArray(appDeals);

    responseData = {
      status: StatusCodes.Success,
      message: Strings.dealsSuccess,
      data: appDeals,
    };
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.dealsNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getCart(userId: number | string): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(userId);

  if (Boolean(userData)) {
    const userCart: any[] = Store.getState().other.loginUserInfo?.cart ?? [];

    if (userCart.length != 0) {
      responseData = {
        status: StatusCodes.Success,
        message: Strings.getCartSuccess,
        data: userCart,
      };
    } else {
      responseData = {
        status: StatusCodes.NotFound,
        message: Strings.cartEmpty,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getCategoryItems(
  categoryId: number | string | undefined,
): ResponseInterface {
  const items: any[] = getItems();

  let categoryItems: any[] = items.filter(
    (lol: CategoryItemInterface) => lol.categoryId == categoryId,
  );

  if (categoryItems.length != 0) {
    categoryItems = Helpers.sort(categoryItems, 'item');

    responseData = {
      status: StatusCodes.Success,
      message: Strings.itemsSuccess,
      data: categoryItems.reverse(),
    };
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.itemsNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getItems(): any[] {
  const items: any[] = FakeData?.items ?? [];

  return items;
}

export function getMemberDeals(): ResponseInterface {
  const items: any[] = getItems();

  let memberDeals: any[] = items.filter((lol: CategoryItemInterface) =>
    Boolean(lol.isMemberDeals),
  );

  if (memberDeals.length != 0) {
    memberDeals = Helpers.randomizeArray(memberDeals);

    responseData = {
      status: StatusCodes.Success,
      message: Strings.dealsSuccess,
      data: memberDeals,
    };
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.dealsNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getSubCategoryItems(
  subCategoryId: number | string | undefined,
): ResponseInterface {
  const items: any[] = getItems();

  let subCategoryItems: any[] = items.filter(
    (lol: CategoryItemInterface) => lol.subCategoryId == subCategoryId,
  );

  if (subCategoryItems.length != 0) {
    subCategoryItems = Helpers.sort(subCategoryItems, 'item');

    responseData = {
      status: StatusCodes.Success,
      message: Strings.itemsSuccess,
      data: subCategoryItems.reverse(),
    };
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.itemsNotFound,
      data: null,
    };
  }

  return responseData;
}

export function getWishList(userId: number | string): ResponseInterface {
  const userData: undefined | UserInterface = fetchUser(userId);

  if (Boolean(userData)) {
    const userWishList: any[] =
      Store.getState().other.loginUserInfo?.favorites ?? [];

    if (userWishList.length != 0) {
      let helperArray: any[] = [];

      userWishList.forEach((element: number | string) => {
        const itemData: CategoryItemInterface | undefined = fetchItem(element);

        Boolean(itemData) && helperArray.push(itemData);
      });

      responseData = {
        status: StatusCodes.Success,
        message: Strings.wishListSuccess,
        data: helperArray,
      };
    } else {
      responseData = {
        status: StatusCodes.NotFound,
        message: Strings.wishListEmpty,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.userNotFound,
      data: null,
    };
  }

  return responseData;
}

export function updateCart(query: CartInterFace): ResponseInterface {
  const itemData: CategoryItemInterface | undefined = fetchItem(query.itemId);

  if (Boolean(itemData)) {
    const userData: undefined | UserInterface = fetchUser(query.userId);

    if (Boolean(userData)) {
      const cartItemData: CartItemInterFace | null = checkCart(query);

      const userUpdatedData: undefined | UserInterface = updateUser({
        id: query.userId,
        cart: {...itemData, qty: query.quantity},
      });

      if (Boolean(userUpdatedData)) {
        (!Boolean(cartItemData) || !Boolean(query.quantity)) &&
          Store.dispatch(storeCartItemCount(userUpdatedData?.cart.length));

        responseData = {
          status: StatusCodes.Success,
          message: Boolean(cartItemData)
            ? Strings.cartUpdateSuccess
            : Strings.cartSuccess,
          data: null,
        };
      } else {
        responseData = {
          status: StatusCodes.Error,
          message: Boolean(cartItemData)
            ? Strings.cartUpdateError
            : Strings.cartError,
          data: null,
        };
      }
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.userNotFound,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.Error,
      message: Strings.itemNotFound,
      data: null,
    };
  }

  return responseData;
}

export function updateFavorite(query: FavoriteInterFace): ResponseInterface {
  const itemData: CategoryItemInterface | undefined = fetchItem(query.itemId);

  if (Boolean(itemData)) {
    const userData: undefined | UserInterface = fetchUser(query.userId);

    if (Boolean(userData)) {
      const userUpdatedData: undefined | UserInterface = updateUser({
        id: query.userId,
        favorites: query.itemId,
      });

      if (Boolean(userUpdatedData)) {
        Store.dispatch(storeWishItemCount(userUpdatedData?.favorites.length));

        responseData = {
          status: StatusCodes.Success,
          message: Strings.favouriteSuccess,
          data: null,
        };
      } else {
        responseData = {
          status: StatusCodes.Error,
          message: Strings.favouriteError,
          data: null,
        };
      }
    } else {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.userNotFound,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.Error,
      message: Strings.itemNotFound,
      data: null,
    };
  }

  return responseData;
}
