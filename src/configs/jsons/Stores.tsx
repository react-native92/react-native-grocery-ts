import {fetchUser} from './Users';
import {
  RatingReviewCheckInterface,
  RatingReviewInterface,
  ResponseInterface,
  ReviewInterface,
  StoreInterface,
  UserInterface,
} from '../ts/Interfaces';
import {StatusCodes} from '../ts/Enums';
import {storeRatingReview, storeRatingsReviews} from '../../redux/Root.Actions';
import Store from '../../redux/Store';
import Strings from '../../utils/Strings';
import {writeJSON} from '../../utils/Helpers';

// Stores Variables
let responseData: ResponseInterface;

export function checkRatingsReviews(
  query: RatingReviewCheckInterface,
): null | RatingReviewInterface {
  if (Boolean(query.storeId) && Boolean(query.userId)) {
    const userData: undefined | UserInterface = fetchUser(query.userId);
    if (Boolean(userData)) {
      const ratingsReviews: any[] = getRatingsReviews();

      const ratingReviewData: RatingReviewInterface = ratingsReviews.find(
        (lol: RatingReviewInterface) =>
          lol.storeId == query.storeId && lol.userId == query.userId,
      );

      return ratingReviewData;
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function createRatingReview(
  requestData: RatingReviewInterface,
): ResponseInterface {
  requestData = {
    ...requestData,
    ratingReviewId: getRatingsReviews().length + 1,
  };

  try {
    Store.dispatch(storeRatingReview(requestData));

    writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

    responseData = {
      status: StatusCodes.Success,
      message: Strings.createRatingSuccess,
      data: null,
    };
  } catch (error) {
    responseData = {
      status: StatusCodes.Error,
      message: Strings.createRatingCache,
      data: null,
    };
  }

  return responseData;
}

export function deleteRatingReview(
  ratingReviewId: number | string | undefined,
): ResponseInterface {
  const ratingsReviews: any[] = getRatingsReviews();

  const index: number = ratingsReviews.findIndex(
    (ratingReviewData: RatingReviewInterface) =>
      ratingReviewData.ratingReviewId == ratingReviewId,
  );

  if (index != -1) {
    ratingsReviews.splice(index, 1);

    try {
      Store.dispatch(storeRatingsReviews(ratingsReviews));

      writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

      responseData = {
        status: StatusCodes.Success,
        message: Strings.ratingReviewDeleteSuccess,
        data: null,
      };
    } catch (error) {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.ratingReviewDeleteCache,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.ratingReviewNotFound,
      data: null,
    };
  }

  return responseData;
}

export function fetchAllStores(): ResponseInterface {
  const stores: any[] = getStores();

  if (stores.length != 0) {
    responseData = {
      status: StatusCodes.Success,
      message: Strings.storesSuccess,
      data: stores,
    };
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.storesNotFound,
      data: null,
    };
  }

  return responseData;
}

export function fetchStore(query: any): StoreInterface | undefined {
  const stores: any[] = getStores();

  const storeData: any = stores.find(
    (lol: StoreInterface) => lol.storeId == query,
  );

  return storeData;
}

export function getRatingsReviews(): any[] {
  const ratingsReviews: any[] = Store.getState()?.app?.ratingsReviews ?? [];

  return ratingsReviews;
}

export function getStores(): any[] {
  const stores: any[] = Store.getState()?.app?.stores ?? [];

  return stores;
}

export function updateReview(query: ReviewInterface): ResponseInterface {
  const ratingsReviews: any[] = getRatingsReviews();

  const index: number = ratingsReviews.findIndex(
    (ratingReviewData: RatingReviewInterface) =>
      ratingReviewData.ratingReviewId == query.reviewId,
  );

  if (index != -1) {
    ratingsReviews[index].createdAt = query.createdAt;
    ratingsReviews[index].review = query.review;

    try {
      Store.dispatch(storeRatingsReviews(ratingsReviews));

      writeJSON(Strings.JSON, JSON.stringify(Store.getState().app));

      responseData = {
        status: StatusCodes.Success,
        message: Strings.reviewUpdateSuccess,
        data: null,
      };
    } catch (error) {
      responseData = {
        status: StatusCodes.Error,
        message: Strings.reviewUpdateCache,
        data: null,
      };
    }
  } else {
    responseData = {
      status: StatusCodes.NotFound,
      message: Strings.reviewNotFound,
      data: null,
    };
  }

  return responseData;
}
