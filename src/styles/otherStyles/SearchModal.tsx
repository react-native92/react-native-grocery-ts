import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const SearchModal = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.primary,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  textInputContainerStyle: {
    height: 44,
    borderBottomWidth: 0,
  },
});

export default SearchModal;
