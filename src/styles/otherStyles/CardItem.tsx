import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const CardItem = StyleSheet.create({
  itemContainer: {
    backgroundColor: Colors.white,
    borderRadius: 4,
    marginHorizontal: 2,
    paddingHorizontal: 8,
    paddingVertical: 8,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
  imageContainer: {
    flex: 0.75,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 16,
  },
  weightContainer: {
    position: 'absolute',
    backgroundColor: Colors.lightGrey,
    borderRadius: 2,
    paddingHorizontal: 4,
    paddingVertical: 2,
    left: 0,
    top: 0,
    zIndex: 1,
  },
  iconContainer: {
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
});

export default CardItem;
