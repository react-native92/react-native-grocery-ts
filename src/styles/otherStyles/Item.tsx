import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const Item = StyleSheet.create({
  weightContainer: {
    backgroundColor: Colors.lightGrey,
    alignSelf: 'center',
    borderRadius: 2,
    marginVertical: 20,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  quantityIncDecContainer: {
    width: '87.5%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    marginVertical: 8,
    paddingVertical: 8,
  },
  iconButtonContainer: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.manatee,
    borderRadius: 40 / 2,
  },
  buttonContainer: {width: '37.5%', alignSelf: 'center'},
});

export default Item;
