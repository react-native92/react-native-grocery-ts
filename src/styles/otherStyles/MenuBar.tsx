import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const MenuBar = StyleSheet.create({
  menuBarModalContainer: {
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
    paddingHorizontal: 8,
    paddingVertical: 16,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
});

export default MenuBar;
