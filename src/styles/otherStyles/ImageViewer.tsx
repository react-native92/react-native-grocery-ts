import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const ImageViewer = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.primary,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
});

export default ImageViewer;
