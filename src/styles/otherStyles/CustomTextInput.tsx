import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const CustomTextInput = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 40,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
    marginVertical: 4,
  },
});

export default CustomTextInput;
