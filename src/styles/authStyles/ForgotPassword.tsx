import {StyleSheet} from 'react-native';

const ForgotPassword = StyleSheet.create({
  sendButtonContainer: {
    width: '25%',
    alignSelf: 'flex-end',
    marginVertical: 8,
  },
});

export default ForgotPassword;
