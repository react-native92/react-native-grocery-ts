import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const OnBoard = StyleSheet.create({
  squareContainer: {
    backgroundColor: Colors.primary,
    borderRadius: 20,
    transform: [{rotate: '45deg'}],
    elevation: 8,
  },
  buttonContainer: {width: '37.5%', elevation: 0},
});

export default OnBoard;
