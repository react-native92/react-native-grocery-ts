import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const MobileNumberVerification = StyleSheet.create({
  verifyButtonContainer: {
    width: '50%',
    alignSelf: 'flex-end',
  },
  otpInactiveView: {
    width: 75,
    height: 50,
    fontSize: 16,
    fontWeight: '800',
    color: Colors.ebony,
    textAlign: 'center',
    textTransform: 'none',
    borderBottomColor: Colors.lightGrey,
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  otpActiveView: {
    borderBottomColor: Colors.primary,
    borderBottomWidth: 2,
  },
  otpContainer: {
    height: 60,
    marginVertical: 8,
  },
  resendVerifyContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  modalSquareContainer: {
    backgroundColor: Colors.primary,
    borderRadius: 20,
    transform: [{rotate: '45deg'}],
    elevation: 8,
  },
  modalMessageContainer: {
    flex: 0.25,
    backgroundColor: Colors.white,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  modalMesssageIconContainer: {
    flex: 0.1375,
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  modalMesssageLightButtonContainer: {
    width: '25%',
    height: 36,
    alignSelf: 'flex-end',
    marginHorizontal: 8,
    elevation: 0,
  },
  modalMesssageSolidButtonContainer: {
    width: '25%',
    height: 36,
    alignSelf: 'flex-end',
    borderRadius: 2,
  },
});

export default MobileNumberVerification;
