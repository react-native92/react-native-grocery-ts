import {StyleSheet} from 'react-native';

const SignIn = StyleSheet.create({
  forgotPasswordSignInContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 8,
  },
  signInButtonContainer: {
    width: '50%',
    alignSelf: 'flex-end',
  },
});

export default SignIn;
