import {StyleSheet} from 'react-native';

const SignUp = StyleSheet.create({
  signUpButtonContainer: {
    width: '25%',
    alignSelf: 'flex-end',
    marginVertical: 16,
  },
});

export default SignUp;
