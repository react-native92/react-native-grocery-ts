import {StyleSheet} from 'react-native';

const MobileNumber = StyleSheet.create({
  mobileNumberTextInputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 20,
  },
  sendButtonContainer: {
    width: '25%',
    alignSelf: 'flex-end',
    marginVertical: 8,
  },
});

export default MobileNumber;
