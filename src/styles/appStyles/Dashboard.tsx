import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const AppNavigation = StyleSheet.create({
  iconBadgeContainer: {
    position: 'absolute',
    height: 16,
    width: 16,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.red,
    borderRadius: 16 / 2,
    left: 16,
    bottom: 12,
    zIndex: 1,
  },
  notificationDot: {
    position: 'absolute',
    height: 8,
    width: 8,
    borderRadius: 8 / 2,
    backgroundColor: Colors.red,
    right: 0,
  },
});

export default AppNavigation;
