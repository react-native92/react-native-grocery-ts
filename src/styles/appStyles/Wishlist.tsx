import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const Wishlist = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    overflow: 'hidden',
    backgroundColor: Colors.white,
    borderRadius: 4,
    paddingHorizontal: 8,
    paddingVertical: 8,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
  cardImageContainer: {flex: 0.25, backgroundColor: Colors.white},
  cardRatingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 8,
  },
  skeletonCardContainer: {
    width: '100%',
    height: Helpers.windowHeight * 0.145,
    borderRadius: 4,
  },
});

export default Wishlist;
