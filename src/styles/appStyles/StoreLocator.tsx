import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const StoreLocator = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mapView: {
    ...StyleSheet.absoluteFillObject,
  },
  storeListContainer: {
    position: 'absolute',
    width: '100%',
    backgroundColor: Colors.transparent,
    paddingHorizontal: 8,
    paddingVertical: 8,
    bottom: 0,
  },
  storeContainer: {
    width: Helpers.windowWidth * 0.5,
    backgroundColor: Colors.white,
    borderWidth: 2,
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 12,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
  storeInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 2,
  },
  storeRatingContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 2,
  },
  storeButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
  },
  solidButtonContainer: {
    width: '37.5%',
    height: 32,
    borderRadius: 32 / 2,
    marginVertical: 0,
    elevation: 0,
  },
  lightButtonContainer: {
    width: '27.5%',
    height: 32,
    borderColor: Colors.lightGrey,
    borderWidth: 1,
    borderRadius: 32 / 2,
    marginVertical: 0,
    elevation: 0,
  },
  skeletonCardContainer: {
    width: Helpers.windowWidth * 0.5,
    height: Helpers.windowHeight * 0.21625,
    borderRadius: 4,
  },
});

export default StoreLocator;
