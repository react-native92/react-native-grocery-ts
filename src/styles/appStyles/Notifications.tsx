import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const Notifications = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.white,
    borderRadius: 4,
    marginVertical: 8,
    marginHorizontal: 2,
    paddingHorizontal: 8,
    paddingVertical: 8,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
  iconContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.iceberg,
    borderRadius: 40 / 2,
  },
  deleteContainer: {
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.red,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
});

export default Notifications;
