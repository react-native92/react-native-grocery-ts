import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';
import Strings from '../../utils/Strings';

const Store = StyleSheet.create({
  categoriesContainer: {
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
    bottom: 2,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 8,
  },
  categoriesRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  categoryContainer: {
    width: `${100 / Strings.categoryRowItems}%`,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  categoryIconContainer: {
    height: 48,
    width: 48,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.iceberg,
    borderRadius: 48 / 2,
  },
  knobContainer: {
    width: '25%',
    height: 4,
    backgroundColor: Colors.primary75,
    alignSelf: 'center',
    bottom: 16,
  },
  skeletonCardContainer: {
    width: Helpers.windowWidth * 0.21625,
    height: Helpers.windowHeight * 0.2575,
    borderRadius: 4,
  },
});

export default Store;
