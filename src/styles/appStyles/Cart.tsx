import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const Cart = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    overflow: 'hidden',
    backgroundColor: Colors.white,
    borderRadius: 4,
    paddingHorizontal: 8,
    paddingVertical: 8,
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
  cardQuantityIncDecContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    overflow: 'hidden',
    backgroundColor: Colors.white,
    borderRadius: 4,
  },
  cardQuantityIconContainer: {
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.manatee,
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
  cardRatingContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 4,
  },
  cardWeightContainer: {
    backgroundColor: Colors.lightText,
    borderRadius: 4,
    marginVertical: 4,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  cardDeleteContainer: {
    position: 'absolute',
    paddingHorizontal: 4,
    paddingVertical: 4,
    bottom: '37.5%',
    right: 8,
  },
  cardStockStatusContainer: {
    position: 'absolute',
    paddingHorizontal: 8,
    paddingVertical: 4,
    bottom: 0,
    right: 0,
  },
  proceedToBuyContainer: {height: 48, borderRadius: 0, marginVertical: 0},
  proceedToBuyText: {textTransform: 'none'},
  skeletonCardContainer: {
    width: '100%',
    height: Helpers.windowHeight * 0.21625,
    borderRadius: 4,
  },
});

export default Cart;
