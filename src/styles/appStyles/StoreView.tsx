import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const StoreView = StyleSheet.create({
  itemContainer: {
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  itemSubContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  activityIndicatorContainer: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
  callToActionContainer: {
    position: 'absolute',
    paddingHorizontal: 4,
    paddingVertical: 4,
    right: 4,
    bottom: 4,
  },
  buttonContainer: {
    width: '33.33%',
    borderRadius: 40 / 2,
    marginVertical: 0,
    elevation: 0,
  },
  ratingsContainer: {borderBottomColor: Colors.lightGrey, borderBottomWidth: 1},
  reviewsButtonContainer: {
    width: '87.5%',
    height: 32,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 32 / 2,
    marginVertical: 0,
    paddingVertical: 4,
    elevation: 0,
  },
  featuresContainer: {
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
  },
  footerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingVertical: 8,
    elevation: 4,
  },
  footerIconContainer: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
    borderRadius: 40 / 2,
    marginRight: 16,
  },
  shareButtonContainer: {
    width: '27.5%',
    height: 36,
    borderColor: Colors.lightGrey,
    borderWidth: 1,
    borderRadius: 36 / 2,
    marginVertical: 0,
    elevation: 0,
  },
});

export default StoreView;
