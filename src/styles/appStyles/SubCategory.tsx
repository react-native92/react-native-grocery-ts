import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';
import * as Helpers from '../../utils/Helpers';

const SubCategory = StyleSheet.create({
  screenContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {flex: 0.275, backgroundColor: Colors.lightGrey},
  cardContainer: {
    backgroundColor: Colors.white,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 8,
    paddingHorizontal: 8,
    paddingVertical: 8,
    bottom: '7.5%',
    shadowColor: Colors.ebony,
    shadowOffset: {width: 4, height: 16},
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 12,
  },
  skeletonCardContainer: {
    width: '100%',
    height: Helpers.windowHeight * 0.0475,
    borderRadius: 4,
  },
});

export default SubCategory;
