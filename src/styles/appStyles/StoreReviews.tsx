import {StyleSheet} from 'react-native';
import Colors from '../../utils/Colors';

const StoreReviews = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.primary,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  ratingsContainer: {borderBottomColor: Colors.lightGrey, borderBottomWidth: 1},
  rateReviewContainer: {
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 1,
  },
  postButtonContainer: {
    height: 32,
    marginVertical: 0,
    paddingVertical: 4,
    elevation: 0,
  },
  userLogoContainer: {
    width: 32,
    height: 32,
    backgroundColor: Colors.lightGrey,
    borderRadius: 32 / 2,
  },
  profileLogoContainer: {
    width: 40,
    height: 40,
    backgroundColor: Colors.lightGrey,
    borderRadius: 40 / 2,
  },
  reviewContainer: {
    height: 80,
    borderColor: Colors.lightGrey,
    borderWidth: 1,
    borderRadius: 8,
    marginVertical: 8,
  },
});

export default StoreReviews;
