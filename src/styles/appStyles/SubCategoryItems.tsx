import {StyleSheet} from 'react-native';

const SubCategoryItems = StyleSheet.create({
  screenContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subCategoriesRowContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
});

export default SubCategoryItems;
