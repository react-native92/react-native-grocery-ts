import React, {FC, ReactElement} from 'react';
import {useTheme} from '@react-navigation/native';
import Colors from '../utils/Colors';
import SkeletonLoader from 'react-native-skeleton-placeholder';
import Strings from '../utils/Strings';
import * as Helpers from '../utils/Helpers';

const SkeletonPlaceholder: FC<any> = (props: any): ReactElement => {
  // SkeletonPlaceholder Variables

  // Theme Variables
  const themeScheme = Helpers.getThemeScheme();
  const Theme = useTheme().colors;

  return (
    <SkeletonLoader
      backgroundColor={Theme.background}
      highlightColor={
        themeScheme == Strings.dark ? Colors.stroke50 : Colors.lightGrey
      }
      speed={1000}>
      {props.children}
    </SkeletonLoader>
  );
};

export default SkeletonPlaceholder;
