import React, {ReactElement} from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import Colors from '../utils/Colors';
import ForgotPassword from '../screens/authScreens/ForgotPassword';
import MobileNumber from '../screens/authScreens/MobileNumber';
import MobileNumberVerification from '../screens/authScreens/MobileNumberVerification';
import OnBoard from '../screens/authScreens/OnBoard';
import SignInUp from '../screens/authScreens/SignInUp';
import Splash from '../screens/authScreens/Splash';
import Strings from '../utils/Strings';

const AuthNavigation = (): ReactElement => {
  // AuthNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group
      screenOptions={(): NativeStackNavigationOptions => ({
        headerShown: false,
        statusBarColor: Colors.white,
        statusBarStyle: 'dark',
      })}>
      <Stack.Screen
        name={Strings.forgotPassword}
        component={ForgotPassword}
        options={{
          headerShown: true,
          statusBarColor: Colors.primary75,
          statusBarStyle: 'light',
        }}
      />

      <Stack.Screen
        name={Strings.mobileNumber}
        component={MobileNumber}
        options={{
          headerShown: true,
          headerTitle: Strings.addNumber,
          statusBarColor: Colors.primary75,
          statusBarStyle: 'light',
        }}
      />

      <Stack.Screen
        name={Strings.mobileNumberVerification}
        component={MobileNumberVerification}
        options={{
          headerShown: true,
          headerTitle: Strings.verifyNumber,
          statusBarColor: Colors.primary75,
          statusBarStyle: 'light',
        }}
      />

      <Stack.Screen
        name={Strings.onBoard}
        component={OnBoard}
        options={{statusBarColor: Colors.primary75, statusBarStyle: 'light'}}
      />

      <Stack.Screen
        name={Strings.signInUp}
        component={SignInUp}
        initialParams={{initialRouteName: Strings.signIn}}
      />

      <Stack.Screen name={Strings.splash} component={Splash} />
    </Stack.Group>
  );
};

export default AuthNavigation;
