import React, {ReactElement} from 'react';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import Colors from '../utils/Colors';
import Dashboard from '../screens/appScreens/Dashboard';
import Item from '../components/appComponents/Item';
import Notifications from '../screens/appScreens/Notifications';
import StoreLocator from '../screens/appScreens/StoreLocator';
import StoreReviews from '../screens/appScreens/StoreReviews';
import StoreView from '../screens/appScreens/StoreView';
import Strings from '../utils/Strings';
import SubCategory from '../screens/appScreens/SubCategory';
import SubCategoryItems from '../screens/appScreens/SubCategoryItems';

const AppNavigation = (): ReactElement => {
  // AppNavigation Variables
  const Stack = createNativeStackNavigator();

  return (
    <Stack.Group
      screenOptions={(): NativeStackNavigationOptions => ({
        headerShown: true,
        statusBarColor: Colors.primary75,
        statusBarStyle: 'light',
      })}>
      <Stack.Screen
        name={Strings.dashboard}
        component={Dashboard}
        options={({route}): NativeStackNavigationOptions => ({
          headerBackVisible: false,
          headerTitle: getFocusedRouteNameFromRoute(route) ?? Strings.store,
        })}
      />

      <Stack.Screen name={Strings.item} component={Item} />

      <Stack.Screen name={Strings.notifications} component={Notifications} />

      <Stack.Screen name={Strings.storeLocator} component={StoreLocator} />

      <Stack.Screen name={Strings.storeReviews} component={StoreReviews} />

      <Stack.Screen name={Strings.storeView} component={StoreView} />

      <Stack.Screen
        name={Strings.subCategory}
        component={SubCategory}
        options={{
          headerStyle: {backgroundColor: Colors.transparent},
          headerTransparent: true,
          statusBarColor: Colors.black,
        }}
      />

      <Stack.Screen
        name={Strings.subCategoryItems}
        component={SubCategoryItems}
      />
    </Stack.Group>
  );
};

export default AppNavigation;
