const Strings = {
  // App Strings
  dark: 'dark',

  // AppNavigation Strings
  cart: 'Cart',
  dashboard: 'Dashboard',
  notifications: 'Notifications',
  profile: 'Profile',
  store: 'Store',
  storeLocator: 'Store Locator',
  storeReviews: 'Store Reviews',
  storeView: 'Store View',
  subCategory: 'Sub Category',
  subCategoryItems: 'Sub Category Items',

  // AuthNavigation Strings
  addNumber: 'Add number',
  forgotPassword: 'Forgot password',
  mobileNumber: 'Mobile Number',
  mobileNumberVerification: 'Mobile Number Verification',
  onBoard: 'OnBoard',
  signIn: 'Sign In',
  signInUp: 'SignInUp',
  signUp: 'Sign Up',
  splash: 'Splash',
  verifyNumber: 'Verify number',

  // Button Strings
  button: 'Button',

  // Cart Strings
  inStock: 'In Stock',
  outOfStock: 'Out of Stock',
  proceedToBuy: 'Proceed to Buy',

  // CustomTextInput Strings
  textInput: 'Text Input',
  errorMessage: 'Provide an error message',

  // Dashboard Strings
  exitAccount: 'Exit Account',
  exitAlertText: 'Are you sure to exit RN Grocery app?',

  // ForgotPassword Strings
  forgotPasswordText: 'Password reset',
  subForgotPasswordText: 'Enter email address to reset password',

  // Helpers Strings
  background: 'background',
  inactive: 'inactive',
  invalidAction: 'Invalid Action!',
  issueAction: 'Facing issue(s) while performing action!',
  unableAction: 'Sorry! Unable to perform action!',

  // Item Strings
  item: 'Item',
  quantity: 'Quantity',

  // JSON Strings
  JSON: 'Grocery.json',

  // MenuBar Strings
  currency: 'Currency',
  gotAQuestion: 'Got a Question?',
  groceryShopping: 'Grocery Shopping',
  help: 'Help',
  logout: 'Logout',
  orderHistory: 'Order History',
  termsConditions: 'Terms & Conditions',
  trackOrders: 'Track Orders',
  wishlist: 'Wishlist',

  // MobileNumber Strings
  mobileNumberText: "What's your number",
  subMobileNumberText: 'Enter mobile number to continue',

  // MobileNumberVerification Strings
  mobileNumberVerificationText: 'Verify your number',
  subMobileNumberVerificationText: '4 digit code sent to',
  resend: 'Resend',
  in: 'In',
  congratulations: 'Congratulations',
  verifyModalText:
    'Your mobile number verified \nsuccessfull! You can now continue \nusing Grocery Shopping App.',

  // Moment Strings
  d3: 'ddd',
  h2M1A: 'hh:MM A',
  H2m2: 'HH:mm',
  H2m2s2: 'HH:mm:ss',
  l2: 'll',
  Y4M2D2: 'YYYY-MM-DD',

  // NoResponse Strings
  notFindAnything: "We couldn't find anything!",
  whoops: 'Whoops!',

  // Notification Strings
  clear: 'Clear',
  clearAll: 'Clear All',
  lastWeek: 'Last Week',
  now: 'Now',
  others: 'Others',
  today: 'Today',
  yesterday: 'Yesterday',

  // Notify Strings
  accountRegisteredNotify:
    'Your account has been registered with us. Start adding items to your cart!',
  accountVerifiedNotify: 'Your account has been verified. Stay tuned with us!',
  cartEmptyNotify:
    'Everyone is grabbing their groceries. Hurry and get yours before stocks run out!',
  verification: 'Verification',
  welcomeBoard: 'Welcome Board',

  // OnBoard Strings
  exitText: 'Press back again to exit RN Grocery app!',
  welcomeTo: 'Welcome to',
  grocery: 'Grocery',
  shoppping: 'Shopping',

  // Other Strings
  categoryRowItems: 4,
  categorySlice: 10,
  debounceTimeout: 500,
  error: 'Error',
  googleMap:
    'https://www.google.com/maps/dir/?api=1&travelmode=driving&destination=',
  googleMapNavigation:
    'https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=',
  info: 'Info',
  otpLength: 4,
  ratingsCount: 5,
  subCategoryRowItems: 3,
  success: 'Success',
  timerSeconds: 59,

  // Other Button Strings
  cancel: 'Cancel',
  confirm: 'Confirm',
  send: 'Send',
  verify: 'Verify',
  later: 'Later',
  buyNow: 'Buy Now',
  addToCart: 'Add to Cart',

  // SearchModal Strings
  speechKey: 'AIzaSyCPi_GLVG2EC-uXpVcKMlxtWA5pYTyhOXI',
  search: 'Search',

  // SignIn Strings
  singInText: 'Sign in to Grocery App',
  subSignInText: 'Enter email & password to continue',

  // SignUp Strings
  singUpText: 'Welcome to Grocery App',
  subSignUpText: "Let's get started",
  username: 'Username',
  emailAddress: 'Email address',
  password: 'Password',

  // Splash Strings
  checkJSONCache: 'Sorry! Unable to check Grocery.json!',
  readJSONCache: 'Sorry! Unable to read Grocery.json!',

  // Store Strings
  allCategories: 'All Categories',
  fontAwesomeIcons: 'FontAwesome Icons',
  groceryAppDeals: 'Grocery App Deals',
  groceryAppMemberDeals: 'Grocery App Member Deals',
  viewAll: 'View all',

  // StoreLocator Strings
  call: 'Call',
  closed: 'Closed',
  directions: 'Directions',
  open: 'Open',
  share: 'Share',
  storeLocation: 'Store Location',

  // StoreReviews Strings
  describeYourExperience: 'Describe your experience',
  post: 'Post',
  rateAndReview: 'Rate and Review',
  shareDetailsExperience: 'Share details of your own experience at this place',
  shareExperience: 'Share your experience to help others',

  // StoreView Strings
  add: 'Add',
  address: 'Address',
  features: 'Features',
  payments: 'Payments',
  ratings: 'Ratings',
  reviews: 'Reviews',
  serviceOptions: 'Service Options',
  storeStatus: 'Store Status',
  website: 'Website',
  yet: 'Yet',

  // Error Strings
  asyncStorageCache: 'Sorry! Unable to clear async storage data!',
  cartEmpty: 'Sorry! The cart is empty!',
  cartError: 'Sorry! Unable to add the item to your cart!',
  cartSuccess: 'The item has been added to your cart!',
  cartUpdateError: 'Sorry! Unable to update the item to your cart!',
  cartUpdateSuccess: 'The item has been update to your cart!',
  createNotificationCache: 'Sorry! Unable to create the notification!',
  createNotificationSuccess: 'The notification has been created successfully!',
  createRatingCache: 'Sorry! Unable to create the rating!',
  createRatingSuccess: 'The rating has been created successfully!',
  dealsNotFound: 'Sorry! Deals not found!',
  dealsSuccess: 'The deals have been listed successfully!',
  emailAddressAvailibilityError: 'Email address already in use!',
  emailAddressError: 'Email address is required!',
  emailAddressInvalidError: 'Email address is invalid!',
  emailAddressNotFound: 'Email address not found!',
  favouriteError: 'Sorry! Unable to add the item to your favorites!',
  favouriteSuccess: 'The item has been added to your favorites!',
  getCartSuccess: 'The cart has been listed successfully!',
  itemNotFound: 'Sorry! Item not found!',
  itemsNotFound: 'Sorry! Items not found!',
  itemsSuccess: 'The items have been retrieved successfully!',
  loginInvalid: 'Invalid email address or password!',
  loginSuccess: 'The user has logged in successfully!',
  logoutSuccess: 'The user has logged out successfully!',
  mobileNumberAvailibilityError: 'Mobile number already in use!',
  mobileNumberError: 'Mobile number is required!',
  mobileNumberInvalidError: 'Mobile number is invalid!',
  noRatings: 'No ratings',
  notificationClearCache: 'Sorry! Unable to clear the notification!',
  notificationClearSuccess: 'The notifcation has been cleared!',
  notificationEmpty: 'Sorry! The notifications are empty!',
  notificationNotFound: 'Sorry! Notification not found!',
  notificationIdsNotFound: 'Sorry! Notification id(s) not found!',
  notificationsClearCache: 'Sorry! Unable to clear the notification(s)!',
  notificationsClearSuccess: 'The notifcations have been cleared!',
  notificationSuccess: 'The notifications have been listed successfully!',
  otpError: 'OTP is required!',
  otpGenerateError: 'Sorry! Unable to generate OTP!',
  otpGenerateSuccess: 'OTP has been generated successfully!',
  otpInvalidError: 'OTP is invalid!',
  passwordError: 'Password is required!',
  passwordInvalidError:
    'Password must be alphanumeric with atleast 8 characters!',
  ratingReviewDeleteCache: 'Sorry! Unable to delete the rating & review!',
  ratingReviewDeleteSuccess: 'The rating & review has been deleted!',
  ratingReviewNotFound: 'Sorry! Rating & Review not found!',
  registerationCache: 'Sorry! Unable to register the user!',
  registerationSuccess: 'The user has been registered successfully!',
  resendOTPError: 'Sorry! Unable to resend OTP!',
  resendOTPSuccess: 'OTP has been resent successfully!',
  resetPasswordError: 'Sorry! Unable to reset password!',
  resetPasswordSuccess: 'Password has been resetted successfully!',
  reviewUpdateCache: 'Sorry! Unable to update the review!',
  reviewUpdateSuccess: 'The review has been updated!',
  reviewNotFound: 'Sorry! Review not found!',
  stockLimitExceed: 'Sorry! The stock limit has been exceeded!',
  storeNotFound: 'Sorry! Store not found!',
  storesNotFound: 'Sorry! Stores not found!',
  storeSuccess: 'The store has been retrieved successfully!',
  storesSuccess: 'The stores have been retrieved successfully!',
  userExists: 'Sorry! User already exit(s)',
  usernameAvailibilityError: 'Username already in use!',
  usernameError: 'Username is required!',
  userNotFound: 'Sorry! User not found!',
  verifyOTPError: 'Sorry! Unable to verify OTP!',
  verifyOTPSuccess: 'OTP has been verified successfully!',
  wishListEmpty: 'Sorry! The favorites are empty!',
  wishListSuccess: 'The favorites have been listed successfully!',
  yetToBeOpened: 'Yet to be opened',
};

export default Strings;
