import {Dimensions, Linking, useColorScheme} from 'react-native';
import {createNotification} from '../configs/jsons/Notifications';
import {FirebaseMessagingTypes} from '@react-native-firebase/messaging';
import {NotificationInterface, UserInterface} from '../configs/ts/Interfaces';
import {showMessage} from 'react-native-flash-message';
import AsyncStorage from '@react-native-async-storage/async-storage';
import BackgroundTimer from 'react-native-background-timer';
import moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import Strings from './Strings';

// Common Functions
export function applyKeyToChar(code: string): any {
  return textToChars(Strings.grocery).reduce((a: any, b: any) => a ^ b, code);
}

export function byteHex(n: any): any {
  return ('0' + Number(n).toString(16)).substr(-2);
}

export function checkJSON(fileName: string): Promise<boolean> {
  return RNFetchBlob.fs.exists(
    `${RNFetchBlob.fs.dirs.DownloadDir}/${fileName}`,
  );
}

export function debounce<Params extends any[]>(
  f: (...args: Params) => any,
  delay: number,
): (...args: Params) => void {
  let timer: any;

  return (...args: Params) => {
    clearTimeout(timer);

    timer = setTimeout(() => {
      f(...args);
    }, delay);
  };
}

export function decrypt(value: any): string {
  return value
    .match(/.{1,2}/g)
    .map((hex: any) => parseInt(hex, 16))
    .map(applyKeyToChar)
    .map((charCode: any) => String.fromCharCode(charCode))
    .join('');
}

export function encrypt(value: any): any {
  return value
    .split('')
    .map(textToChars)
    .map(applyKeyToChar)
    .map(byteHex)
    .join('');
}

export function handleLinking(url: string): void {
  if (Boolean(validateUrl(url))) {
    Linking.canOpenURL(url)
      .then((supported: boolean): any => {
        if (supported) {
          return Linking.openURL(url);
        } else {
          showMessage({
            icon: 'auto',
            message: Strings.invalidAction,
            description: Strings.unableAction,
            type: 'warning',
          });
        }
      })
      .catch((): void => {
        showMessage({
          icon: 'auto',
          message: Strings.error,
          description: Strings.issueAction,
          type: 'danger',
        });
      });
  } else {
    Linking.openURL(url).catch((): void => {
      showMessage({
        icon: 'auto',
        message: Strings.error,
        description: Strings.issueAction,
        type: 'danger',
      });
    });
  }
}

export function randomizeArray(array: any[]): any[] {
  const randomizedArray: any[] = array
    .map((lol: any): any => ({lol, sort: Math.random()}))
    .sort((a: any, b: any): any => a.sort - b.sort)
    .map(({lol}): any => lol);

  return randomizedArray;
}

export function readJSON(fileName: string): Promise<any> {
  return RNFetchBlob.fs.readFile(
    `${RNFetchBlob.fs.dirs.DownloadDir}/${fileName}`,
    'utf8',
  );
}

export function sort(arr: any[], key: string): any[] {
  const sortedArr: any[] = arr.sort((a: any, b: any): any =>
    a[key] > b[key] ? -1 : 1,
  );

  return sortedArr;
}

export function textToChars(text: string): any {
  return text.split('').map(c => c.charCodeAt(0));
}

export function writeJSON(
  fileName: string,
  data: string,
  append?: boolean,
): void {
  RNFetchBlob.fs
    .writeStream(
      `${RNFetchBlob.fs.dirs.DownloadDir}/${fileName}`,
      'utf8',
      append,
    )
    .then((stream: any): any => {
      stream.write(data);

      return stream.close();
    })
    .catch((): void => {
      showMessage({
        description: `Sorry! Unable to create ${fileName}!`,
        icon: 'auto',
        message: Strings.error,
        type: 'danger',
      });
    });
}

// Common Variables
export const appStates: readonly string[] = [
  Strings.background,
  Strings.inactive,
];

// Field Validation Functions
export function checkField(str: string): string {
  return str && str.trim();
}

// Get Screen Height
export const screenHeight: number =
  Dimensions.get('screen').height > Dimensions.get('screen').width
    ? Dimensions.get('screen').height
    : Dimensions.get('screen').width;

// Get Screen Weight
export const screenWidth: number =
  Dimensions.get('screen').width > Dimensions.get('screen').height
    ? Dimensions.get('screen').width
    : Dimensions.get('screen').height;

// Get Window Height
export const windowHeight: number =
  Dimensions.get('window').height > Dimensions.get('window').width
    ? Dimensions.get('window').height
    : Dimensions.get('window').width;

// Get Window Weight
export const windowWidth: number =
  Dimensions.get('window').width > Dimensions.get('window').height
    ? Dimensions.get('window').width
    : Dimensions.get('window').height;

// Notification Functions
export function initNotifyBG(userData: undefined | UserInterface): void {
  BackgroundTimer.setInterval(async (): Promise<void> => {
    let userLoggedIn: null | string = await AsyncStorage.getItem(
      'userLoggedIn',
    );

    userLoggedIn = Boolean(userLoggedIn)
      ? moment().diff(moment(userLoggedIn), 'minutes').toString()
      : '0';

    userData?.cart.length == 0 &&
      userLoggedIn > '30' &&
      cartEmptyNotify(userData);
  }, 600000);
}

export function cartEmptyNotify(userData: undefined | UserInterface): void {
  const requestData: NotificationInterface = {
    createdAt: moment().format(`${Strings.Y4M2D2} ${Strings.H2m2s2}`),
    description: `Hey ${userData?.username.toLocaleUpperCase()}! ${
      Strings.cartEmptyNotify
    }`,
    isDelete: false,
    title: Strings.cart,
    userId: userData?.id,
  };

  createNotification(requestData);
}

export function pushNotify(
  remoteMessage: FirebaseMessagingTypes.RemoteMessage,
  userId: null | number | string,
): void {
  if (Boolean(userId) && remoteMessage?.notification) {
    const notificationData: FirebaseMessagingTypes.Notification =
      remoteMessage.notification;

    const requestData: NotificationInterface = {
      createdAt: moment().format(`${Strings.Y4M2D2} ${Strings.H2m2s2}`),
      description: notificationData.body,
      isDelete: false,
      title: notificationData.title,
      userId: userId,
    };

    createNotification(requestData);
  } else {
  }
}

// Regex Functions
export function validateEmail(emailId: string): boolean {
  const emailRegex: any =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  return Boolean(emailId)
    ? !emailRegex.test(String(emailId).toLowerCase())
    : false;
}

export function validatePassword(password: string): boolean {
  const passwordRegex = /^(?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9]{8,20}$/;

  return Boolean(password) ? !passwordRegex.test(String(password)) : false;
}

export function validateUrl(url: string): boolean {
  const urlRegex =
    /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

  return urlRegex.test(url);
}

// Theme Functions
export function getThemeScheme() {
  return useColorScheme();
}
