const Colors = {
  // Theme Hex Colors
  primary: '#2ED678',

  // App Hex Colors
  black: '#000000',
  ebony: '#282B40',
  ghostWhite: '#F7F7FA',
  iceberg: '#D4F3E5',
  lightGrey: '#D3D3D3',
  lightText: '#98A5C4',
  manatee: '#9AA1AD',
  red: '#FF0000',
  royalBlue: '#4169E1',
  stowaway: '#7B8593',
  tealGreen: '#23A26A',
  transparent: 'transparent',
  white: '#FFFFFF',

  // App RGBA Colors
  black125: 'rgba(0, 0, 0, 0.125)',
  white125: 'rgba(255, 255, 255, 0.125)',
  primary75: 'rgba(46, 214, 120, 0.75)',
  stroke50: 'rgba(229, 232, 239, 0.5)',
};

export default Colors;
