/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import {pushNotify} from './src/utils/Helpers';
import App from './App';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

// Register background handler for firebase push notification
messaging().setBackgroundMessageHandler(async remoteMessage => {
  const userId = await AsyncStorage.getItem('userId');

  pushNotify(remoteMessage, userId);
});

function HeadlessCheck({isHeadless}) {
  if (isHeadless) {
    return null;
  }

  return <App />;
}

AppRegistry.registerComponent(appName, () => HeadlessCheck);
